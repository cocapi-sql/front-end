<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$curr_member_array = array();
	$old_member_array = array();
    $utc_str = gmdate("Y-m-d H:i:s", time());
    
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
        
    // pull old members from sql
    $old_member_sql = "SELECT apm1.`member_tag`, apm1.`member_name`, ";
    $old_member_sql .= "  MAX(apm1.`member_townHallLevel`) AS th, ";
    $old_member_sql .= "  MIN(apm1.`member_timestamp`) AS start_time, ";
    $old_member_sql .= "  MAX(apm1.`member_current_timestamp`) AS end_time ";
    $old_member_sql .= "FROM `API_Member` apm1 ";
    $old_member_sql .= "WHERE apm1.`member_current_timestamp` NOT LIKE (SELECT apm2.`member_current_timestamp` ";
    $old_member_sql .= "           FROM `API_Member` apm2 ";
    $old_member_sql .= "           ORDER BY apm2.`member_current_timestamp` DESC ";
    $old_member_sql .= "           LIMIT 1 ) ";
    $old_member_sql .= "GROUP BY apm1.`member_tag`;";
    $old_member_result = $conn->query($old_member_sql);
    
    while($old_member_row = $old_member_result->fetch_assoc()) {
        $temp_tag = $old_member_row['member_tag'];
        $old_member_array[$temp_tag]['name'] = $old_member_row['member_name'];
        $old_member_array[$temp_tag]['th'] = $old_member_row['th'];
        $old_member_array[$temp_tag]['start_time'] = $old_member_row['start_time'];
        $old_member_array[$temp_tag]['end_time'] = $old_member_row['end_time'];
        $old_member_array[$temp_tag]['data_flag'] = 0;
        
        // get start time stamp
        $banned_sql = "SELECT * ";
        $banned_sql .= "FROM `API_Mem_Banned` ";
        $banned_sql .= "WHERE `banned_tag` LIKE '" . $temp_tag . "';";
        $banned_result = $conn->query($banned_sql);

        while($banned_row = $banned_result->fetch_assoc()) {
            $old_member_array[$temp_tag]['banned_time'] = $banned_row['banned_timestamp'];
            $old_member_array[$temp_tag]['notify'] = $banned_row['banned_notify'];
            $old_member_array[$temp_tag]['notified'] = $banned_row['banned_notified'];
            $old_member_array[$temp_tag]['notified_time'] = $banned_row['banned_notified_timestamp'];
            $old_member_array[$temp_tag]['entered'] = $banned_row['banned_entered'];
            $old_member_array[$temp_tag]['notes'] = $banned_row['banned_notes'];
            $old_member_array[$temp_tag]['data_flag'] = 1;
        }
    
    }

    if ($_POST['action'] == 'Save') {
        foreach ( $old_member_array as $temp_tag => $value ) {
            // get posted data
            $temp_ban = $_POST['ban_' . $temp_tag];
            if ( $temp_ban == NULL ) {
                $temp_ban = 0;
            }
            $temp_not = $_POST['not_' . $temp_tag];
            if ( $temp_not == NULL ) {
                $temp_not = 0;
            }
            $temp_note = substr( $_POST['note_' . $temp_tag], 0, 256 );

            // check if anything changed
            if (( $old_member_array[$temp_tag]['data_flag'] == 0 )&&
                    (( $temp_ban == 1 )||( $temp_not == 1 )||( $temp_note != NULL ))) {

                // insert member into sql
                $member_insert_sql = "INSERT INTO `API_Mem_Banned` ";
                $member_insert_sql .= "  (`banned_id`, `banned_tag`, `banned_timestamp`, `banned_notify`, ";
                $member_insert_sql .= "  `banned_notified`, `banned_notified_timestamp`, `banned_entered`, ";
                $member_insert_sql .= "  `banned_notes`) ";
                $member_insert_sql .= "VALUES (NULL, '" . $temp_tag . "', '" . $utc_str . "', '" . $temp_ban . "',  ";
                $member_insert_sql .= " '" . $temp_not . "', '0000-00-00 00:00:00.000000', 'unknown',  ";
                $member_insert_sql .= " '" . addslashes( $temp_note ) . "');";

                if ($conn->query($member_insert_sql) === TRUE) {
                    echo "Insert member " . $old_member_array[$temp_tag]['name'] . "<br>";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
    
            } elseif (( $old_member_array[$temp_tag]['data_flag'] == 1 )&&
                    (( $temp_ban != $old_member_array[$temp_tag]['notify'] )||
                    ( $temp_not != $old_member_array[$temp_tag]['notified'] )||
                    ( $temp_note !== $old_member_array[$temp_tag]['notes'] ))) {

                // update member in sql
                $member_update_sql = "UPDATE `API_Mem_Banned` ";
                $member_update_sql .= "SET `banned_timestamp` = '" . $utc_str . "', ";
                $member_update_sql .= "  `banned_notify` = '" . $temp_ban . "',  ";
                $member_update_sql .= "  `banned_notified` = '" . $temp_not . "',  ";
                $member_update_sql .= "  `banned_notes` = '" . addslashes( $temp_note ) . "'  ";
                $member_update_sql .= "WHERE `banned_tag` LIKE '" . $temp_tag . "';";
                
                if ($conn->query($member_update_sql) === TRUE) {
                    echo "Update member " . $old_member_array[$temp_tag]['name'] . "<br>";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
            }
            // set array data to entered data
            $old_member_array[$temp_tag]['notify'] = $temp_ban;
            $old_member_array[$temp_tag]['notified'] = $temp_not;
            $old_member_array[$temp_tag]['notes'] = $temp_note;
        }
    }

    // echo "curr_member_array<br>========================<br>";
    // echo var_dump ( $curr_member_array );
    // echo "<br>old_member_array<br>========================<br>";
    // echo var_dump ( $old_member_array );
?> 

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>Old Members</title>
</head>
<body>
    <form method="post">
        <center>
            <div id='Old Members'> 
                <h3 align="center">Old Members</h3>
                <br><input type="submit" name="action" value="Save"><br>
                <table width="100%">
                    <thead>
                        <th align="center"><b>Member</b></th>
                        <th align="center" width="10%"><b>Banned</b></th>
                        <th align="center" width="10%"><b>Notified</b></th>
                        <th align="center" width="40%"><b>Notes</b></th>
                    </thead> 
                    <tbody>
                        <?php foreach ( $old_member_array as $temp_tag => $value ) { ?>
                            <tr> 
                                <td align="left">
                                    <b><?php echo $old_member_array[$temp_tag]["name"]; ?></b><br>
                                    &emsp;<?php echo $temp_tag; ?>&emsp;
                                    TH: <?php echo $old_member_array[$temp_tag]["th"]; ?><br>
                                    &emsp;Start Date: <?php echo $old_member_array[$temp_tag]["start_time"]; ?><br> 
                                    &emsp;Last Date: <?php echo $old_member_array[$temp_tag]["end_time"]; ?><br> 
                                </td>
                                <td align="center">
                                    <input type="checkbox" 
                                    <?php if ( $old_member_array[$temp_tag]["notify"] == 1 ) { ?> 
                                        checked="checked" 
                                    <?php } ?>
                                    name="<?php echo 'ban_'.$temp_tag ?>"
                                    value="1">
                                </td>
                                <td align="center">
                                    <input type="checkbox" 
                                    <?php if ( $old_member_array[$temp_tag]["notified"] == 1 ) { ?> 
                                        checked="checked" 
                                    <?php } ?>
                                    name="<?php echo 'not_'.$temp_tag ?>"
                                    value="1">
                                </td>
                                <td align="left">
                                    <textarea rows="5" cols="50" 
                                    name="<?php echo 'note_'.$temp_tag ?>"><?php echo $old_member_array[$temp_tag]['notes'] ?></textarea> 
                                </td>
                            </tr> 
                        <?php } ?>
                    </tbody>
                </table> 
            <br><input type="submit" name="action" value="Save"><br>
            </div> 
        </center> 
    </form>
<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
</body> 




