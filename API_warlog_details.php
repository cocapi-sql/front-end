<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$war_selected = $_GET['war'];
	$warlog_array = array();
	$home_mem_array = array();
	$opp_mem_array = array();
	$battle_array = array();
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	
	$warlog_sql = "SELECT * ";
	$warlog_sql .= " FROM  `API_WarLog` ";
	$warlog_sql .= " WHERE  `warlog_id` = " . $war_selected . ";";
	$warlog_result = $conn->query($warlog_sql);
	
	while($warlog_row = $warlog_result->fetch_assoc()) {
		$warlog_array['result'] = $warlog_row['warlog_result'];
		$warlog_array['preparationStartTime'] = $warlog_row['warlog_preparationStartTime'];
		$warlog_array['startTime'] = $warlog_row['warlog_startTime'];
		$warlog_array['endTime'] = $warlog_row['warlog_endTime'];
		$warlog_array['teamSize'] = $warlog_row['warlog_teamSize'];
		$warlog_array['home_tag'] = $warlog_row['warlog_clan_tag'];
		$warlog_array['home_clanLevel'] = $warlog_row['warlog_clan_clanLevel'];
		$warlog_array['home_stars'] = $warlog_row['warlog_clan_stars'];
		$warlog_array['home_destructionPercentage'] = $warlog_row['warlog_clan_destructionPercentage'];
		$warlog_array['home_attacks'] = $warlog_row['warlog_clan_attacks'];
		$warlog_array['home_expEarned'] = $warlog_row['warlog_clan_expEarned'];
		$warlog_array['opp_tag'] = $warlog_row['warlog_opp_tag'];
		$warlog_array['opp_name'] = quotemeta( $warlog_row['warlog_opp_name'] );
		$warlog_array['opp_clanLevel'] = $warlog_row['warlog_opp_clanLevel'];
		$warlog_array['opp_stars'] = $warlog_row['warlog_opp_stars'];
		$warlog_array['opp_destructionPercentage'] = $warlog_row['warlog_opp_destructionPercentage'];
		$warlog_array['opp_attacks'] = $warlog_row['warlog_opp_attacks'];
		$warlog_array['opp_badgeUrls_small'] = $warlog_row['warlog_opp_badgeUrls_small'];
		$warlog_array['opp_badgeUrls_medium'] = $warlog_row['warlog_opp_badgeUrls_medium'];
		$warlog_array['opp_badgeUrls_large'] = $warlog_row['warlog_opp_badgeUrls_large'];
	}	
	
	$clan_sql = "SELECT * ";
	$clan_sql .= " FROM  `API_Clan` ";
	$clan_sql .= " WHERE  `clan_tag` LIKE  '" . $warlog_array['home_tag'] . "';";
	$clan_result = $conn->query($clan_sql);
	
	while($clan_row = $clan_result->fetch_assoc()) {
		$warlog_array['home_name'] = quotemeta( $clan_row['clan_name'] );
		$warlog_array['home_badgeUrls_small'] = $clan_row['clan_badgeUrls_small'];
		$warlog_array['home_badgeUrls_medium'] = $clan_row['clan_badgeUrls_medium'];
		$warlog_array['home_badgeUrls_large'] = $clan_row['clan_badgeUrls_large'];
	}
	
	
	$home_mem_sql = "SELECT * ";
	$home_mem_sql .= " FROM  `API_WarLog_Member` ";
	$home_mem_sql .= " WHERE  `member_warlog_id` = " . $war_selected . " ";
	$home_mem_sql .= " AND  `member_clan_tag` LIKE  '" . $warlog_array['home_tag'] . "';";
	$home_mem_result = $conn->query($home_mem_sql);
	
	while($home_mem_row = $home_mem_result->fetch_assoc()) {
		$temp_pos = $home_mem_row['member_mapPosition'];
		$home_mem_array[$temp_pos]['member_tag'] = $home_mem_row['member_tag'];
		$home_mem_array[$temp_pos]['member_name'] = quotemeta( $home_mem_row['member_name'] );
		$home_mem_array[$temp_pos]['member_townhallLevel'] = $home_mem_row['member_townhallLevel'];
	}

	$opp_mem_sql = "SELECT * ";
	$opp_mem_sql .= " FROM  `API_WarLog_Member` ";
	$opp_mem_sql .= " WHERE  `member_warlog_id` = " . $war_selected . " ";
	$opp_mem_sql .= " AND  `member_clan_tag` LIKE  '" . $warlog_array['opp_tag'] . "';";
	$opp_mem_result = $conn->query($opp_mem_sql);
	
	while($opp_mem_row = $opp_mem_result->fetch_assoc()) {
		$temp_pos = $opp_mem_row['member_mapPosition'];
		$opp_mem_array[$temp_pos]['member_tag'] = $opp_mem_row['member_tag'];
		$opp_mem_array[$temp_pos]['member_name'] = quotemeta( $opp_mem_row['member_name'] );
		$opp_mem_array[$temp_pos]['member_townhallLevel'] = $opp_mem_row['member_townhallLevel'];
	}
	
	$battle_sql = "SELECT att.member_mapPosition AS att_pos, att.member_name AS att_name, ";
    $battle_sql .= "   def.member_mapPosition AS def_pos, def.member_name AS def_name, ";
	$battle_sql .= "   `battle_stars`, `battle_newStars`, `battle_destructionPercentage`, `battle_clean_flag`,  ";
	$battle_sql .= "   `battle_loot_flag`, `battle_earlyLoot_flag`, `battle_noCC_flag` ";
	$battle_sql .= " FROM  `API_WarLog_Battle` AS bat ";
	$battle_sql .= " INNER JOIN `API_WarLog_Member` as att ";
	$battle_sql .= "  ON bat.battle_attackerTag LIKE att.member_tag AND ";
	$battle_sql .= "    bat.battle_warlog_id LIKE att.member_warlog_id ";
	$battle_sql .= " INNER JOIN `API_WarLog_Member` as def ";
	$battle_sql .= "  ON bat.battle_defenderTag LIKE def.member_tag AND ";
	$battle_sql .= "    bat.battle_warlog_id LIKE def.member_warlog_id ";
	$battle_sql .= " WHERE  `battle_warlog_id` = " . $war_selected . " ";
	$battle_sql .= " ORDER BY  `battle_order` ASC;";
	$battle_result = $conn->query($battle_sql);
	
	$i = 1;
	while($battle_row = $battle_result->fetch_assoc()) {
		$temp_att_pos = $battle_row['att_pos'];
		$temp_def_pos = $battle_row['def_pos'];
		$battle_array[$i]['att_pos'] = $temp_att_pos;
		$battle_array[$i]['att_name'] = quotemeta( $battle_row['att_name'] );
		$battle_array[$i]['def_pos'] = $temp_def_pos;
		$battle_array[$i]['def_name'] = quotemeta( $battle_row['def_name'] );
		$battle_array[$i]['stars'] = $battle_row['battle_stars'];
		$battle_array[$i]['newStars'] = $battle_row['battle_newStars'];
		$battle_array[$i]['destructionPercentage'] = $battle_row['battle_destructionPercentage'];
		$battle_array[$i]['clean_flag'] = $battle_row['battle_clean_flag'];
		$battle_array[$i]['loot_flag'] = $battle_row['battle_loot_flag'];
		$battle_array[$i]['earlyLoot_flag'] = $battle_row['battle_earlyLoot_flag'];
		$battle_array[$i]['noCC_flag'] = $battle_row['battle_noCC_flag'];
		$battle_array[$i]['star_image'] = 'http://www.the-blacklist.ca/images/jumi_code/API/Stars/';
		$battle_array[$i]['star_image'] .= $battle_array[$i]['stars'] . 'Star';
		$battle_array[$i]['star_image'] .= $battle_array[$i]['newStars'] . 'New.png';
// 		http://www.the-blacklist.ca/images/jumi_code/API/Stars/0Star0New.png
		
		// check if attacker is in home clan
		if ( strcmp ( $battle_array[$i]['att_name'], $home_mem_array[$temp_att_pos]['member_name'] ) == 0 ) {
			// home attacker and th levels
			$battle_array[$i]['home_flag'] = 1;
			$battle_array[$i]['att_th'] = $home_mem_array[$temp_att_pos]['member_townhallLevel'];
			$battle_array[$i]['def_th'] = $opp_mem_array[$temp_def_pos]['member_townhallLevel'];
			// check if attack 1 is used
			if ( !isset ( $home_mem_array[$temp_att_pos]['att1'] )) {
				$temp_att = 'att1';
			} else {
				$temp_att = 'att2';
			}
			$home_mem_array[$temp_att_pos][$temp_att]['def_pos'] = $battle_array[$i]['def_pos'];
			$home_mem_array[$temp_att_pos][$temp_att]['def_name'] = quotemeta( $battle_array[$i]['def_name'] );
			$home_mem_array[$temp_att_pos][$temp_att]['att'] = $i;
			$home_mem_array[$temp_att_pos][$temp_att]['stars'] = $battle_array[$i]['stars'];
			$home_mem_array[$temp_att_pos][$temp_att]['newStars'] = $battle_array[$i]['newStars'];
			$home_mem_array[$temp_att_pos][$temp_att]['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
			$home_mem_array[$temp_att_pos][$temp_att]['clean_flag'] = $battle_array[$i]['clean_flag'];
			$home_mem_array[$temp_att_pos][$temp_att]['loot_flag'] = $battle_array[$i]['loot_flag'];
			$home_mem_array[$temp_att_pos][$temp_att]['earlyLoot_flag'] = $battle_array[$i]['earlyLoot_flag'];
			$home_mem_array[$temp_att_pos][$temp_att]['noCC_flag'] = $battle_array[$i]['noCC_flag'];
			$home_mem_array[$temp_att_pos][$temp_att]['star_image'] = $battle_array[$i]['star_image'];
			// check if def is used
			if ( !isset ( $opp_mem_array[$temp_def_pos]['def'] )) {
				// first attack against base
				$opp_mem_array[$temp_def_pos]['def']['att_pos'] =  $battle_array[$i]['att_pos'];
				$opp_mem_array[$temp_def_pos]['def']['att_name'] =  quotemeta( $battle_array[$i]['att_name'] );
				$opp_mem_array[$temp_def_pos]['def']['count'] = 1;
				$opp_mem_array[$temp_def_pos]['def']['stars'] = $battle_array[$i]['stars'];
				$opp_mem_array[$temp_def_pos]['def']['totalStars'] = $battle_array[$i]['stars'];
				$opp_mem_array[$temp_def_pos]['def']['newStars'] = $battle_array[$i]['newStars'];
				$opp_mem_array[$temp_def_pos]['def']['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
				$opp_mem_array[$temp_def_pos]['def']['totalDestruction'] = $battle_array[$i]['destructionPercentage'];
				$opp_mem_array[$temp_def_pos]['def']['star_image'] = $battle_array[$i]['star_image'];
			} else {
				$opp_mem_array[$temp_def_pos]['def']['count']++;
				// check if better attack
				if (( $opp_mem_array[$temp_def_pos]['def']['stars'] < $battle_array[$i]['stars'] )||
						(( $opp_mem_array[$temp_def_pos]['def']['stars'] == $battle_array[$i]['stars'] )&&
						( $opp_mem_array[$temp_def_pos]['def']['destructionPercentage'] < $battle_array[$i]['destructionPercentage'] ))) {
					$opp_mem_array[$temp_def_pos]['def']['att_pos'] =  $battle_array[$i]['att_pos'];
					$opp_mem_array[$temp_def_pos]['def']['att_name'] =  quotemeta( $battle_array[$i]['att_name'] );
					$opp_mem_array[$temp_def_pos]['def']['count']++;
					$opp_mem_array[$temp_def_pos]['def']['stars'] = $battle_array[$i]['stars'];
					$opp_mem_array[$temp_def_pos]['def']['newStars'] = $battle_array[$i]['newStars'];
					$opp_mem_array[$temp_def_pos]['def']['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
					$opp_mem_array[$temp_def_pos]['def']['star_image'] = $battle_array[$i]['star_image'];
				}
				// total total stars and destructions
				$opp_mem_array[$temp_def_pos]['def']['totalStars'] = $battle_array[$i]['stars'] +
						$opp_mem_array[$temp_def_pos]['def']['totalStars'];
				$opp_mem_array[$temp_def_pos]['def']['totalDestruction'] = $battle_array[$i]['destructionPercentage'] +
						$opp_mem_array[$temp_def_pos]['def']['totalDestruction'];
			}
			// check if attack lost
			if ( $battle_array[$i]['stars'] == 0 ) {
				$warlog_array['home_attLost']++;
			} else {
				$warlog_array['home_attWon']++;
			}
			// add up total destruction and total stars
			$warlog_array['home_totalDes'] = $warlog_array['home_totalDes'] + $battle_array[$i]['destructionPercentage'];
			$warlog_array['home_totalStars'] = $warlog_array['home_totalStars'] + $battle_array[$i]['stars'];
			
			// setup battle flags
			$battle_array[$i]['battle_flag'] = '&emsp;&emsp;';
			if ( $battle_array[$i]['earlyLoot_flag'] ) {
				$battle_array[$i]['battle_flag'] .= 'Early Loot';
			} elseif ( $battle_array[$i]['loot_flag'] ) {
				$battle_array[$i]['battle_flag'] .= 'Loot';
			}
			
			if ((( $battle_array[$i]['earlyLoot_flag'] == 1 )&&( $battle_array[$i]['clean_flag'] == 1 ))||
					(( $battle_array[$i]['loot_flag'] == 1 )&&( $battle_array[$i]['clean_flag'] == 1 ))) {
				$battle_array[$i]['battle_flag'] .= ', Clean Up';
			} elseif ( $battle_array[$i]['clean_flag'] == 1 ) {
				$battle_array[$i]['battle_flag'] .= 'Clean Up';
			}
			
			if ((( $battle_array[$i]['earlyLoot_flag'] == 1 )&&( $battle_array[$i]['battle_noCC_flag'] == 1 ))||
					(( $battle_array[$i]['loot_flag'] == 1 )&&( $battle_array[$i]['battle_noCC_flag'] == 1 ))) {
				$battle_array[$i]['battle_flag'] .= ', No CC';
			} elseif ( $battle_array[$i]['battle_noCC_flag'] == 1 ) {
				$battle_array[$i]['battle_flag'] .= 'No CC';
			}
			
			$home_mem_array[$temp_att_pos][$temp_att]['battle_flag'] = $battle_array[$i]['battle_flag'];
		} else {
			// opp attacker
			$battle_array[$i]['home_flag'] = 0;
			$battle_array[$i]['att_th'] = $home_mem_array[$temp_att_pos]['member_townhallLevel'];
			$battle_array[$i]['def_th'] = $opp_mem_array[$temp_def_pos]['member_townhallLevel'];
			// check if attack 1 is used
			if ( !isset ( $opp_mem_array[$temp_att_pos]['att1'] )) {
				$temp_att = 'att1';
			} else {
				$temp_att = 'att2';
			}
			$opp_mem_array[$temp_att_pos][$temp_att]['def_pos'] = $battle_array[$i]['def_pos'];
			$opp_mem_array[$temp_att_pos][$temp_att]['def_name'] = quotemeta( $battle_array[$i]['def_name'] );
			$opp_mem_array[$temp_att_pos][$temp_att]['att'] = $i;
			$opp_mem_array[$temp_att_pos][$temp_att]['stars'] = $battle_array[$i]['stars'];
			$opp_mem_array[$temp_att_pos][$temp_att]['newStars'] = $battle_array[$i]['newStars'];
			$opp_mem_array[$temp_att_pos][$temp_att]['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
			$opp_mem_array[$temp_att_pos][$temp_att]['star_image'] = $battle_array[$i]['star_image'];
			// check if def is used
			if ( !isset ( $home_mem_array[$temp_def_pos]['def'] )) {
				// first attack against base
				$home_mem_array[$temp_def_pos]['def']['att_pos'] =  $battle_array[$i]['att_pos'];
				$home_mem_array[$temp_def_pos]['def']['att_name'] =  quotemeta( $battle_array[$i]['att_name'] );
				$home_mem_array[$temp_def_pos]['def']['count'] = 1;
				$home_mem_array[$temp_def_pos]['def']['stars'] = $battle_array[$i]['stars'];
				$home_mem_array[$temp_def_pos]['def']['totalStars'] = $battle_array[$i]['stars'];
				$home_mem_array[$temp_def_pos]['def']['newStars'] = $battle_array[$i]['newStars'];
				$home_mem_array[$temp_def_pos]['def']['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
				$home_mem_array[$temp_def_pos]['def']['totalDestruction'] = $battle_array[$i]['destructionPercentage'];
				$home_mem_array[$temp_def_pos]['def']['star_image'] = $battle_array[$i]['star_image'];
			} else {
				$home_mem_array[$temp_def_pos]['def']['count']++;
				// check if better attack
				if (( $home_mem_array[$temp_def_pos]['def']['stars'] < $battle_array[$i]['stars'] )||
						(( $home_mem_array[$temp_def_pos]['def']['stars'] == $battle_array[$i]['stars'] )&&
						( $home_mem_array[$temp_def_pos]['def']['destructionPercentage'] < $battle_array[$i]['destructionPercentage'] ))) {
					$home_mem_array[$temp_def_pos]['def']['att_pos'] =  $battle_array[$i]['att_pos'];
					$home_mem_array[$temp_def_pos]['def']['att_name'] =  $battle_array[$i]['att_name'];
					$home_mem_array[$temp_def_pos]['def']['count']++;
					$home_mem_array[$temp_def_pos]['def']['stars'] = $battle_array[$i]['stars'];
					$home_mem_array[$temp_def_pos]['def']['newStars'] = $battle_array[$i]['newStars'];
					$home_mem_array[$temp_def_pos]['def']['destructionPercentage'] = $battle_array[$i]['destructionPercentage'];
					$home_mem_array[$temp_def_pos]['def']['star_image'] = $battle_array[$i]['star_image'];
				}
				// total total stars and destructions
				$home_mem_array[$temp_def_pos]['def']['totalStars'] = $battle_array[$i]['stars'] +
						$home_mem_array[$temp_def_pos]['def']['totalStars'];
				$home_mem_array[$temp_def_pos]['def']['totalDestruction'] = $battle_array[$i]['destructionPercentage'] +
						$home_mem_array[$temp_def_pos]['def']['totalDestruction'];
			}
			// check if attack lost
			if ( $battle_array[$i]['stars'] == 0 ) {
				$warlog_array['opp_attLost']++;
			} else {
				$warlog_array['opp_attWon']++;
			}
			// add up total destruction and total stars
			$warlog_array['opp_totalDes'] = $warlog_array['opp_totalDes'] + $battle_array[$i]['destructionPercentage'];
			$warlog_array['opp_totalStars'] = $warlog_array['opp_totalStars'] + $battle_array[$i]['stars'];
		}
		$i++;
	}
	// iterate threw members and computer opponents star bases and star attacks
	for ( $i = 1; $i <= $warlog_array['teamSize']; $i++ ) {
		// check home clan for opp stars
		if (( !isset ( $home_mem_array[$i]['def']['stars'] ))||( $home_mem_array[$i]['def']['stars'] == 0 )) {
			$warlog_array['opp_0Star']++;
		} elseif ( $home_mem_array[$i]['def']['stars'] == 1 ) {
			$warlog_array['opp_1Star']++;
		} elseif ( $home_mem_array[$i]['def']['stars'] == 2 ) {
			$warlog_array['opp_2Star']++;
		} elseif ( $home_mem_array[$i]['def']['stars'] == 3 ) {
			$warlog_array['opp_3Star']++;
		}
		// check opp clan for home stars
		if (( !isset ( $opp_mem_array[$i]['def']['stars'] ))||( $opp_mem_array[$i]['def']['stars'] == 0 )) {
			$warlog_array['home_0Star']++;
		} elseif ( $opp_mem_array[$i]['def']['stars'] == 1 ) {
			$warlog_array['home_1Star']++;
		} elseif ( $opp_mem_array[$i]['def']['stars'] == 2 ) {
			$warlog_array['home_2Star']++;
		} elseif ( $opp_mem_array[$i]['def']['stars'] == 3 ) {
			$warlog_array['home_3Star']++;
		}
	}
	// set to 0 is not set
	if ( !isset ( $warlog_array['home_0Star'] )) {
		$warlog_array['home_0Star'] = 0;
	} 
	if ( !isset ( $warlog_array['home_1Star'] )) {
		$warlog_array['home_1Star'] = 0;
	} 
	if ( !isset ( $warlog_array['home_2Star'] )) {
		$warlog_array['home_2Star'] = 0;
	} 
	if ( !isset ( $warlog_array['home_3Star'] )) {
		$warlog_array['home_3Star'] = 0;
	}
	if ( !isset ( $warlog_array['opp_0Star'] )) {
		$warlog_array['opp_0Star'] = 0;
	} 
	if ( !isset ( $warlog_array['opp_1Star'] )) {
		$warlog_array['opp_1Star'] = 0;
	} 
	if ( !isset ( $warlog_array['opp_2Star'] )) {
		$warlog_array['opp_2Star'] = 0;
	} 
	if ( !isset ( $warlog_array['opp_3Star'] )) {
		$warlog_array['opp_3Star'] = 0;
	}
	// total attacks
	$warlog_array{'total_att'} = $warlog_array{'home_attacks'} + $warlog_array{'opp_attacks'};
	// count remaining attacks
	$warlog_array['home_attRem'] = ( $warlog_array['teamSize'] * 3 ) - $warlog_array['home_attacks'];
	$warlog_array['opp_attRem'] = ( $warlog_array['teamSize'] * 3 ) - $warlog_array['opp_attacks'];
	// compute stars per attack
	$warlog_array['home_starsperAtt'] = number_format( $warlog_array['home_totalStars'] / $warlog_array['home_attacks'], 2 );
	$warlog_array['opp_starsperAtt'] = number_format( $warlog_array['opp_totalStars'] / $warlog_array['opp_attacks'], 2 );
	// compute new stars per attack
	$warlog_array['home_newStarsperAtt'] = number_format( $warlog_array['home_stars'] / $warlog_array['home_attacks'], 2 );
	$warlog_array['opp_newStarsperAtt'] = number_format( $warlog_array['opp_stars'] / $warlog_array['opp_attacks'], 2 );

// 	echo var_dump($home_mem_array);
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $warlog_array['home_name']; ?></title>
</head>
<body>
	<div id="War Header">
		<center>
			<table width="80%">
				<tr>
					<td align="center" width="40%">
						<img src="<?php echo $warlog_array['home_badgeUrls_medium']; ?>" />
					</td>
					<td></td>
					<td align="center" width="40%">
						<img src="<?php echo $warlog_array['opp_badgeUrls_medium']; ?>" />
					</td>
				</tr>
				<tr>
					<td align="center">
						<h1><?php echo $warlog_array['home_name']; ?></h1>
					</td>
					<td align="center">
						<h1>VS</h1>
					</td>
					<td align="center">
						<h1><?php echo $warlog_array['opp_name']; ?></h1>
					</td>
				</tr>
				<tr>
					<td align="center">
						<h3><?php echo $warlog_array['home_stars']; ?></h3>
					</td>
					<td align="center">
						<h3>Stars</h3>
					</td>
					<td align="center">
						<h3><?php echo $warlog_array['opp_stars']; ?></h3>
					</td>
				</tr>
				<tr>
					<td align="center">
						<h3><?php echo $warlog_array['home_destructionPercentage']; ?>%</h3>
					</td>
					<td align="center">
						<h3>Destruction</h3>
					</td>
					<td align="center">
						<h3><?php echo $warlog_array['opp_destructionPercentage']; ?>%</h3>
					</td>
				</tr>
			</table>
		</center>
	</div>
	<div id="accordion">
		<h3 align="center">War Info.</h3>
		<div id="War Info">
			<center>
				<table width="100%">
					<tr>
						<th align="center" width="40%"></th>
						<th align="center">
							Attack Totals
						</th>
						<th align="center" width="40%"></th>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_attacks']; ?>
						</td>
						<td align="center">
							Attacks used
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_attacks']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_attWon']; ?>
						</td>
						<td align="center">
							Attacks won
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_attWon']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_attLost']; ?>
						</td>
						<td align="center">
							Attacks lost
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_attLost']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_attRem']; ?>
						</td>
						<td align="center">
							Attacks remaining
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_attRem']; ?>
						</td>
					</tr>
					<tr>
						<th align="center"></th>
						<th align="center">
							Best Attacks
						</th>
						<th align="center"></th>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_3Star']; ?>
						</td>
						<td align="center">
							3 stars
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_3Star']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_2Star']; ?>
						</td>
						<td align="center">
							2 stars
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_2Star']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_1Star']; ?>
						</td>
						<td align="center">
							1 stars
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_1Star']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_0Star']; ?>
						</td>
						<td align="center">
							0 stars
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_0Star']; ?>
						</td>
					</tr>
					<tr>
						<th align="center"></th>
						<th align="center">
							Attack Stats
						</th>
						<th align="center"></th>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_starsperAtt']; ?>
						</td>
						<td align="center">
							Stars per attack
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_starsperAtt']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_newStarsperAtt']; ?>
						</td>
						<td align="center">
							New stars per attack
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_newStarsperAtt']; ?>
						</td>
					</tr>
					<tr>
						<td align="center">
							<?php echo $warlog_array['home_destructionPercentage']; ?>%
						</td>
						<td align="center">
							Average Destruction
						</td>
						<td align="center">
							<?php echo $warlog_array['opp_destructionPercentage']; ?>%
						</td>
					</tr>
				</table>
			</center>
		</div>
		<h3 align="center">Home Attacks</h3>
		<div id="Home Attacks">
			<center>
				<table width="100%">
					<tr>
						<th>
							Map<br>Pos.
						</th>
						<th align="center">
							<br>Member
						</th>
						<th>
							<br>Attack #1
						</th>
						<th>
							<br>Attack #2
						</th>
						<th>
							Total<br>Stars
						</th>
					</tr>
					<?php for ( $i = 1; $i <= $warlog_array{'teamSize'}; $i++ ) { ?>
						<tr>
							<td align="center">
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $home_mem_array[$i]{'member_name'}; ?>
							</td>
							<td>
								<?php
									if ( isset( $home_mem_array[$i]{'att1'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $home_mem_array[$i]{'att1'}{'def_pos'} . ". " . 
															$home_mem_array[$i]{'att1'}{'def_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $home_mem_array[$i]['att1']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $home_mem_array[$i]{'att1'}{'battle_flag'};
												echo '</td>';
												echo '<th align="center">';
													echo $home_mem_array[$i]{'att1'}{'destructionPercentage'} . "%"; 
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td>
								<?php
									if ( isset( $home_mem_array[$i]{'att2'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $home_mem_array[$i]{'att2'}{'def_pos'} . ". " . 
															$home_mem_array[$i]{'att2'}{'def_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $home_mem_array[$i]['att2']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $home_mem_array[$i]{'att2'}{'battle_flag'};
												echo '</td>';
												echo '<th align="center">';
													echo $home_mem_array[$i]{'att2'}{'destructionPercentage'} . "%"; 
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td align="center">
								<?php echo $home_mem_array[$i]{'att1'}{'stars'} + $home_mem_array[$i]{'att2'}{'stars'}; ?>
								/
								<?php echo $home_mem_array[$i]{'att1'}{'newStars'} + $home_mem_array[$i]{'att2'}{'newStars'}; ?>
							</td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Home Defense</h3>
		<div id="Home Defense">
			<center>
				<table width="100%">
					<tr>
						<th>
							Map<br>Pos.
						</th>
						<th align="center">
							<br>Member
						</th>
						<th>
							<br>Defense
						</th>
						<th>
							Defense<br>Attacks
						</th>
						<th>
							Total<br>Stars
						</th>
						<th>
							Total<br>Dest.
						</th>
					</tr>
					<?php for ( $i = 1; $i <= $warlog_array{'teamSize'}; $i++ ) { ?>
						<tr>
							<td align="center">
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $home_mem_array[$i]{'member_name'}; ?>
							</td>
							<td>
								<?php
									if ( isset( $home_mem_array[$i]{'def'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $home_mem_array[$i]{'def'}{'att_pos'} . ". " . 
															$home_mem_array[$i]{'def'}{'att_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $home_mem_array[$i]['def']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td></td>';
												echo '<th align="center">';
													echo $home_mem_array[$i]{'def'}{'destructionPercentage'} . '%';
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td align="center">
								<?php echo $home_mem_array[$i]{'def'}{'count'}; ?>
							</td>
							<td align="center">
								<?php echo $home_mem_array[$i]{'def'}{'totalStars'}; ?>
							</td>
							<td align="center">
								<?php echo $home_mem_array[$i]{'def'}{'totalDestruction'} . '%'; ?>
							</td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Enemy Attacks</h3>
		<div id="Enemy Attacks">
			<center>
				<table width="100%">
					<tr>
						<th>
							Map<br>Pos.
						</th>
						<th align="center">
							<br>Member
						</th>
						<th>
							<br>Attack #1
						</th>
						<th>
							<br>Attack #2
						</th>
						<th>
							Total<br>Stars
						</th>
					</tr>
					<?php for ( $i = 1; $i <= $warlog_array{'teamSize'}; $i++ ) { ?>
						<tr>
							<td align="center">
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $opp_mem_array[$i]{'member_name'}; ?>
							</td>
							<td>
								<?php
									if ( isset( $opp_mem_array[$i]{'att1'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $opp_mem_array[$i]{'att1'}{'def_pos'} . ". " . 
															$opp_mem_array[$i]{'att1'}{'def_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $opp_mem_array[$i]['att1']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo '&emsp;';
												echo '</td>';
												echo '<th align="center">';
													echo $opp_mem_array[$i]{'att1'}{'destructionPercentage'} . "%"; 
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td>
								<?php
									if ( isset( $opp_mem_array[$i]{'att2'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $opp_mem_array[$i]{'att2'}{'def_pos'} . ". " . 
															$opp_mem_array[$i]{'att2'}{'def_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $opp_mem_array[$i]['att2']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo '&emsp;';
												echo '</td>';
												echo '<th align="center">';
													echo $opp_mem_array[$i]{'att2'}{'destructionPercentage'} . "%"; 
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td align="center">
								<?php echo $opp_mem_array[$i]{'att1'}{'stars'} + $opp_mem_array[$i]{'att2'}{'stars'}; ?>
								/
								<?php echo $opp_mem_array[$i]{'att1'}{'newStars'} + $opp_mem_array[$i]{'att2'}{'newStars'}; ?>
							</td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Enemy Defense</h3>
		<div id="Enemy Defense">
			<center>
				<table width="100%">
					<tr>
						<th>
							Map<br>Pos.
						</th>
						<th align="center">
							<br>Member
						</th>
						<th>
							<br>Defense
						</th>
						<th>
							Defense<br>Attacks
						</th>
						<th>
							Total<br>Stars
						</th>
						<th>
							Total<br>Dest.
						</th>
					</tr>
					<?php for ( $i = 1; $i <= $warlog_array{'teamSize'}; $i++ ) { ?>
						<tr>
							<td align="center">
								<?php echo $i; ?>
							</td>
							<td>
								<?php echo $opp_mem_array[$i]{'member_name'}; ?>
							</td>
							<td>
								<?php
									if ( isset( $opp_mem_array[$i]{'def'} )) {
										echo '<table width="100%">';
											echo '<tr>';
												echo '<td align="left" width="80%">';
													echo $opp_mem_array[$i]{'def'}{'att_pos'} . ". " . 
															$opp_mem_array[$i]{'def'}{'att_name'};
												echo '</td>';
												echo '<th align="center">';
													echo '<img src="' . $opp_mem_array[$i]['def']['star_image'] . '" width="60" height="20">';
												echo '</th>';
											echo "</tr>";
											echo '<tr>';
												echo '<td></td>';
												echo '<th align="center">';
													echo $opp_mem_array[$i]{'def'}{'destructionPercentage'} . '%';
												echo '</th>';
											echo "</tr>";
										echo "</table>";
									} else {
										echo "<br>N/A";
									}
								?>
							</td>
							<td align="center">
								<?php echo $opp_mem_array[$i]{'def'}{'count'}; ?>
							</td>
							<td align="center">
								<?php echo $opp_mem_array[$i]{'def'}{'totalStars'}; ?>
							</td>
							<td align="center">
								<?php echo $opp_mem_array[$i]{'def'}{'totalDestruction'} . '%'; ?>
							</td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Battles</h3>
		<div id="Battles">
			<center>
				<table width="100%">
					<tr>
						<th width="40%"></th>
						<th align="center">
							VS
						</th>
						<th width="40%"></th>
					</tr>
					<?php for ( $i = $warlog_array{'total_att'}; $i > 0; $i-- ) { ?>
						<tr>
							<?php if ( $battle_array[$i]['home_flag'] == 1 ) { ?>
								<th colspan="2">
									<table width="100%">
										<tr>
											<th align="left">
												<?php echo $battle_array[$i]{'att_pos'} . ". " . 
																$battle_array[$i]{'att_name'}; ?>
											</th>
											<th align="center" width=60>
												<img src="<?php echo $battle_array[$i]['star_image'] ?>" width="60" height="20">
											</th>
										</tr> 
										<tr>
											<th align="left">
												<?php echo '&emsp;&emsp;Town Hall ' . $battle_array[$i]{'att_th'} .
																'&emsp;&emsp;' .$battle_array[$i]{'battle_flag'}; ?>
											</th>
											<th align="center">
												<?php echo $battle_array[$i]{'destructionPercentage'} . '%'; ?>
											</th>
										</tr>
									</table>
								</th>
								<td align="right">
									<?php echo $battle_array[$i]{'def_pos'} . ". " . 
													$battle_array[$i]{'def_name'}; ?>
									<br>
									<?php echo 'Town Hall ' . $battle_array[$i]{'att_th'} . '&emsp;&emsp;'; ?>
								</td>
							<?php } else { ?>
								<td aligh="left">
									<?php echo $battle_array[$i]{'def_pos'} . ". " . 
													$battle_array[$i]{'def_name'}; ?>
									<br>
									<?php echo '&emsp;&emsp;Town Hall ' . $battle_array[$i]{'att_th'}; ?>
								</td>
								<th colspan="2">
									<table width="100%">
										<tr>
											<th align="center" width=60>
												<img src="<?php echo $battle_array[$i]['star_image'] ?>" width="60" height="20">
											</th>
											<th align="right">
												<?php echo $battle_array[$i]{'att_pos'} . ". " . 
																$battle_array[$i]{'att_name'}; ?>
											</th>
										</tr> 
										<tr>
											<th align="center">
												<?php echo $battle_array[$i]{'destructionPercentage'} . '%'; ?>
											</th>
											<th align="right">
												<?php echo '&emsp;&emsp;Town Hall ' . $battle_array[$i]{'att_th'} .
																'&emsp;&emsp;' .$battle_array[$i]{'battle_flag'}; ?>
											</th>
										</tr>
									</table>
								</th>
							<?php } ?>
						</tr>
					<?php } ?>				
				</table>
			</center>
		</div>
	</div>
</br>  


<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#accordion" ).accordion({
			heightStyle: "content"
		});
	});
</script>


</body>

