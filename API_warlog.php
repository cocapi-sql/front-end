<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$clan_selected = "BLACK LIST";
	$clan_array = array();
	$warlog_array = array();
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# Get member or use default set to BlackList
	if ( isset ( $_GET['clan'] ) ) {
		$clan_selected = $_GET['clan'];
	}
	
	$clan_sql = "SELECT * ";
	$clan_sql .= "FROM `API_Clan` ";
	$clan_sql .= "WHERE clan_name LIKE '" . $clan_selected . "' ";
	$clan_sql .= "LIMIT 1;";
	$clan_result = $conn->query($clan_sql);
	

	while($clan_row = $clan_result->fetch_assoc()) {
		$clan_array["clan_tag"] = $clan_row["clan_tag"];
		$clan_array["clan_name"] = $clan_row["clan_name"];
		$clan_array["clan_description"] = str_replace( '\\', '', $clan_row["clan_description"] );
		$clan_array["clan_badgeUrls_medium"] = $clan_row["clan_badgeUrls_medium"];
	}

	$warlog_sql = "SELECT * ";
	$warlog_sql .= " FROM `API_WarLog`  ";
	$warlog_sql .= " WHERE `warlog_clan_tag` LIKE '" . $clan_array["clan_tag"] . "'  ";
	$warlog_sql .= " ORDER BY `warlog_endTime` DESC;";
	$warlog_result = $conn->query($warlog_sql);
	
	$temp_id = 0;
	while($warlog_row = $warlog_result->fetch_assoc()) {
		$warlog_array[$temp_id]["warlog_id"] = $warlog_row["warlog_id"];
		$warlog_array[$temp_id]["warlog_result"] = $warlog_row["warlog_result"];
		$warlog_array[$temp_id]["warlog_details"] = $warlog_row["warlog_details"];
		$warlog_array[$temp_id]["warlog_endTime"] = $warlog_row["warlog_endTime"];
		$warlog_array[$temp_id]["warlog_teamSize"] = $warlog_row["warlog_teamSize"];
		$warlog_array[$temp_id]["warlog_clan_clanLevel"] = $warlog_row["warlog_clan_clanLevel"];
		$warlog_array[$temp_id]["warlog_clan_stars"] = $warlog_row["warlog_clan_stars"];
		$warlog_array[$temp_id]["warlog_clan_destructionPercentage"] = $warlog_row["warlog_clan_destructionPercentage"];
		$warlog_array[$temp_id]["warlog_clan_attacks"] = $warlog_row["warlog_clan_attacks"];
		$warlog_array[$temp_id]["warlog_clan_expEarned"] = $warlog_row["warlog_clan_expEarned"];
		$warlog_array[$temp_id]["warlog_opp_tag"] = $warlog_row["warlog_opp_tag"];
		$warlog_array[$temp_id]["warlog_opp_name"] = $warlog_row["warlog_opp_name"];
		$warlog_array[$temp_id]["warlog_opp_clanLevel"] = $warlog_row["warlog_opp_clanLevel"];
		$warlog_array[$temp_id]["warlog_opp_stars"] = $warlog_row["warlog_opp_stars"];
		$warlog_array[$temp_id]["warlog_opp_destructionPercentage"] = $warlog_row["warlog_opp_destructionPercentage"];
		$warlog_array[$temp_id]["warlog_opp_badgeUrls_small"] = $warlog_row["warlog_opp_badgeUrls_small"];
		$warlog_array[$temp_id]["warlog_opp_badgeUrls_medium"] = $warlog_row["warlog_opp_badgeUrls_medium"];
		$warlog_array[$temp_id]["warlog_opp_badgeUrls_large"] = $warlog_row["warlog_opp_badgeUrls_large"];
		$temp_id++;
	}	
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>


	<h1 align="center"><?php echo $clan_array["clan_name"]; ?></h1>
	<center>
		<img src="<?php echo $clan_array["clan_badgeUrls_medium"]; ?>" /><br>
		<h3>Description:</h3>
		<?php echo $clan_array["clan_description"]; ?><br><br><br>
	</center>

	<div id="Clan WarLog Info">
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b>War<br>Date</b></th>
				<th align="center"><b>War<br>Size</b></th>
				<th align="center"><b>Home<br>Level</b></th>
				<th align="center"><b>Home<br>Dest.</b></th>
				<th align="center"><b>Home<br>Stars</b></th>
				<th align="center"><b>Result</b></th>
				<th align="center"><b>Enemy<br>Stars</b></th>
				<th align="center"><b>Enemy<br>Dest.</b></th>
				<th align="center"><b>Enemy<br>Level</b></th>
				<th align="center"><b>Enemy<br>Name</b></th>
			</thead>
			<tbody>
				<?php foreach ( $warlog_array as $temp_id => $value ) { ?>
					<tr>
						<td>
							<?php echo date_format( date_create( $warlog_array[$temp_id]["warlog_endTime"] ), "m/d/Y" ); ?>
						</td>
						<td align="center">
							<?php echo $warlog_array[$temp_id]["warlog_teamSize"]; ?>
							&nbsp;VS&nbsp;
							<?php echo $warlog_array[$temp_id]["warlog_teamSize"]; ?>
						</td>
						<td align="center">
							<?php echo $warlog_array[$temp_id]["warlog_clan_clanLevel"]; ?>
						</td>
						<td align="center">
							<?php 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] == $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									if ( $warlog_array[$temp_id]["warlog_clan_destructionPercentage"] > $warlog_array[$temp_id]["warlog_opp_destructionPercentage"] ) {
										echo "<font color=\"green\">";
								} elseif ( $warlog_array[$temp_id]["warlog_clan_destructionPercentage"] < $warlog_array[$temp_id]["warlog_opp_destructionPercentage"] ) {
									echo "<font color=\"red\">";
								} else {
									echo "<font color=\"yellow\">";
								}
								}
								echo $warlog_array[$temp_id]["warlog_clan_destructionPercentage"] . "%"; 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] == $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "</font>";
								}
							?>
						</td>
						<td align="center">
							<?php 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] > $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "<font color=\"green\">";
								} elseif ( $warlog_array[$temp_id]["warlog_clan_stars"] < $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "<font color=\"red\">";
								} else {
									echo "<font color=\"yellow\">";
								}
								echo $warlog_array[$temp_id]["warlog_clan_stars"]; 
							?>
							</font>
						</td>
						<td align="center">
							<?php 
								if ( $warlog_array[$temp_id]["warlog_result"] == 'win' ) {
									echo "<font color=\"green\">";
								} elseif ( $warlog_array[$temp_id]["warlog_result"] == 'lose' ) {
									echo "<font color=\"red\">";
								} else {
									echo "<font color=\"yellow\">";
								}
								echo $warlog_array[$temp_id]["warlog_result"]; 
								if ( $warlog_array[$temp_id]["warlog_details"] == 1 ) {
									echo '<br><a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=72&war=' . $warlog_array[$temp_id]["warlog_id"] . '">';
									echo 'Detail</a>';
								}
							?>
							</font>
						</td>
						<td align="center">
							<?php 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] < $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "<font color=\"green\">";
								} elseif ( $warlog_array[$temp_id]["warlog_clan_stars"] > $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "<font color=\"red\">";
								} else {
									echo "<font color=\"yellow\">";
								}
								echo $warlog_array[$temp_id]["warlog_opp_stars"]; 
							?>
							</font>
						</td>
						<td align="center">
							<?php 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] == $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									if ( $warlog_array[$temp_id]["warlog_clan_destructionPercentage"] < $warlog_array[$temp_id]["warlog_opp_destructionPercentage"] ) {
										echo "<font color=\"green\">";
									} elseif ( $warlog_array[$temp_id]["warlog_clan_destructionPercentage"] > $warlog_array[$temp_id]["warlog_opp_destructionPercentage"] ) {
										echo "<font color=\"red\">";
									} else {
										echo "<font color=\"yellow\">";
									}
								}
								echo $warlog_array[$temp_id]["warlog_opp_destructionPercentage"] . "%"; 
								if ( $warlog_array[$temp_id]["warlog_clan_stars"] == $warlog_array[$temp_id]["warlog_opp_stars"] ) {
									echo "</font>";
								}
							?>
						</td>
						<td align="center">
							<?php echo $warlog_array[$temp_id]["warlog_opp_clanLevel"]; ?>
						</td>
						<td>
							<img src="<?php echo $warlog_array[$temp_id]["warlog_opp_badgeUrls_small"]; ?>" />
							<?php echo $warlog_array[$temp_id]["warlog_opp_name"]; ?>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

<center>
	<b>This data was updated at <?php echo $member_array["member_timestamp"]; ?> GMT.</b>
</center>
</br>  

<h2 style="text-align: center;">Clan Picker</h2>
<form method="get">

	<center>
		<input type="radio" name="clan" value="BLACK LIST">&nbsp; Black List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="WHITE LIST">&nbsp; White List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="GOLD LIST">&nbsp; Gold List &nbsp;&nbsp;</input>
	</center>
	<center><input type="submit" name="submit" value="Submit"/></center>
</form>

<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>

</body>