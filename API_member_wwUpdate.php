<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$member_selected = "#R280P220";
	$member_array = array();
	$defense_array = array();
	$traps_array = array();
	$misc_array = array();
	$nonWt_array = array();
	$heros_array = array();
	$troops_array = array();
	$spells_array = array();
	if ( is_null ( $_POST['action'] )) {
		$intialize = 0;
	} else {
		$intialize = 1;
	}
	$utc_str = gmdate("Y-m-d H:i:s", time());
	
	// setup and zero war weight
	$warWeight = array();
	$warWeight['defense'] = 0;
	$warWeight['bldg_defense'] = 0;
	$warWeight['wall_defense'] = 0;
	$warWeight['traps'] = 0;
	$warWeight['misc'] = 0;
	$warWeight['troops'] = 0;
	$warWeight['elixir_troops'] = 0;
	$warWeight['de_troops'] = 0;
	$warWeight['heros']['off'] = 0;
	$warWeight['heros']['def'] = 0;
	$warWeight['spells'] = 0;
	$warWeight['def'] = 0;
	$warWeight['off'] = 0;
	$warWeight['total'] = 0;
	$warWeight['rounded'] = 0;
	$warWeight['flag'] = 1;
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	// Get member or use default set to BlackList
	if ( isset ( $_GET['member'] ) ) {
		$member_selected = '#' . $_GET['member'];
	}
			
	$member_sql = "SELECT * ";
	$member_sql .= " FROM `API_Member` AS am ";
	$member_sql .= " INNER JOIN `API_League` AS al ";
	$member_sql .= "  ON am.`member_league_id`=al.`league_id` ";
	$member_sql .= " INNER JOIN `API_Clan` AS ac ";
	$member_sql .= "  ON am.`member_clan_tag`=ac.`clan_tag` ";
	$member_sql .= " WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_sql .= " ORDER BY `member_current_timestamp` DESC ";
	$member_sql .= " LIMIT 1;";
	$member_result = $conn->query($member_sql);
	
	while($member_row = $member_result->fetch_assoc()) {
		$member_array["member_tag"] = $member_row["member_tag"];
		$member_array["clan_tag"] = $member_row["clan_tag"];
		$member_array["league_id"] = $member_row["league_id"];
		$member_array["member_name"] = $member_row["member_name"];
		$member_array["member_timestamp"] = $member_row["member_current_timestamp"];
		$member_array["member_clanRank"] = $member_row["member_clanRank"];
		$member_array["member_previousClanRank"] = $member_row["member_previousClanRank"];
		$member_array["member_expLevel"] = $member_row["member_expLevel"];
		$member_array["member_role"] = $member_row["member_role"];
		$member_array["member_townHallLevel"] = $member_row["member_townHallLevel"];
		$member_array["member_trophies"] = $member_row["member_trophies"];
		$member_array["member_bestTrophies"] = $member_row["member_bestTrophies"];
		$member_array["member_warStars"] = $member_row["member_warStars"];
		$member_array["member_attackWins"] = $member_row["member_attackWins"];
		$member_array["member_defenseWins"] = $member_row["member_defenseWins"];
		$member_array["member_donations"] = $member_row["member_donations"];
		$member_array["member_total_donations"] = $member_row["member_total_donations"];
		$member_array["member_donationsReceived"] = $member_row["member_donationsReceived"];
		$member_array["member_total_donationsReceived"] = $member_row["member_total_donationsReceived"];
		$member_array["clan_name"] = $member_row["clan_name"];
		$member_array["clan_description"] = $member_row["clan_description"];
		$member_array["clan_badgeUrls_small"] = $member_row["clan_badgeUrls_small"];
		$member_array["clan_badgeUrls_medium"] = $member_row["clan_badgeUrls_medium"];
		$member_array["clan_badgeUrls_large"] = $member_row["clan_badgeUrls_large"];
		$member_array["league_name"] = $member_row["league_name"];
		$member_array["league_iconUrls_tiny"] = $member_row["league_iconUrls_tiny"];
		$member_array["league_iconUrls_small"] = $member_row["league_iconUrls_small"];
		$member_array["league_iconUrls_medium"] = $member_row["league_iconUrls_medium"];
		$member_array["get_member_id"] = str_replace ( '#', '', $member_row["member_tag"] );
	}	
	
	$member_ww_sql = "SELECT  * ";
	$member_ww_sql .= " FROM  `API_Mem_WarWeight` ";
	$member_ww_sql .= "WHERE  `memWW_member_tag` LIKE  '" . $member_selected . "' ";
	$member_ww_sql .= "ORDER BY  `memWW_timestamp` DESC ";
	$member_ww_sql .= "LIMIT 1;";
	$member_ww_result = $conn->query($member_ww_sql);

	while($member_ww_row = $member_ww_result->fetch_assoc()) {
		$warWeight['db_unadjusted'] = $member_ww_row['memWW_unadjusted'];
		$warWeight['db_adjusted'] = $member_ww_row['memWW_adjusted'];
		$warWeight['db_penalty'] = $member_ww_row['memWW_penalty'];
		$warWeight['step1'] = $member_ww_row['memWW_Step1'];
		$warWeight['step2'] = $member_ww_row['memWW_Step2'];
		$warWeight['step3_offRatio'] = $member_ww_row['memWW_Step3_OffRatio'];
		$warWeight['step3_defRatio'] = $member_ww_row['memWW_Step3_DefRatio'];
		$warWeight['step3_diffRatio'] = $member_ww_row['memWW_Step3_DiffRatio'];
		$warWeight['step3'] = $member_ww_row['memWW_Step3'];
		$warWeight['step4_diff'] = $member_ww_row['memWW_Step4_Diff'];
		$warWeight['step4'] = $member_ww_row['memWW_Step4'];
		if ( $warWeight['step4'] == NULL ) {
			$warWeight['step4_text'] = 'N/A';
		} elseif ( $warWeight['step4'] == 0 ) {
			$warWeight['step4_text'] = '<font color="green">No</font>';
		} elseif ( $warWeight['step4'] == 1 ) {
			$warWeight['step4_text'] = '<font color="yellow">Low</font>';
		} elseif ( $warWeight['step4'] == 2 ) {
			$warWeight['step4_text'] = '<font color="orange">Medium</font>';
		} elseif ( $warWeight['step4'] == 3 ) {
			$warWeight['step4_text'] = '<font color="red">High</font>';
		}
	}	

	// get info on defensive buildings for current th level
	$temp_thCnt = "def_cnt_th" . $member_array["member_townHallLevel"];
	$member_def_curr_sql = "SELECT `ww_name`, `ww_build_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_def_curr_sql .= "		`def_base`, `def_order`, `" . $temp_thCnt . "`, `def_maxLevel` ";
	$member_def_curr_sql .= "FROM `API_WarWeight` ";
	$member_def_curr_sql .= "INNER JOIN `API_Mem_Defense` ";
	$member_def_curr_sql .= "	ON `ww_build_id` = `def_id` ";
	$member_def_curr_sql .= "WHERE `ww_build_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_def_curr_sql .= "GROUP BY `ww_name`;";
	$member_def_curr_result = $conn->query($member_def_curr_sql);

	while($member_def_curr_row = $member_def_curr_result->fetch_assoc()) {
		$temp_order = $member_def_curr_row["def_order"];
		$defense_array[$temp_order]["name"] = $member_def_curr_row["ww_name"];
		$defense_array[$temp_order]["id"] = $member_def_curr_row["ww_build_id"];
		$defense_array[$temp_order]["currMaxLvl"] = $member_def_curr_row["ww_level"];
		$defense_array[$temp_order]["type"] = $member_def_curr_row["ww_type"];
		$defense_array[$temp_order]["base"] = $member_def_curr_row["def_base"];
		$defense_array[$temp_order]["currMaxCnt"] = $member_def_curr_row[$temp_thCnt];
		$defense_array[$temp_order]["maxLvl"] = $member_def_curr_row["def_maxLevel"];
		if (( $defense_array[$temp_order]["currMaxCnt"] >  $defense_array['maxCnt'] )&&
				( $defense_array[$temp_order]["name"] !== 'Wall' )) {
			$defense_array['maxCnt'] = $defense_array[$temp_order]["currMaxCnt"];
		}
	}			
	// get info on defense buildings for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_def_prev_sql = "SELECT `def_order`, MAX(`ww_level`) AS ww_level ";
	$member_def_prev_sql .= "FROM `API_WarWeight` ";
	$member_def_prev_sql .= "INNER JOIN `API_Mem_Defense` ";
	$member_def_prev_sql .= "	ON `ww_build_id` = `def_id` ";
	$member_def_prev_sql .= "WHERE `ww_build_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_def_prev_sql .= "GROUP BY `def_order`;"; 
	$member_def_prev_result = $conn->query($member_def_prev_sql);

	while($member_def_prev_row = $member_def_prev_result->fetch_assoc()) {
		$temp_order = $member_def_prev_row["def_order"];
		$defense_array[$temp_order]["prevMaxLvl"] = $member_def_prev_row["ww_level"];
	}	  
	
	// get defense building war weight for all levels
	$member_def_ww_sql = "SELECT `def_order`, `ww_level`, `ww_weight` ";
	$member_def_ww_sql .= "FROM `API_WarWeight` ";
	$member_def_ww_sql .= "INNER JOIN `API_Mem_Defense` ";
	$member_def_ww_sql .= "	ON `ww_build_id` = `def_id` ";
	$member_def_ww_sql .= "WHERE `ww_build_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . ";";
	$member_def_ww_result = $conn->query($member_def_ww_sql);
	
	while($member_def_ww_row = $member_def_ww_result->fetch_assoc()) {
		$temp_order = $member_def_ww_row["def_order"];
		$temp_level = $member_def_ww_row["ww_level"];
		$defense_array[$temp_order]["ww"][$temp_level] = $member_def_ww_row["ww_weight"];
	}	  
		
	// get last updated defenses for user
	$member_defense_sql = "SELECT def_order, defToMem_warWeight, ";
	$member_defense_sql .= "   	defToMem_pos1, defToMem_pos2, defToMem_pos3, defToMem_pos4, ";
	$member_defense_sql .= "   	defToMem_pos5, defToMem_pos6, defToMem_pos7, defToMem_pos8, ";
	$member_defense_sql .= "   	defToMem_pos9, defToMem_pos10, defToMem_pos11, defToMem_pos12 ";
	$member_defense_sql .= "FROM `API_Mem_DefToMem` api1 ";
	$member_defense_sql .= "INNER JOIN `API_Mem_Defense` ";
	$member_defense_sql .= "	 ON `defToMem_defense_id` = `def_id` ";
	$member_defense_sql .= "WHERE `defToMem_member_tag` LIKE '" . $member_selected . "' AND ";
	$member_defense_sql .= "  defToMem_timestamp = (SELECT MAX(defToMem_timestamp) ";
	$member_defense_sql .= "				FROM API_Mem_DefToMem api2 ";
	$member_defense_sql .= "				WHERE api1.`defToMem_member_tag` LIKE api2.`defToMem_member_tag` ";
	$member_defense_sql .= "               AND api1.`defToMem_defense_id` LIKE api2.`defToMem_defense_id`) ";
	$member_defense_sql .= "GROUP BY `def_order`;";
	$member_defense_result = $conn->query($member_defense_sql);

	while($member_defense_row = $member_defense_result->fetch_assoc()) {
		$temp_order= $member_defense_row["def_order"];
		$temp_thCnt = $defense_array[$temp_order]["currMaxCnt"];
		$temp_ww = 0;
	
		// if walls
		if ( $temp_order == 100 ) {
			for ( $i = 1; $i <= 12; $i++ ) {
				$temp_cnt = $member_defense_row["defToMem_pos" . $i];
				$defense_array[$temp_order]["cnt"]['total'] = $defense_array[$temp_order]["cnt"]['total'] + $temp_cnt;
				$defense_array[$temp_order]["cnt"][$i] = $temp_cnt;
				$temp_ww += $defense_array[$temp_order]["ww"][$i] * $temp_cnt;
			}
			$defense_array[$temp_order]["warWeight"] = $temp_ww;
			$warWeight['wall_defense'] += $temp_ww;
			$warWeight['def'] += $temp_ww;
		} else {
			for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
				$temp_level = $member_defense_row["defToMem_pos" . $i];
				$defense_array[$temp_order]["level"][$i] = $temp_level;
				$defense_array[$temp_order]["warWeight"] = $defense_array[$temp_order]["warWeight"] + $temp_ww[$temp_level];
				$temp_ww += $defense_array[$temp_order]["ww"][$temp_level];
			}
			$defense_array[$temp_order]["warWeight"] = $temp_ww;
			$warWeight['bldg_defense'] += $temp_ww;
			$warWeight['def'] += $temp_ww;
		}
	}
	
	// get info on traps for current th level
	$temp_thCnt = "trap_cnt_th" . $member_array["member_townHallLevel"];
	$member_trap_curr_sql = "SELECT `ww_name`, `ww_trap_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_trap_curr_sql .= "		`trap_base`, `trap_order`, `" . $temp_thCnt . "`, `trap_maxLevel` ";
	$member_trap_curr_sql .= "FROM `API_WarWeight` ";
	$member_trap_curr_sql .= "INNER JOIN `API_Mem_Traps` ";
	$member_trap_curr_sql .= "	ON `ww_trap_id` = `trap_id` ";
	$member_trap_curr_sql .= "WHERE `ww_trap_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_trap_curr_sql .= "GROUP BY `ww_name`;";
	$member_trap_curr_result = $conn->query($member_trap_curr_sql);

	while($member_trap_curr_row = $member_trap_curr_result->fetch_assoc()) {
		$temp_order = $member_trap_curr_row["trap_order"];
		$traps_array[$temp_order]["name"] = $member_trap_curr_row["ww_name"];
		$traps_array[$temp_order]["id"] = $member_trap_curr_row["ww_trap_id"];
		$traps_array[$temp_order]["currMaxLvl"] = $member_trap_curr_row["ww_level"];
		$traps_array[$temp_order]["type"] = $member_trap_curr_row["ww_type"];
		$traps_array[$temp_order]["base"] = $member_trap_curr_row["trap_base"];
		$traps_array[$temp_order]["currMaxCnt"] = $member_trap_curr_row[$temp_thCnt];
		$traps_array[$temp_order]["maxLvl"] = $member_trap_curr_row["trap_maxLevel"];
		if ( $traps_array[$temp_order]["currMaxCnt"] >  $traps_array['maxCnt'] ) {
			$traps_array['maxCnt'] = $traps_array[$temp_order]["currMaxCnt"];
		}
	}			
	
	// get info on traps for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_trap_prev_sql = "SELECT `trap_order`, MAX(`ww_level`) AS ww_level ";
	$member_trap_prev_sql .= "FROM `API_WarWeight` ";
	$member_trap_prev_sql .= "INNER JOIN `API_Mem_Traps` ";
	$member_trap_prev_sql .= "	ON `ww_trap_id` = `trap_id` ";
	$member_trap_prev_sql .= "WHERE `ww_trap_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_trap_prev_sql .= "GROUP BY `trap_order`;"; 
	$member_trap_prev_result = $conn->query($member_trap_prev_sql);

	while($member_trap_prev_row = $member_trap_prev_result->fetch_assoc()) {
		$temp_order = $member_trap_prev_row["trap_order"];
		$traps_array[$temp_order]["prevMaxLvl"] = $member_trap_prev_row["ww_level"];
	}	  
	
	// get traps war weight for all levels
	$member_trap_ww_sql = "SELECT `trap_order`, `ww_level`, `ww_weight` ";
	$member_trap_ww_sql .= "FROM `API_WarWeight` ";
	$member_trap_ww_sql .= "INNER JOIN `API_Mem_Traps` ";
	$member_trap_ww_sql .= "	ON `ww_trap_id` = `trap_id` ";
	$member_trap_ww_sql .= "WHERE `ww_trap_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . ";";
	$member_trap_ww_result = $conn->query($member_trap_ww_sql);
	
	while($member_trap_ww_row = $member_trap_ww_result->fetch_assoc()) {
		$temp_order = $member_trap_ww_row["trap_order"];
		$temp_level = $member_trap_ww_row["ww_level"];
		$traps_array[$temp_order]["ww"][$temp_level] = $member_trap_ww_row["ww_weight"];
	}	  
		
	// get last updated traps for user
	$member_traps_sql = "SELECT trap_order, trapToMem_warWeight, ";
	$member_traps_sql .= "   trapToMem_pos1, trapToMem_pos2, trapToMem_pos3, trapToMem_pos4, ";
	$member_traps_sql .= "   trapToMem_pos5, trapToMem_pos6 ";
	$member_traps_sql .= "FROM `API_Mem_TrapToMem` api1 ";
	$member_traps_sql .= "INNER JOIN `API_Mem_Traps` ";
	$member_traps_sql .= "	ON `trapToMem_trap_id` = `trap_id` ";
	$member_traps_sql .= "WHERE `trapToMem_member_tag` LIKE '" . $member_selected . "' AND ";
	$member_traps_sql .= "  trapToMem_timestamp = (SELECT MAX(trapToMem_timestamp) ";
	$member_traps_sql .= "               FROM API_Mem_TrapToMem api2 ";
	$member_traps_sql .= "               WHERE api1.`trapToMem_member_tag` LIKE api2.`trapToMem_member_tag` ";
	$member_traps_sql .= "               AND api1.`trapToMem_trap_id` LIKE api2.`trapToMem_trap_id`) ";
	$member_traps_sql .= "GROUP BY `trap_order`;"; 
	$member_traps_result = $conn->query($member_traps_sql);

	while($member_traps_row = $member_traps_result->fetch_assoc()) {
		$temp_order= $member_traps_row["trap_order"];
		$temp_thCnt = $traps_array[$temp_order]["currMaxCnt"];
		$temp_ww = 0;
		for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
			$temp_level = $member_traps_row["trapToMem_pos" . $i];
			$traps_array[$temp_order]["level"][$i] = $temp_level;
			$temp_ww += $traps_array[$temp_order]["ww"][$temp_level];
		}
		$traps_array[$temp_order]["warWeight"] = $temp_ww;
		$warWeight['traps'] += $traps_array[$temp_order]["warWeight"];
		$warWeight['def'] += $traps_array[$temp_order]["warWeight"];
	}

	// get info on misc buildings for current th level
	$temp_thCnt = "misc_cnt_th" . $member_array["member_townHallLevel"];
	$member_misc_curr_sql = "SELECT `ww_name`, `ww_misc_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_misc_curr_sql .= "		`misc_base`, `misc_order`, `" . $temp_thCnt . "`, `misc_maxLevel` ";
	$member_misc_curr_sql .= "FROM `API_WarWeight` ";
	$member_misc_curr_sql .= "INNER JOIN `API_Mem_Misc` ";
	$member_misc_curr_sql .= "	ON `ww_misc_id` = `misc_id` ";
	$member_misc_curr_sql .= "WHERE `ww_misc_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_misc_curr_sql .= "GROUP BY `ww_name`;";
	$member_misc_curr_result = $conn->query($member_misc_curr_sql);

	while($member_misc_curr_row = $member_misc_curr_result->fetch_assoc()) {
		$temp_order = $member_misc_curr_row["misc_order"];
		$misc_array[$temp_order]["name"] = $member_misc_curr_row["ww_name"];
		$misc_array[$temp_order]["id"] = $member_misc_curr_row["ww_misc_id"];
		$misc_array[$temp_order]["currMaxLvl"] = $member_misc_curr_row["ww_level"];
		$misc_array[$temp_order]["type"] = $member_misc_curr_row["ww_type"];
		$misc_array[$temp_order]["base"] = $member_misc_curr_row["misc_base"];
		$misc_array[$temp_order]["currMaxCnt"] = $member_misc_curr_row[$temp_thCnt];
		$misc_array[$temp_order]["maxLvl"] = $member_misc_curr_row["misc_maxLevel"];
		if ( $misc_array[$temp_order]["currMaxCnt"] >  $misc_array['maxCnt'] )  {
			$misc_array['maxCnt'] = $misc_array[$temp_order]["currMaxCnt"];
		}
	}			
	
	// get info on misc buildings for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_misc_prev_sql = "SELECT `misc_order`, MAX(`ww_level`) AS ww_level ";
	$member_misc_prev_sql .= "FROM `API_WarWeight` ";
	$member_misc_prev_sql .= "INNER JOIN `API_Mem_Misc` ";
	$member_misc_prev_sql .= "	ON `ww_misc_id` = `misc_id` ";
	$member_misc_prev_sql .= "WHERE `ww_misc_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_misc_prev_sql .= "GROUP BY `misc_order`;"; 
	$member_misc_prev_result = $conn->query($member_misc_prev_sql);

	while($member_misc_prev_row = $member_misc_prev_result->fetch_assoc()) {
		$temp_order = $member_misc_prev_row["misc_order"];
		$misc_array[$temp_order]["prevMaxLvl"] = $member_misc_prev_row["ww_level"];
	}	  
	
	// get misc building war weight for all levels
	$member_misc_ww_sql = "SELECT `misc_order`, `ww_level`, `ww_weight` ";
	$member_misc_ww_sql .= "FROM `API_WarWeight` ";
	$member_misc_ww_sql .= "INNER JOIN `API_Mem_Misc` ";
	$member_misc_ww_sql .= "	ON `ww_misc_id` = `misc_id` ";
	$member_misc_ww_sql .= "WHERE `ww_misc_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . ";";
	$member_misc_ww_result = $conn->query($member_misc_ww_sql);
	
	while($member_misc_ww_row = $member_misc_ww_result->fetch_assoc()) {
		$temp_order = $member_misc_ww_row["misc_order"];
		$temp_level = $member_misc_ww_row["ww_level"];
		$misc_array[$temp_order]["ww"][$temp_level] = $member_misc_ww_row["ww_weight"];
	}	  

	// get last updated misc build for user
	$member_misc_sql = "SELECT misc_order, miscToMem_warWeight, ";
	$member_misc_sql .= "   miscToMem_pos1, miscToMem_pos2, miscToMem_pos3, miscToMem_pos4, ";
	$member_misc_sql .= "   miscToMem_pos5, miscToMem_pos6, miscToMem_pos7 ";
	$member_misc_sql .= "FROM `API_Mem_MiscToMem` api1 ";
	$member_misc_sql .= "INNER JOIN `API_Mem_Misc` ";
	$member_misc_sql .= "	ON `miscToMem_misc_id` = `misc_id` ";
	$member_misc_sql .= "WHERE `miscToMem_member_tag` LIKE '" . $member_selected . "' AND ";
	$member_misc_sql .= "  miscToMem_timestamp = (SELECT MAX(miscToMem_timestamp) ";
	$member_misc_sql .= "               FROM API_Mem_MiscToMem api2 ";
	$member_misc_sql .= "               WHERE api1.`miscToMem_member_tag` LIKE api2.`miscToMem_member_tag` ";
	$member_misc_sql .= "               AND api1.`miscToMem_misc_id` LIKE api2.`miscToMem_misc_id`) ";
	$member_misc_sql .= "GROUP BY `misc_order`;"; 
	$member_misc_result = $conn->query($member_misc_sql);

	while($member_misc_row = $member_misc_result->fetch_assoc()) {
		$temp_order= $member_misc_row["misc_order"];
		$temp_thCnt = $misc_array[$temp_order]["currMaxCnt"];
		$temp_ww = 0;
		for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
			$temp_level = $member_misc_row["miscToMem_pos" . $i];
			$misc_array[$temp_order]["level"][$i] = $temp_level;
			$temp_ww += $misc_array[$temp_order]["ww"][$temp_level];
		}
		$misc_array[$temp_order]["warWeight"] = $temp_ww;
		$warWeight['misc'] += $misc_array[$temp_order]["warWeight"];
		$warWeight['def'] += $misc_array[$temp_order]["warWeight"];
	}

	// get info on nonWt buildings for current th level
	$temp_thCnt = "nonWt_cnt_th" . $member_array["member_townHallLevel"];
	$member_nonWt_curr_sql = "SELECT `ww_name`, `ww_nonWt_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_nonWt_curr_sql .= "		`nonWt_base`, `nonWt_order`, `" . $temp_thCnt . "`, `nonWt_maxLevel` ";
	$member_nonWt_curr_sql .= "FROM `API_WarWeight` ";
	$member_nonWt_curr_sql .= "INNER JOIN `API_Mem_NonWt` ";
	$member_nonWt_curr_sql .= "	ON `ww_nonWt_id` = `nonWt_id` ";
	$member_nonWt_curr_sql .= "WHERE `ww_nonWt_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_nonWt_curr_sql .= "GROUP BY `ww_name`;";
	$member_nonWt_curr_result = $conn->query($member_nonWt_curr_sql);
	
	while($member_nonWt_curr_row = $member_nonWt_curr_result->fetch_assoc()) {
		$temp_order = $member_nonWt_curr_row["nonWt_order"];
		$nonWt_array[$temp_order]["name"] = $member_nonWt_curr_row["ww_name"];
		$nonWt_array[$temp_order]["id"] = $member_nonWt_curr_row["ww_nonWt_id"];
		$nonWt_array[$temp_order]["currMaxLvl"] = $member_nonWt_curr_row["ww_level"];
		$nonWt_array[$temp_order]["type"] = $member_nonWt_curr_row["ww_type"];
		$nonWt_array[$temp_order]["base"] = $member_nonWt_curr_row["nonWt_base"];
		$nonWt_array[$temp_order]["currMaxCnt"] = $member_nonWt_curr_row[$temp_thCnt];
		$nonWt_array[$temp_order]["maxLvl"] = $member_nonWt_curr_row["nonWt_maxLevel"];
		if ( $nonWt_array[$temp_order]["currMaxCnt"] >  $nonWt_array['maxCnt'] ) {
			$nonWt_array['maxCnt'] = $nonWt_array[$temp_order]["currMaxCnt"];
		}
	}			

	// get info on nonWt buildings for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_nonWt_prev_sql = "SELECT `nonWt_order`, MAX(`ww_level`) AS ww_level ";
	$member_nonWt_prev_sql .= "FROM `API_WarWeight` ";
	$member_nonWt_prev_sql .= "INNER JOIN `API_Mem_NonWt` ";
	$member_nonWt_prev_sql .= "	ON `ww_nonWt_id` = `nonWt_id` ";
	$member_nonWt_prev_sql .= "WHERE `ww_nonWt_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_nonWt_prev_sql .= "GROUP BY `nonWt_order`;"; 
	$member_nonWt_prev_result = $conn->query($member_nonWt_prev_sql);
	
	while($member_nonWt_prev_row = $member_nonWt_prev_result->fetch_assoc()) {
		$temp_order = $member_nonWt_prev_row["nonWt_order"];
		$nonWt_array[$temp_order]["prevMaxLvl"] = $member_nonWt_prev_row["ww_level"];
	}	  
	
	// get current monWt building levels
	$member_nonWt_sql = "SELECT `nonWt_order`, ";
	$member_nonWt_sql .= "   nonWtToMem_pos1, nonWtToMem_pos2, nonWtToMem_pos3, nonWtToMem_pos4, ";
	$member_nonWt_sql .= "   nonWtToMem_pos5 ";
	$member_nonWt_sql .= "FROM `API_Mem_NonWtToMem` api1 ";
	$member_nonWt_sql .= "INNER JOIN `API_Mem_NonWt` ";
	$member_nonWt_sql .= " ON `nonWtToMem_nonWt_id` = `nonWt_id` ";
	$member_nonWt_sql .= "WHERE `nonWtToMem_member_tag` LIKE '" . $member_selected . "' AND ";
	$member_nonWt_sql .= "  nonWtToMem_timestamp = (SELECT MAX(nonWtToMem_timestamp) ";
	$member_nonWt_sql .= "               FROM API_Mem_NonWtToMem api2 ";
	$member_nonWt_sql .= "               WHERE api1.`nonWtToMem_member_tag` LIKE api2.`nonWtToMem_member_tag` ";
	$member_nonWt_sql .= "               AND api1.`nonWtToMem_nonWt_id` LIKE api2.`nonWtToMem_nonWt_id`) ";
	$member_nonWt_sql .= "GROUP BY `nonWtToMem_nonWt_id`;";
	$member_nonWt_result = $conn->query($member_nonWt_sql);
	
	while($member_nonWt_row = $member_nonWt_result->fetch_assoc()) {
		$temp_order= $member_nonWt_row["nonWt_order"];
		$nonWt_array[$temp_order]["level"]["1"] = $member_nonWt_row["nonWtToMem_pos1"];
		$nonWt_array[$temp_order]["level"]["2"] = $member_nonWt_row["nonWtToMem_pos2"];
		$nonWt_array[$temp_order]["level"]["3"] = $member_nonWt_row["nonWtToMem_pos3"];
		$nonWt_array[$temp_order]["level"]["4"] = $member_nonWt_row["nonWtToMem_pos4"];
		$nonWt_array[$temp_order]["level"]["5"] = $member_nonWt_row["nonWtToMem_pos5"];
	}	  
			
	// get info on heros for current th level
	$member_hero_curr_sql = "SELECT `ww_name`, `ww_heros_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_hero_curr_sql .= "		`hero_base`, `hero_order`, `hero_maxLevel` ";
	$member_hero_curr_sql .= "FROM `API_WarWeight` ";
	$member_hero_curr_sql .= "INNER JOIN `API_Mem_Heros` ";
	$member_hero_curr_sql .= "	ON `ww_heros_id` = `hero_id` ";
	$member_hero_curr_sql .= "WHERE `ww_heros_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_hero_curr_sql .= "  AND `hero_base` = 1 ";
	$member_hero_curr_sql .= "GROUP BY `ww_name`;";
	$member_hero_curr_result = $conn->query($member_hero_curr_sql);

	while($member_hero_curr_row = $member_hero_curr_result->fetch_assoc()) {
		$temp_order = $member_hero_curr_row["hero_order"];
		$heros_array[$temp_order]["name"] = $member_hero_curr_row["ww_name"];
		$heros_array[$temp_order]["id"] = $member_hero_curr_row["ww_heros_id"];
		$heros_array[$temp_order]["currMaxLvl"] = $member_hero_curr_row["ww_level"];
		$heros_array[$temp_order]["type"] = $member_hero_curr_row["ww_type"];
		$heros_array[$temp_order]["base"] = $member_hero_curr_row["hero_base"];
		$heros_array[$temp_order]["maxLvl"] = $member_hero_curr_row["hero_maxLevel"];
	}			

	// get info on heros for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_hero_prev_sql = "SELECT `hero_order`, MAX(`ww_level`) AS ww_level ";
	$member_hero_prev_sql .= "FROM `API_WarWeight` ";
	$member_hero_prev_sql .= "INNER JOIN `API_Mem_Heros` ";
	$member_hero_prev_sql .= "	ON `ww_heros_id` = `hero_id` ";
	$member_hero_prev_sql .= "WHERE `ww_heros_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_hero_prev_sql .= "  AND `hero_base` = 1 ";
	$member_hero_prev_sql .= "GROUP BY `hero_order`;"; 
	$member_hero_prev_result = $conn->query($member_hero_prev_sql);

	while($member_hero_prev_row = $member_hero_prev_result->fetch_assoc()) {
		$temp_order = $member_hero_prev_row["hero_order"];
		$heros_array[$temp_order]["prevMaxLvl"] = $member_hero_prev_row["ww_level"];
	}	  
	
	// get heros war weight for all levels
	$member_hero_ww_sql = "SELECT `hero_order`, `ww_level`, `ww_weight`, `ww_weight_def` ";
	$member_hero_ww_sql .= "FROM `API_WarWeight` ";
	$member_hero_ww_sql .= "INNER JOIN `API_Mem_Heros` ";
	$member_hero_ww_sql .= "	ON `ww_heros_id` = `hero_id` ";
	$member_hero_ww_sql .= "WHERE `ww_heros_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_hero_ww_sql .= "  AND `hero_base` = 1;";
	$member_hero_ww_result = $conn->query($member_hero_ww_sql);
	
	while($member_hero_ww_row = $member_hero_ww_result->fetch_assoc()) {
		$temp_order = $member_hero_ww_row["hero_order"];
		$temp_level = $member_hero_ww_row["ww_level"];
		$heros_array[$temp_order]["ww_off"][$temp_level] = $member_hero_ww_row["ww_weight"];
		$heros_array[$temp_order]["ww_def"][$temp_level] = $member_hero_ww_row["ww_weight_def"];
	}	  

	// get last updated heros for user
	$member_heros_sql = "SELECT hero_order, heroToMem_warWeight, heroToMem_level ";
	$member_heros_sql .= "FROM `API_Mem_HerosToMem` api1 ";
	$member_heros_sql .= "INNER JOIN `API_Mem_Heros` ";
	$member_heros_sql .= "	ON `heroToMem_heros_id` = `hero_id` ";
	$member_heros_sql .= "WHERE `heroToMem_member_tag` LIKE '" . $member_selected . "' AND ";
	$member_heros_sql .= "  `hero_base` = 1 AND ";
	$member_heros_sql .= "  heroToMem_timestamp = (SELECT MAX(heroToMem_timestamp) ";
	$member_heros_sql .= "               FROM API_Mem_HerosToMem api2 ";
	$member_heros_sql .= "               WHERE api1.`heroToMem_member_tag` LIKE api2.`heroToMem_member_tag` ";
	$member_heros_sql .= "               AND api1.`heroToMem_heros_id` LIKE api2.`heroToMem_heros_id`) ";
	$member_heros_sql .= "GROUP BY `hero_order`;"; 
	$member_heros_result = $conn->query($member_heros_sql);

	while($member_heros_row = $member_heros_result->fetch_assoc()) {
		$temp_order = $member_heros_row["hero_order"];
		$temp_level = $member_heros_row["heroToMem_level"];
		$heros_array[$temp_order]["warWeight_off"] = $heros_array[$temp_order]["ww_off"][$temp_level];
		$heros_array[$temp_order]["warWeight_def"] = $heros_array[$temp_order]["ww_def"][$temp_level];
		$warWeight['heros']['off'] += $heros_array[$temp_order]["warWeight_off"];
		$warWeight['heros']['def'] += $heros_array[$temp_order]["warWeight_def"];
		$warWeight['off'] += $heros_array[$temp_order]["warWeight_off"];
		$warWeight['def'] += $heros_array[$temp_order]["warWeight_def"];
		$heros_array[$temp_order]["level"] = $temp_level;
	}

	// get info on troops for current th level
	$member_troop_curr_sql = "SELECT `ww_name`, `ww_troops_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_troop_curr_sql .= "		`troops_base`, `troops_order`, `troops_maxLevel` ";
	$member_troop_curr_sql .= "FROM `API_WarWeight` ";
	$member_troop_curr_sql .= "INNER JOIN `API_Mem_Troops` ";
	$member_troop_curr_sql .= "	ON `ww_troops_id` = `troops_id` ";
	$member_troop_curr_sql .= "WHERE `ww_troops_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_troop_curr_sql .= "  AND `troops_base` = 1 ";
	$member_troop_curr_sql .= "GROUP BY `ww_name`;";
	$member_troop_curr_result = $conn->query($member_troop_curr_sql);

	while($member_troop_curr_row = $member_troop_curr_result->fetch_assoc()) {
		$temp_order = $member_troop_curr_row["troops_order"];
		$troops_array[$temp_order]["name"] = $member_troop_curr_row["ww_name"];
		$troops_array[$temp_order]["id"] = $member_troop_curr_row["ww_troops_id"];
		$troops_array[$temp_order]["currMaxLvl"] = $member_troop_curr_row["ww_level"];
		$troops_array[$temp_order]["type"] = $member_troop_curr_row["ww_type"];
		$troops_array[$temp_order]["base"] = $member_troop_curr_row["troops_base"];
		$troops_array[$temp_order]["maxLvl"] = $member_troop_curr_row["troops_maxLevel"];
	}			

	// get info on troops for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_troop_prev_sql = "SELECT `troops_order`, MAX(`ww_level`) AS ww_level ";
	$member_troop_prev_sql .= "FROM `API_WarWeight` ";
	$member_troop_prev_sql .= "INNER JOIN `API_Mem_Troops` ";
	$member_troop_prev_sql .= "	ON `ww_troops_id` = `troops_id` ";
	$member_troop_prev_sql .= "WHERE `ww_troops_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_troop_prev_sql .= "  AND `troops_base` = 1 ";
	$member_troop_prev_sql .= "GROUP BY `troops_order`;"; 
	$member_troop_prev_result = $conn->query($member_troop_prev_sql);

	while($member_troop_prev_row = $member_troop_prev_result->fetch_assoc()) {
		$temp_order = $member_troop_prev_row["troops_order"];
		$troops_array[$temp_order]["prevMaxLvl"] = $member_troop_prev_row["ww_level"];
	}	  

	// get troops war weight for all levels
	$member_troop_ww_sql = "SELECT `troops_order`, `ww_level`, `ww_weight` ";
	$member_troop_ww_sql .= "FROM `API_WarWeight` ";
	$member_troop_ww_sql .= "INNER JOIN `API_Mem_Troops` ";
	$member_troop_ww_sql .= "	ON `ww_troops_id` = `troops_id` ";
	$member_troop_ww_sql .= "WHERE `ww_troops_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_troop_ww_sql .= "  AND `troops_base` = 1;";
	$member_troop_ww_result = $conn->query($member_troop_ww_sql);
	
	while($member_troop_ww_row = $member_troop_ww_result->fetch_assoc()) {
		$temp_order = $member_troop_ww_row["troops_order"];
		$temp_level = $member_troop_ww_row["ww_level"];
		$troops_array[$temp_order]["ww"][$temp_level] = $member_troop_ww_row["ww_weight"];
	}	  

	// get last updated troops for user
	$member_troops_sql = "SELECT troops_order, troopsToMem_warWeight, troopsToMem_level ";
	$member_troops_sql .= "FROM `API_Mem_TroopsToMem` api1 ";
	$member_troops_sql .= "INNER JOIN `API_Mem_Troops` ";
	$member_troops_sql .= "	ON `troopsToMem_troop_id` = `troops_id` ";
	$member_troops_sql .= "WHERE `troopsToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_troops_sql .= "  AND `troops_base` = 1 AND ";
	$member_troops_sql .= "  troopsToMem_timestamp = (SELECT MAX(troopsToMem_timestamp) ";
	$member_troops_sql .= "               FROM API_Mem_TroopsToMem api2 ";
	$member_troops_sql .= "               WHERE api1.`troopsToMem_member_tag` LIKE api2.`troopsToMem_member_tag` ";
	$member_troops_sql .= "               AND api1.`troopsToMem_troop_id` LIKE api2.`troopsToMem_troop_id`) ";
	$member_troops_sql .= "GROUP BY `troops_order`;"; 
	$member_troops_result = $conn->query($member_troops_sql);

	while($member_troops_row = $member_troops_result->fetch_assoc()) {
		$temp_order = $member_troops_row["troops_order"];
		$temp_level = $member_troops_row["troopsToMem_level"];
		$temp_ww = $troops_array[$temp_order]["ww"][$temp_level];
		$troops_array[$temp_order]["level"] = $temp_level;
		$troops_array[$temp_order]["warWeight"] = $temp_ww;
		$warWeight['troops'] += $temp_ww;
		if ( $temp_order < 100 ) {
			$warWeight['elixir_troops'] += $temp_ww;
			$warWeight['off'] += $temp_ww;
		} else {
			$warWeight['de_troops'] += $temp_ww;
			$warWeight['off'] += $temp_ww;
		}
	}

	// get info on spells for current th level
	$member_spell_curr_sql = "SELECT `ww_name`, `ww_spells_id`, MAX(`ww_level`) AS ww_level, `ww_type`, ";
	$member_spell_curr_sql .= "		`spells_base`, `spells_order`, `spells_maxLevel` ";
	$member_spell_curr_sql .= "FROM `API_WarWeight` ";
	$member_spell_curr_sql .= "INNER JOIN `API_Mem_Spells` ";
	$member_spell_curr_sql .= "	ON `ww_spells_id` = `spells_id` ";
	$member_spell_curr_sql .= "WHERE `ww_spells_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_spell_curr_sql .= "  AND `spells_base` = 1 ";
	$member_spell_curr_sql .= "GROUP BY `ww_name`;";
	$member_spell_curr_result = $conn->query($member_spell_curr_sql);

	while($member_spell_curr_row = $member_spell_curr_result->fetch_assoc()) {
		$temp_order = $member_spell_curr_row["spells_order"];
		$spells_array[$temp_order]["name"] = $member_spell_curr_row["ww_name"];
		$spells_array[$temp_order]["id"] = $member_spell_curr_row["ww_spells_id"];
		$spells_array[$temp_order]["currMaxLvl"] = $member_spell_curr_row["ww_level"];
		$spells_array[$temp_order]["type"] = $member_spell_curr_row["ww_type"];
		$spells_array[$temp_order]["base"] = $member_spell_curr_row["spells_base"];
		$spells_array[$temp_order]["maxLvl"] = $member_spell_curr_row["spells_maxLevel"];
	}			

	// get info on spells for previous th level
	$temp_thLevel = $member_array["member_townHallLevel"] - 1;
	$member_spell_prev_sql = "SELECT `spells_order`, MAX(`ww_level`) AS ww_level ";
	$member_spell_prev_sql .= "FROM `API_WarWeight` ";
	$member_spell_prev_sql .= "INNER JOIN `API_Mem_Spells` ";
	$member_spell_prev_sql .= "	ON `ww_spells_id` = `spells_id` ";
	$member_spell_prev_sql .= "WHERE `ww_spells_id` IS NOT NULL AND `ww_thLevel` <= " . $temp_thLevel . " ";
	$member_spell_prev_sql .= "  AND `spells_base` = 1 ";
	$member_spell_prev_sql .= "GROUP BY `spells_order`;"; 
	$member_spell_prev_result = $conn->query($member_spell_prev_sql);

	while($member_spell_prev_row = $member_spell_prev_result->fetch_assoc()) {
		$temp_order = $member_spell_prev_row["spells_order"];
		$spells_array[$temp_order]["prevMaxLvl"] = $member_spell_prev_row["ww_level"];
	}	  
	
	// get spells war weight for all levels
	$member_spell_ww_sql = "SELECT `spells_order`, `ww_level`, `ww_weight` ";
	$member_spell_ww_sql .= "FROM `API_WarWeight` ";
	$member_spell_ww_sql .= "INNER JOIN `API_Mem_Spells` ";
	$member_spell_ww_sql .= "	ON `ww_spells_id` = `spells_id` ";
	$member_spell_ww_sql .= "WHERE `ww_spells_id` IS NOT NULL AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_spell_ww_sql .= "  AND `spells_base` = 1;";
	$member_spell_ww_result = $conn->query($member_spell_ww_sql);
	
	while($member_spell_ww_row = $member_spell_ww_result->fetch_assoc()) {
		$temp_order = $member_spell_ww_row["spells_order"];
		$temp_level = $member_spell_ww_row["ww_level"];
		$spells_array[$temp_order]["ww"][$temp_level] = $member_spell_ww_row["ww_weight"];
	}	  

	// get last updated spells for user
	$member_spells_sql = "SELECT spells_order, spellsToMem_warWeight, spellsToMem_level ";
	$member_spells_sql .= "FROM `API_Mem_SpellsToMem` api1 ";
	$member_spells_sql .= "INNER JOIN `API_Mem_Spells` ";
	$member_spells_sql .= "	ON `spellsToMem_spells_id` = `spells_id` ";
	$member_spells_sql .= "WHERE `spellsToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_spells_sql .= "  AND `spells_base` = 1 AND ";
	$member_spells_sql .= "  spellsToMem_timestamp = (SELECT MAX(spellsToMem_timestamp) ";
	$member_spells_sql .= "               FROM API_Mem_SpellsToMem api2 ";
	$member_spells_sql .= "               WHERE api1.`spellsToMem_member_tag` LIKE api2.`spellsToMem_member_tag` ";
	$member_spells_sql .= "               AND api1.`spellsToMem_spells_id` LIKE api2.`spellsToMem_spells_id`) ";
	$member_spells_sql .= "GROUP BY `spells_order`;"; 
	$member_spells_result = $conn->query($member_spells_sql);

	while($member_spells_row = $member_spells_result->fetch_assoc()) {
		$temp_order= $member_spells_row["spells_order"];
		$temp_level = $member_spells_row["spellsToMem_level"];
		$temp_ww = $spells_array[$temp_order]["ww"][$temp_level];
		$spells_array[$temp_order]["level"] = $temp_level;
		$spells_array[$temp_order]["warWeight"] = $temp_ww;
		$warWeight['spells'] += $temp_ww;
		$warWeight['off'] += $temp_ww;
	}
	
	// get war weight for all th levels
	$warWeight_TH_sql = "SELECT * ";
	$warWeight_TH_sql .= " FROM `API_WarWeight_TH` ";
	$warWeight_TH_sql .= " ORDER BY `TH` ASC;";
	$warWeight_TH_result = $conn->query($warWeight_TH_sql);
	
	while($warWeight_TH_row = $warWeight_TH_result->fetch_assoc()) {
		$i = $warWeight_TH_row['TH'];
		$warWeight[$i]['WarWeight_Min'] = $warWeight_TH_row['WarWeight_Min'];
		$warWeight[$i]['WarWeight_Median'] = $warWeight_TH_row['WarWeight_Median'];
		$warWeight[$i]['WarWeight_Max'] = $warWeight_TH_row['WarWeight_Max'];
		$warWeight[$i]['DefLvlperTh'] = $warWeight_TH_row['WarWeight_DefLvlperTh'];
		$warWeight[$i]['OffLvlperTH'] = $warWeight_TH_row['WarWeight_OffLvlperTH'];
}

	// compute total and adjusted war weights
	$warWeight['total'] = $warWeight['off'] + $warWeight['def'];
	// check if bldgs are entered
	if ( $warWeight['bldg_defense'] == 0 ) {
		// set rounded weight to 0
		$warWeight['rounded'] = 0;
	} else {
		// compute rounded weight
		$warWeight['rounded'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;
	}
	$warWeight['rounded'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;
	
	// check if first time threw
	if ( $intialize == 0 ) {
		// merge arrays for Engineered update
		$items_array = array_merge ( $defense_array, $traps_array, $misc_array, $nonWt_array, $heros_array, $troops_array, $spells_array );
		UpdateEngineered ( $member_selected, $member_array["member_townHallLevel"], $warWeight, $items_array, $conn );
		$intialize = 1;
	}
	
	if ($_POST['action'] == 'Update') {
		UpdateDefArray( $defense_array, $member_array, $warWeight, $conn );
		UpdateTrapArray( $traps_array, $member_array, $warWeight, $conn );
		UpdateMiscArray( $misc_array, $member_array, $warWeight, $conn );
		UpdateNonWtArray( $nonWt_array, $member_array, $conn );

		// merge arrays for Engineered update
		$items_array = array_merge ( $defense_array, $traps_array, $misc_array, $nonWt_array, $heros_array, $troops_array, $spells_array );
		UpdateEngineered ( $member_selected, $member_array["member_townHallLevel"], $warWeight, $items_array, $conn );

	} else if ($_POST['action'] == 'Save') {
		$error = UpdateDefArray( $defense_array, $member_array, $warWeight, $conn );
		$error = $error + UpdateTrapArray( $traps_array, $member_array, $warWeight, $conn );
		$error = $error + UpdateMiscArray( $misc_array, $member_array, $warWeight, $conn );
		$error = $error + UpdateNonWtArray( $nonWt_array, $member_array, $conn );

		// merge arrays for Engineered update
		$items_array = array_merge ( $defense_array, $traps_array, $misc_array, $heros_array, $troops_array, $spells_array );
		UpdateEngineered ( $member_selected, $member_array["member_townHallLevel"], $warWeight, $items_array, $conn );

		if ( $error == 0 ) {

			// save def builds (only 8 pos for def builds, 12 for walls)
			$insert_def_sql = "INSERT INTO  `thebl962_jumi`.`API_Mem_DefToMem` (";
			$insert_def_sql .= "`defToMem_member_tag`, `defToMem_defense_name`, ";
			$insert_def_sql .= "`defToMem_defense_id`, `defToMem_timestamp`, ";
			$insert_def_sql .= "`defToMem_pos1`, `defToMem_pos2`, `defToMem_pos3`, `defToMem_pos4`, ";
			$insert_def_sql .= "`defToMem_pos5`, `defToMem_pos6`, `defToMem_pos7`, `defToMem_pos8`) ";
			$insert_def_sql .= "VALUES ";
			for ( $i = 1; $i < 100; $i++ ) {
				if ( !empty ( $defense_array[$i]["name"] )) {
					if ( $i > 1 ) {
						$insert_def_sql .= ", ";
					}
					$insert_def_sql .= "('" . $member_selected . "',  '" . $defense_array[$i]["name"] . "'";
					$insert_def_sql .= ", '" . $defense_array[$i]["id"] . "',  '" . $utc_str . "'";
					for ( $j = 1; $j <= 8; $j++ ) {
						if ( !empty ( $defense_array[$i]["level"][$j] )) {
							$insert_def_sql .= ", '" . $defense_array[$i]["level"][$j] . "'";
						} else {
							$insert_def_sql .= ", NULL";
						}
					}
					$insert_def_sql .= ")";
				}
			}
			$insert_def_sql .= ";";

			if ($conn->query($insert_def_sql) === TRUE) {
				echo "Saved: Defense Buildings<br>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			// save walls
			$insert_wall_sql = "INSERT INTO  `thebl962_jumi`.`API_Mem_DefToMem` (";
			$insert_wall_sql .= "`defToMem_member_tag`, `defToMem_defense_name`, ";
			$insert_wall_sql .= "`defToMem_defense_id`, `defToMem_timestamp`, ";
			$insert_wall_sql .= "`defToMem_pos1`, `defToMem_pos2`, `defToMem_pos3`, `defToMem_pos4` ,";
			$insert_wall_sql .= "`defToMem_pos5`, `defToMem_pos6`, `defToMem_pos7`, `defToMem_pos8` , ";
			$insert_wall_sql .= "`defToMem_pos9`, `defToMem_pos10`, `defToMem_pos11`, `defToMem_pos12`) ";
			$insert_wall_sql .= "VALUES ";
			$insert_wall_sql .= "('" . $member_selected . "',  '" . $defense_array[100]["name"] . "'";
			$insert_wall_sql .= ", '" . $defense_array[100]["id"] . "',  '" . $utc_str . "'";
			for ( $j = 1; $j <= 12; $j++ ) {
				if ( !empty ( $defense_array[100]["cnt"][$j] )) {
					$insert_wall_sql .= ", '" . $defense_array[100]["cnt"][$j] . "'";
				} else {
					$insert_wall_sql .= ", NULL";
				}
			}
			$insert_wall_sql .= ")";
			$insert_wall_sql .= ";";
			
			if ($conn->query($insert_wall_sql) === TRUE) {
				echo "Saved: Walls<br>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$insert_trap_sql = "INSERT INTO  `thebl962_jumi`.`API_Mem_TrapToMem` (";
			$insert_trap_sql .= "`trapToMem_member_tag`, `trapToMem_trap_name` , ";
			$insert_trap_sql .= "`trapToMem_trap_id`, `trapToMem_timestamp`, ";
			$insert_trap_sql .= "`trapToMem_pos1`, `trapToMem_pos2`, `trapToMem_pos3`, `trapToMem_pos4` ,";
			$insert_trap_sql .= "`trapToMem_pos5`, `trapToMem_pos6`) ";
			$insert_trap_sql .= "VALUES ";
			for ( $i = 1; $i < 20; $i++ ) {
				if ( !empty ( $traps_array[$i]["name"] )) {
					if ( $i > 1 ) {
						$insert_trap_sql .= ", ";
					}
					$insert_trap_sql .= "('" . $member_selected . "',  '" . $traps_array[$i]["name"] . "', '";
					$insert_trap_sql .= $traps_array[$i]["id"] . "',  '" . $utc_str . "' ";
					for ( $j = 1; $j <= 6; $j++ ) {
						if ( !empty ( $traps_array[$i]["level"][$j] )) {
							$insert_trap_sql .= ", '" . $traps_array[$i]["level"][$j] . "'";
						} else {
							$insert_trap_sql .= ", NULL";
						}
					}
					$insert_trap_sql .= ")";
				}
			}
			$insert_trap_sql .= ";";			

			if ($conn->query($insert_trap_sql) === TRUE) {
				echo "Saved: Traps<br>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$insert_misc_sql = "INSERT INTO  `thebl962_jumi`.`API_Mem_MiscToMem` (";
			$insert_misc_sql .= "`miscToMem_member_tag`, `miscToMem_misc_name` , ";
			$insert_misc_sql .= "`miscToMem_misc_id`, `miscToMem_timestamp`, ";
			$insert_misc_sql .= "`miscToMem_pos1`,`miscToMem_pos2`,`miscToMem_pos3`,`miscToMem_pos4` ,";
			$insert_misc_sql .= "`miscToMem_pos5`,`miscToMem_pos6`,`miscToMem_pos7`) ";
			$insert_misc_sql .= "VALUES ";
			for ( $i = 1; $i < 20; $i++ ) {
				if ( !empty ( $misc_array[$i]["name"] )) {
					if ( $i > 1 ) {
						$insert_misc_sql .= ", ";
					}
					$insert_misc_sql .= "('" . $member_selected . "',  '" . $misc_array[$i]["name"] . "', '";
					$insert_misc_sql .= $misc_array[$i]["id"] . "',  '" . $utc_str . "'";
					for ( $j = 1; $j <= 7; $j++ ) {
						if ( !empty ( $misc_array[$i]["level"][$j] )) {
							$insert_misc_sql .= ", '" . $misc_array[$i]["level"][$j] . "'";
						} else {
							$insert_misc_sql .= ", NULL";
						}
					}
					$insert_misc_sql .= ")";
				}
			}
			$insert_misc_sql .= ";";
			
			if ($conn->query($insert_misc_sql) === TRUE) {
				echo "Saved: Misc. Building<br>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$insert_nonWt_sql = "INSERT INTO  `thebl962_jumi`.`API_Mem_NonWtToMem` (";
			$insert_nonWt_sql .= "`nonWtToMem_member_tag`, `nonWtToMem_nonWt_name`, ";
			$insert_nonWt_sql .= "`nonWtToMem_nonWt_id`, `nonWtToMem_timestamp`, ";
			$insert_nonWt_sql .= "`nonWtToMem_pos1`,`nonWtToMem_pos2`,`nonWtToMem_pos3`,`nonWtToMem_pos4` ,";
			$insert_nonWt_sql .= "`nonWtToMem_pos5`) ";
			$insert_nonWt_sql .= "VALUES ";
			for ( $i = 1; $i < 20; $i++ ) {
				if ( !empty ( $nonWt_array[$i]["name"] )) {
					if ( $i > 1 ) {
						$insert_nonWt_sql .= ', ';
					}
					$insert_nonWt_sql .= "('" . $member_selected . "',  '" . $nonWt_array[$i]["name"] . "', '";
					$insert_nonWt_sql .= $nonWt_array[$i]["id"] . "',  '" . $utc_str . "'"; 
					for ( $j = 1; $j <= 5; $j++ ) {
						if ( !empty ( $nonWt_array[$i]["level"][$j] )) {
							$insert_nonWt_sql .= ", '" . $nonWt_array[$i]["level"][$j] . "'";
						} else {
							$insert_nonWt_sql .= ", NULL";
						}
					}
					$insert_nonWt_sql .= ")";
				}
			}
			$insert_nonWt_sql .= ";";

			if ($conn->query($insert_nonWt_sql) === TRUE) {
				echo "Saved: Non Weight Building<br>";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			$member_ww_insert_sql = "INSERT INTO `thebl962_jumi`.`API_Mem_WarWeight` ";
			$member_ww_insert_sql .= " (`memWW_id`, `memWW_member_tag`, `memWW_defBldg`, `memWW_wall`, ";
			$member_ww_insert_sql .= "  `memWW_trap`, `memWW_miscBldg`, `memWW_hero_off`, `memWW_hero_def`, ";
			$member_ww_insert_sql .= "  `memWW_troop`, `memWW_spell`, `memWW_defense`, `memWW_offense`, ";
			$member_ww_insert_sql .= "  `memWW_total`,`memWW_rounded`,`memWW_Step1`, `memWW_Step2`, ";
			$member_ww_insert_sql .= "  `memWW_Step3_OffRatio`, `memWW_Step3_DefRatio`, `memWW_Step3_DiffRatio`, ";
			$member_ww_insert_sql .= "  `memWW_Step3`, `memWW_Step4_Diff`, `memWW_Step4`, `memWW_timestamp`) ";
			$member_ww_insert_sql .= " VALUES ( NULL, '" . $member_selected . "', '" . $warWeight['bldg_defense'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['wall_defense'] . "', '" . $warWeight['traps'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['misc'] . "', '" . $warWeight['heros']['off'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['heros']['def'] . "', '" . $warWeight['troops'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['spells'] . "', '" . $warWeight['def'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['off'] . "', '" . $warWeight['total'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['rounded'] . "', '" . $warWeight['step1'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['step2'] . "', '" . $warWeight['step3_offRatio'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['step3_defRatio'] . "', '" . $warWeight['step3_diffRatio'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['step3'] . "', '" . $warWeight['step4_diff'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['step4'] . "', '" . $utc_str ."' );";

			if ($conn->query($member_ww_insert_sql) === TRUE) {
				echo "Saved: War Weight<br>";
				$warWeight['flag'] = 0;
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

		} else {
			echo "ERROR CAN NOT SAVE<br>";
		}
	}
		
		
	if ( $warWeight['flag'] == 1 ) {
		UpdateWarWeight ( $member_selected, $member_array["member_townHallLevel"], $warWeight, $conn );
	}

		// echo var_dump ($nonWt_array);	

?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $member_array["member_name"]; ?></title>
</head>
<body>
	<h1 align="center"><?php echo $member_array["member_name"]; ?></h1>
	<center><img src="<?php echo $member_array["league_iconUrls_medium"]; ?>" /><br><c/enter>
 	<form method="post">
		<h2 align="center">War Weight Update</h2>
		<div id="Defense Building">
			<center>
				<h3>Defense Buildings</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b></b>
							</td>
							<?php for ( $i = 1; $i <= $defense_array['maxCnt']; $i++ ) { ?>
								<td align="center">
									<b><?php echo $i ?></b>
								</td>
							<?php } ?>
							<td align="center">
								<b>Max Level<br>Prev. TH</b>
							</td>
							<td align="center">
								<b>Max Level<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>Max Cnt.<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 100 ) { 
							if ( empty ( $defense_array[$i]["name"] )) {
								$i++;
								continue;
							}?>
							<tr>
								<td align="right">
									<?php echo $defense_array[$i]["name"]; ?>
								</td>
								<?php for ( $j = 1; $j <= $defense_array['maxCnt']; $j++ ) { ?>
									<td align="center">
										<?php
											if ( $j > $defense_array[$i]["currMaxCnt"] ) {
												echo "--";
											} else {
												echo "<input type=\"text\" name=\"def_" . $i . "_" . $j . "\" value=\"";
												echo $defense_array[$i]["level"][$j] . "\" size=\"3\">";
											}
										?>
									</td>
								<?php } ?>
								<td align="center">
									<?php echo $defense_array[$i]["prevMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $defense_array[$i]["currMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $defense_array[$i]["currMaxCnt"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $defense_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="<?php echo $defense_array['maxCnt'] + 4 ?>">
								<b>Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['bldg_defense'], 1, ".", "," );?></b>
							</td>
						</tr>
					</tfoot>
				</table>
			</center>
		</div>
		<div id="Wall">
			<center>
				<h3>Walls</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<?php for ( $i = 1; $i <= $defense_array[100]['currMaxLvl']; $i++ ) { ?>
								<td align="center">
									<b><?php echo $i ?></b>
								</td>
							<?php } ?>
							<td align="center">
								<b>Total<br>Count</b>
							</td>
							<td align="center">
								<b>Max Level<br>Prev. TH</b>
							</td>
							<td align="center">
								<b>Max Level<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>Max Cnt.<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<?php for ( $i = 1; $i <= $defense_array[100]['currMaxLvl']; $i++ ) { ?>
								<td align="center">
									<?php
										echo "<input type=\"text\" name=\"def_100_" . $i . "\" value=\"";
										echo $defense_array[100]["cnt"][$i] . "\" size=\"3\">";
									?>
								</td>
							<?php } ?>
							<td align="center">
								<?php echo $defense_array[100]["cnt"]['total']; ?>
							</td>
							<td align="center">
								<?php echo $defense_array[100]["prevMaxLvl"]; ?>
							</td>
							<td align="center">
								<?php echo $defense_array[100]["currMaxLvl"]; ?>
							</td>
							<td align="center">
								<?php echo $defense_array[100]["currMaxCnt"]; ?>
							</td>
							<td align="right">
								<?php echo number_format ( $defense_array[100]["warWeight"], 1, ".", "," ); ?>
							</td>
						</tr>
					</tbody>
				</table>
			</center>
		</div>
		<div id="Trap">
			<center>
				<h3>Traps</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b></b>
							</td>
							<?php for ( $i = 1; $i <= $traps_array['maxCnt']; $i++ ) { ?>
								<td align="center">
									<b><?php echo $i ?></b>
								</td>
							<?php } ?>
							<td align="center">
								<b>Max Level<br>Prev. TH</b>
							</td>
							<td align="center">
								<b>Max Level<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>Max Cnt.<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
					</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 20 ) { 
							if ( empty ( $traps_array[$i]["name"] )) {
								$i++;
								continue;
							}?>
							<tr>
								<td align="right">
									<?php echo $traps_array[$i]["name"]; ?>
								</td>
								<?php for ( $j = 1; $j <= $traps_array['maxCnt']; $j++ ) { ?>
									<td align="center">
										<?php
											if ( $j > $traps_array[$i]["currMaxCnt"] ) {
												echo "--";
											} else {
												echo "<input type=\"text\" name=\"trap_" . $i . "_" . $j . "\" value=\"";
												echo $traps_array[$i]["level"][$j] . "\" size=\"3\">";
											}
										?>
									</td>
								<?php } ?>
								<td align="center">
									<?php echo $traps_array[$i]["prevMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $traps_array[$i]["currMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $traps_array[$i]["currMaxCnt"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $traps_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="<?php echo $traps_array['maxCnt'] + 4 ?>">
								<b>Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['traps'], 1, ".", "," );?></b>
							</td>
						</tr>
					</tfoot>
				</table>
			</center>
		</div>
		<div id="Misc Building">
			<center>
				<h3 align="center">Misc. Building</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b></b>
							</td>
							<?php for ( $i = 1; $i <= $misc_array['maxCnt']; $i++ ) { ?>
								<td align="center">
									<b><?php echo $i ?></b>
								</td>
							<?php } ?>
							<td align="center">
								<b>Max Level<br>Prev. TH</b>
							</td>
							<td align="center">
								<b>Max Level<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>Max Cnt.<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 20 ) { 
							if ( empty ( $misc_array[$i]["name"] )) {
								$i++;
								continue;
							}?>
							<tr>
								<td align="right">
									<?php echo $misc_array[$i]["name"]; ?>
								</td>
								<?php for ( $j = 1; $j <= $misc_array['maxCnt']; $j++ ) { ?>
									<td align="center">
										<?php
											if ( $j > $misc_array[$i]["currMaxCnt"] ) {
												echo "--";
											} else {
												echo "<input type=\"text\" name=\"misc_" . $i . "_" . $j . "\" value=\"";
												echo $misc_array[$i]["level"][$j] . "\" size=\"3\">";
											}
										?>
									</td>
								<?php } ?>
								<td align="center">
									<?php echo $misc_array[$i]["prevMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $misc_array[$i]["currMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $misc_array[$i]["currMaxCnt"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $misc_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="<?php echo $misc_array['maxCnt'] + 4 ?>">
								<b>Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['misc'], 1, ".", "," );?></b>
							</td>
						</tr>
					</tfoot>
				</table>
			</center>
		</div>
		<div id="Non-Weight Building">
			<center>
				<h3 align="center">Non-Weight Building</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b></b>
							</td>
							<?php for ( $i = 1; $i <= $nonWt_array['maxCnt']; $i++ ) { ?>
								<td align="center">
									<b><?php echo $i ?></b>
								</td>
							<?php } ?>
							<td align="center">
								<b>Max Level<br>Prev. TH</b>
							</td>
							<td align="center">
								<b>Max Level<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>Max Cnt.<br>Curr. TH</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php for ( $i = 1; $i < 20; $i++ ) {;
							if ( empty ( $nonWt_array[$i]["name"] )) {
								$i++;
								continue;
							}?>
							<tr>
								<td align="right">
									<?php echo $nonWt_array[$i]["name"]; ?>
								</td>
								<?php for ( $j = 1; $j <= $nonWt_array['maxCnt']; $j++ ) { ?>
									<td align="center">
										<?php
											if ( $j > $nonWt_array[$i]["currMaxCnt"] ) {
												echo "--";
											} else {
												echo "<input type=\"text\" name=\"nonWt_" . $i . "_" . $j . "\" value=\"";
												echo $nonWt_array[$i]["level"][$j] . "\" size=\"3\">";
											}
										?>
									</td>
								<?php } ?>
								<td align="center">
									<?php echo $nonWt_array[$i]["prevMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $nonWt_array[$i]["currMaxLvl"]; ?>
								</td>
								<td align="center">
									<?php echo $nonWt_array[$i]["currMaxCnt"]; ?>
								</td>
								<td align="right">
									0.0
								</td>
							</tr>
						<?php } ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="9">
								<b>Total</b>
							</td>
							<td align="right">
								0.0
							</td>
						</tr>
					</tfoot>
			</table>
			</center>
		</div>
		<br>
		<input type="submit" name="action" value="Update">
		<input type="submit" name="action" value="Save">
		<br>
		<div id="tabs">
			<ul>
				<li><a href="#tabs-1">Heros, Troops, & Spells</a></li>
				<li><a href="#tabs-2">War Weight</a></li>
				<li><a href="#tabs-3">Engineered</a></li>
				<li><a href="#tabs-4">Base Ratio</a></li>
				<li><a href="#tabs-5">Information</a></li>
			</ul>
			<div id="tabs-1">
				<div id="War Weight">
					<h3 align="center">Heros</h3>
					<center>
						<table width="100%" class="sortable">
							<thead>
								<tr>
									<td align="center">
										<b></b>
									</td>
									<td align="center">
										Level
									</td>
									<td align="center">
										<b>Max Level<br>Prev. TH</b>
									</td>
									<td align="center">
										<b>Max Level<br>Curr. TH</b>
									</td>
									<td align="center">
										<b>Def.<br>War Wgt.</b>
									</td>
									<td align="center">
										<b>Off.<br>War Wgt.</b>
									</td>
								</tr>
							</thead>
							<tbody>
								<?php for ( $i = 1; $i <= count ( $heros_array ); $i++ ) { ?>
									<tr>
										<td align="right">
											<?php echo $heros_array[$i]['name']; ?>
										</td>
										<td align="center">
											<?php echo $heros_array[$i]['level']; ?>
										</td>
										<td align="center">
											<?php echo $heros_array[$i]['prevMaxLvl']; ?>
										</td>
										<td align="center">
											<?php echo $heros_array[$i]['currMaxLvl']; ?>
										</td>
										<td align="right">
											<?php echo number_format ( $heros_array[$i]['warWeight_def'], 1 ); ?>
										</td>
										<td align="right">
											<?php echo number_format ( $heros_array[$i]['warWeight_off'], 1 ); ?>
										</td>
									</tr> 
								<?php } ?>
							</tbody>
							<tfoot> 
								<tr>
									<td align="right" colspan="4">
										<b>Total</b>
									</td>
									<td align="right">
										<b><?php echo number_format ( $warWeight['heros']['def'], 1 ); ?></b>
									</td>
									<td align="right">
										<b><?php echo number_format ( $warWeight['heros']['off'], 1 ); ?></b>
									</td>
								</tr>
							</tfoot> 
						</table> 
					</center>
					<h3 align="center">Elixir Troops</h3>
					<center>
						<table width="100%" class="sortable">
							<thead>
								<tr>
									<td align="center">
										<b></b>
									</td>
									<td align="center">
										Level
									</td>
									<td align="center">
										<b>Max Level<br>Prev. TH</b>
									</td>
									<td align="center">
										<b>Max Level<br>Curr. TH</b>
									</td>
									<td align="center">
										<b>War<br>Weight</b>
									</td>
								</tr>
							</thead>
							<tbody>
								<?php for ( $i = 1; $i < 100; $i++ ) { ?>
									<?php if ( is_null ( $troops_array[$i]['name'] )) { break; } ?>
									<tr>
										<td align="right">
											<?php echo $troops_array[$i]['name']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['level']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['prevMaxLvl']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['currMaxLvl']; ?>
										</td>
										<td align="right">
											<?php echo number_format ( $troops_array[$i]['warWeight'], 1 ); ?>
										</td>
									</tr> 
								<?php } ?>
							</tbody>
							<tfoot> 
								<tr>
									<td align="right" colspan="4">
										<b>Total</b>
									</td>
									<td align="right">
										<b><?php echo number_format ( $warWeight['elixir_troops'], 1 ); ?></b>
									</td>
								</tr>
							</tfoot> 
						</table> 
					</center>
					<h3 align="center">Dark Elixir Troops</h3>
					<center>
						<table width="100%" class="sortable">
							<thead>
								<tr>
									<td align="center">
										<b></b>
									</td>
									<td align="center">
										Level
									</td>
									<td align="center">
										<b>Max Level<br>Prev. TH</b>
									</td>
									<td align="center">
										<b>Max Level<br>Curr. TH</b>
									</td>
									<td align="center">
										<b>War<br>Weight</b>
									</td>
								</tr>
							</thead>
							<tbody>
								<?php for ( $i = 101; $i < 200; $i++ ) { ?>
									<?php if ( is_null ( $troops_array[$i]['name'] )) { break; } ?>
									<tr>
										<td align="right">
											<?php echo $troops_array[$i]['name']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['level']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['prevMaxLvl']; ?>
										</td>
										<td align="center">
											<?php echo $troops_array[$i]['currMaxLvl']; ?>
										</td>
										<td align="right">
											<?php echo number_format ( $troops_array[$i]['warWeight'], 1 ); ?>
										</td>
									</tr> 
								<?php } ?>
							</tbody>
							<tfoot> 
								<tr>
									<td align="right" colspan="4">
										<b>Total</b>
									</td>
									<td align="right">
										<b><?php echo number_format ( $warWeight['de_troops'], 1 ); ?></b>
									</td>
								</tr>
							</tfoot> 
						</table> 
					</center>
					<h3 align="center">Spells</h3>
					<center>
						<table width="100%" class="sortable">
							<thead>
								<tr>
									<td align="center">
										<b></b>
									</td>
									<td align="center">
										Level
									</td>
									<td align="center">
										<b>Max Level<br>Prev. TH</b>
									</td>
									<td align="center">
										<b>Max Level<br>Curr. TH</b>
									</td>
									<td align="center">
										<b>War<br>Weight</b>
									</td>
								</tr>
							</thead>
							<tbody>
								<?php for ( $i = 1; $i < 120; $i++ ) { ?>
									<?php if ( is_null ( $spells_array[$i]['name'] )) { continue; } ?>
									<tr>
										<td align="right">
											<?php echo $spells_array[$i]['name']; ?>
										</td>
										<td align="center">
											<?php echo $spells_array[$i]['level']; ?>
										</td>
										<td align="center">
											<?php echo $spells_array[$i]['prevMaxLvl']; ?>
										</td>
										<td align="center">
											<?php echo $spells_array[$i]['currMaxLvl']; ?>
										</td>
										<td align="right">
											<?php echo number_format ( $spells_array[$i]['warWeight'], 1 ); ?>
										</td>
									</tr> 
								<?php } ?>
							</tbody>
							<tfoot> 
								<tr>
									<td align="right" colspan="4">
										<b>Total</b>
									</td>
									<td align="right">
										<b><?php echo number_format ( $warWeight['spells'], 1 ); ?></b>
									</td>
								</tr>
							</tfoot> 
						</table> 
					</center>
				</div>
			</div>
			<div id="tabs-2">
				<div id="War Weight">
					<h3 align="center">Defensive War Weight</h3>
					<center>
						<table width="50%">
							<tr>
								<td align="right" width="60%">
									Def. Bldg.:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['bldg_defense'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right" width="60%">
									Walls:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['wall_defense'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Trap:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['traps'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Misc. Bldg.:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['misc'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Hero:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['heros']['def'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Subtotal:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['def'], 1, ".", "," ); ?>
								</th>
							</tr>
						</table>
					<h3 align="center">Offensive War Weight</h3>
						<table width="50%">
							<tr>
								<td align="right">
									Elixir Troop:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['elixir_troops'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Dark Elixir Troop:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['de_troops'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Spell:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['spells'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Hero:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['heros']['off'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									<b>Subtotal:</b>
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['off'], 1, ".", "," ); ?>
								</th>
							</tr>
						</table>
					<h3 align="center">Total War Weight</h3>
						<table width="50%">
							<tr>
								<td align="right">
									Defensive:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['def'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Offensive:
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['off'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Total:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['total'], 1, ".", "," ); ?>
								</th>
							</tr>
							<tr>
								<th align="right" width="60%">
									Rounded:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['rounded'], 1, ".", "," ); ?>
								</th>
							</tr>
						</table>
						<br><h4>Notes</h4>
						<font size="2">
							Total War Weight is used by COC to list the base order in wars.<br>
							Rounded War Weight is used by COC in war match making process.<br>
						</font> 
					</center>
				</div>
			</div>
			<div id="tabs-3">
				<div id="Engineered">
					<h3 align="center">Engineered</h3>
					<center>
						<table class="sortable">
							<tr>
								<td align="center">
									<b>Step 1</b>
								</td>
								<td align="right" colspan="3">
									Is there anything missing on the base?
								</td>
								<td align="center" width="15%">
									<?php if ( $warWeight['step1'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="center" width="10%">
									<b>Step 2</b>
								</td>
								<td align="right" colspan="3">
									Did the base NOT max out at the previous Townhall level?
								</td>
								<td align="center" width="5%">
									<?php if ( $warWeight['step2'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="center" width="10%" rowspan="3">
									<b>Step 3</b>
								</td>
								<td align="right">
									Offensive ratio
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_offRatio'], 2 ) ?>%
								</td>
								<td align="right" rowspan="3">
									Level ratio > 30%?
								</td>
								<td align="center" rowspan="3">
									<?php if ( $warWeight['step3'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Defensive ratio
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_defRatio'], 2 ) ?>%
								</td>
							</tr>
							<tr>
								<td align="right">
									Difference	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_diffRatio'], 2 ) ?>%
								</td>
							</tr>
							<tr>
								<td align="center" width="10%" rowspan="3">
									<b>Step 4</b>
								</td>
								<td align="right">
									Offensive weight	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['off'], 1, ".", "," ); ?>
								</td>
								<td align="right" rowspan="3">
									Classification:	
								</td>
								<td align="center" rowspan="3">
									<?php echo $warWeight['step4_text'] ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Defensive weight
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['def'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Difference	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step4_diff'], 1, ".", "," ); ?>
								</td>
							</tr>
						</table>
						<br>
						<h4>Step 1</h4>
						<font size="2">
							Does the base have all items, troops, & spells at the CURRENT Townhall level, or are any missing?
						</font>
						<br>
						<h4>Step 2</h4>
						<font size="2">
							Did the base max out everything at the PREVIOUS Townhall level, or was anything missing?
						</font>
						<br>
						<h4>Step 3</h4>
						<font size="2">
							Is the difference between offensive & defensive total level ratios more than 30%?
						</font>
						<br>
						<h4>Step 4</h4>
						<font size="2">
							If (Step 1 AND Step 3 equal YES) <b>OR</b> (Step 2 AND Step 3 equal YES)<br> 
							The base is <em>ENGINEERED</em>. <br> 
							Then use the Classification table on Information tab.<br>
						</font>
					</center>
				</div>
			</div>
			<div id="tabs-4">
				<div id="War Weight">
					<h3 align="center">Base Ratio</h3>
					<center>
						<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
						<br>Under Construction
					</center>
				</div>
			</div>
			<div id="tabs-5">
				<div id="War Weight">
					<h3 align="center">Information Only</h3>
					<center>
						<table width="50%" class="sortable">
							<tr>
								<td align="center">
									<b>Town Hall</b>
								</td>
								<td align="center">
									<b>Median<br>War Weight</b>
								</td>
								<td align="center">
									<b>Max<br>War Weight</b>
								</td>
							</tr>
							<?php for ( $i = 1; $i <= 11; $i++ ) { ?>
								<tr>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
									<td align="center">
										<b><?php echo $warWeight_TH[$i]['WarWeight_Median'] ?></b>
									</td>
									<td align="center">
										<b><?php echo $warWeight_TH[$i]['WarWeight_Max'] ?></b>
									</td>
								</tr>
							<?php } ?>
						</table>
						<br>
						<table width="30%" class="sortable">
							<tr>
								<td align="center">
									Classification
								</td>
								<td align="center">
									Bracket
								</td>
							</tr>
							<tr>
								<td align="center">
									Low
								</td>
								<td align="center">
									0-4999	
								</td>
							</tr>
							<tr>
								<td align="center">
									Medium
								</td>
								<td align="center">
									5000-14999	
								</td>
							</tr>
							<tr>
								<td align="center">
									High
								</td>
								<td align="center">
									15000-max	
								</td>
							</tr>
						</table>
					</center>
				</div>
			</div>
		</div>
	</form>
	<center>
		<img src="<?php echo $member_array["clan_badgeUrls_medium"]; ?>" /><br>
	</center>
</br>  

<!-- JQuery Script -->
<link rel="stylesheet" href="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.css">
<script src="http://www.the-blacklist.ca/images/jumi_code/jquery.js"></script>
<script src="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#tabs" ).tabs({
			active: 2
		});
	} );

</script>

</body>

<?php

function UpdateEngineered ( $member_selected, $member_townHallLevel, &$warWeight, $items_array, $conn ) {
	$level = array();
	$maxItemsCurr = array();
	$maxItemsPrev = array();
	$foundItem = array();
	
	// set step's to no till proven yes
	$warWeight['step1'] = 0;
	$warWeight['step2'] = 0;
	$warWeight['step3'] = 0;
	$warWeight['step4'] = 0;
	
	// check if bldgs are entered
	if ( $warWeight['bldg_defense'] == 0 ) {
		// set rounded weight to 0
		$warWeight['rounded'] = 0;
		return;
	} else {
		// compute rounded weight
		$warWeight['rounded'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;
	}
	// get previous th levels	
	$temp_prevTH = $member_townHallLevel - 1;


	// iterate threw max previous items
	foreach ( $items_array as $item_array ) {
		// echo var_dump ( $maxItemCurr ) . '<br>';
		// echo var_dump ( $item_array ) . '<br>';
		// echo var_dump ( $maxItemPrev ) . '<br><br>';
		$temp_step1_flag = 0;
		$temp_step2_flag = 0;
		$$level['off'] = 0;
		$$level['def'] = 0;
		// do walls special
		if ( $item_array['name'] === 'Wall' ) {
			// check if count is off
			if ( $item_array['cnt']['total'] < $item_array['currMaxCnt'] ) {
				$warWeight['step1'] = 1;
				$temp_step1_flag = 1;
			}
			// itereate threw wall counts
			for ( $i = 1; $i <= $item_array['currMaxLvl']; $i++ ) {
				// check if level is below previous th
				if (( $i <= $item_array['prevMaxLvl'] )&&( $item_array['cnt'][$i] > 0 )) {
					$warWeight['step2'] = 1;
					$temp_step2_flag = 1;
				}
				// add up def levels
				$level['def'] += $item_array['cnt'][$i] * $i;							
			}
		// process Heros
		} elseif ( $item_array['type'] === 'Hero' ) {
			// check if level is not set
			if (( is_null ( $item_array["level"] ))||( $item_array["level"] == 0 )) {
				$warWeight['step1'] = 1;
				$temp_step1_flag = 1;
			}
			// check if level is below previous th
			if ( $item_array["prevMaxLvl"] > $item_array["level"] ) {
				$warWeight['step2'] = 1;
				$temp_step2_flag = 1;
			}
			// add up def levels
			$level['off'] += $item_array["level"];							
		// process Troops and Spells 
		} elseif (( $item_array['type'] === 'Elixir Troops' )||( $item_array['type'] === 'DE Troops' )||
				( $item_array['type'] === 'Spell' )) {
			// check if level is not set
			if (( is_null ( $item_array["level"] ))||( $item_array["level"] == 0 )) {
				$warWeight['step1'] = 1;
				$temp_step1_flag = 1;
			}
			// check if level is below previous th
			if ( $item_array["prevMaxLvl"] > $item_array["level"] ) {
				$warWeight['step2'] = 1;
				$temp_step2_flag = 1;
			}
			// add up def levels
			$level['off'] += $item_array["level"];							
		// process non weighted buildings
		} elseif ( $item_array['type'] === 'NonWt' ) {
			if (( $item_array['name'] === 'Army Camp' )||( $item_array['name'] === 'Clan Castle' )) {
				foreach ( $item_array['level'] as $temp_level) {
					// check if level is not set
					if (( is_null ($temp_level))||( $temp_level == 0 )) {
						$temp_step1_flag = 1;
					}
					// add up def levels
					$level['def'] += $temp_level;							
				}
			}
		// process Defense, Trap, Resource
		} elseif (( $item_array['type'] === 'Defense' )||( $item_array['type'] === 'Trap' )||
				( $item_array['type'] === 'Resource' )) {
			// check if count is off
			if ( count ( $item_array['level'] ) != $item_array['currMaxCnt'] ) {
				$warWeight['step1'] = 1;
				$temp_step1_flag = 1;
			}
			// loop threw item's
			foreach ( $item_array['level'] as $temp_level) {
				// check if level is not set
				if (( is_null ($temp_level))||( $temp_level == 0 )) {
					$warWeight['step1'] = 1;
					$temp_step1_flag = 1;
				}
				// check if level is below previous th
				if ( $item_array["prevMaxLvl"] > $temp_level ) {
					$warWeight['step2'] = 1;
					$temp_step2_flag = 1;
				}
				// add up def levels
				$level['def'] += $temp_level;							
			}
		} 
		// warn user
		if ( $temp_step1_flag == 1 ) {
			echo "Warning: Missing for town hall level: " . $item_array['name'] . " <br>";				
		}
		if ( $temp_step2_flag == 1 ) {
			echo "Warning: Less then previous town hall max: " . $item_array['name'] . " <br>";
		}
	}	


	echo 'off = ' . $level['off'] . '<br>';
	echo 'def = ' . $level['def'] . '<br>';
	// computer Ratios
	$warWeight['step3_defRatio'] = $level['def'] / $warWeight[11]['DefLvlperTh'] * 100;
	$warWeight['step3_offRatio'] = $level['off'] / $warWeight[11]['OffLvlperTH'] * 100;
	$warWeight['step3_diffRatio'] = abs ( $warWeight['step3_defRatio'] - $warWeight['step3_offRatio'] );

	// check if diff Ratio is over 30%
	if ( $warWeight['step3_diffRatio'] > 30 ) {
		$warWeight['step3'] = 1;
	}
	
	// computer war weight difference
	$warWeight['step4_diff'] = abs ( $warWeight['off'] - $warWeight['def'] );

	// check if engineered
	if ((( $warWeight['step1'] == 1 )||( $warWeight['step2'] == 1 ))&&( $warWeight['step3'] == 1 ) ) {
		echo "Base is Engineered.<br>";
		if ( $warWeight['step4_diff'] < 5000 ) {
			$warWeight['step4'] = 1;
			$warWeight['step4_text'] = '<font color="yellow">Low</font>';
		} elseif ( $warWeight['step4_diff'] < 15000 ) {
			$warWeight['step4'] = 2;
			$warWeight['step4_text'] = '<font color="orange">Medium</font>';
		} else {
			$warWeight['step4'] = 3;			
			$warWeight['step4_text'] = '<font color="red">High</font>';
		} 
	} else {
		$warWeight['step4'] = 0;			
		$warWeight['step4_text'] = '<font color="green">No</font>';
	}
}

function UpdateWarWeight ( $member_selected, $member_townHallLevel, &$warWeight, $conn ) {

	$warWeight_TH_sql = "SELECT * ";
	$warWeight_TH_sql .= " FROM `API_WarWeight_TH` ";
	$warWeight_TH_sql .= " WHERE `TH` LIKE " . $member_townHallLevel . ";";
	$warWeight_TH_result = $conn->query($warWeight_TH_sql);
		
	while($warWeight_TH_row = $warWeight_TH_result->fetch_assoc()) {
		$warWeight['min'] = $warWeight_TH_row['WarWeight_Min'];
		$warWeight['median'] = $warWeight_TH_row['WarWeight_Median'];
		$warWeight['max'] = $warWeight_TH_row['WarWeight_Max'];
	}

	if ( $warWeight['bldg_defense'] == 0 ) {
		$warWeight['unadjusted'] = 0;
		$warWeight['penalty'] = 0;
		$warWeight['adjusted'] = $warWeight['median'];			
	} else {
		$warWeight['unadjusted'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;
		
		if (( $warWeight['unadjusted'] + 10000 < $warWeight['median'] )&&
					( $warWeight['total'] - 6 < $warWeight['min'] ))  {
			$warWeight['penalty'] = $warWeight['median'] - $warWeight['unadjusted'];
			if ( $warWeight['penalty'] > 29999 ){
				$warWeight['penalty'] = 29999;
			}
			$warWeight['adjusted'] = $warWeight['unadjusted'] + $warWeight['penalty'];
		} else {
			$warWeight['penalty'] = 0;
			$warWeight['adjusted'] = $warWeight['unadjusted'];
		}
	}
}

function UpdateNonWtArray ( &$nonWt_array, $member_array, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 10; $i++ ) {
		if ( !empty ( $nonWt_array[$i]["name"] )) {
			for ( $j = 1; $j <= $nonWt_array[$i]["currMaxCnt"]; $j++ ) {
				$temp_level_new = $_POST["nonWt_" . $i . "_" . $j];
				$temp_level_old = $nonWt_array[$i]["level"][$j];
				if ( $temp_level_new !== $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $nonWt_array[$i]["currMaxLvl"] ) {
						$nonWt_array[$i]["level"][$j] = $temp_level_new;
					} else {
						echo "ERROR: " . $nonWt_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $nonWt_array[$i]["currMaxLvl"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	return $error;
}		

function UpdateMiscArray ( &$misc_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 7; $i++ ) {
		if ( !empty ( $misc_array[$i]["name"] )) {
			for ( $j = 1; $j <= $misc_array[$i]["currMaxCnt"]; $j++ ) {
				$temp_level_new = $_POST["misc_" . $i . "_" . $j];
				$temp_level_old = $misc_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $misc_array[$i]["currMaxLvl"] ) {
						$misc_array[$i]["level"][$j] = $temp_level_new;
						$temp_ww_new = $misc_array[$i]["ww"][$temp_level_new];
						$temp_ww_old = $misc_array[$i]["ww"][$temp_level_old];
						$temp_ww_change = $temp_ww_new - $temp_ww_old;
						$misc_array[$i]["warWeight"] += $temp_ww_change;
						$warWeight['misc'] += $temp_ww_change;
						$warWeight['def'] += $temp_ww_change;									
						$warWeight['total'] += $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $misc_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $misc_array[$i]["currMaxLvl"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	return $error;
}		
				
function UpdateTrapArray ( &$traps_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 20; $i++ ) {
		if ( !empty ( $traps_array[$i]["name"] )) {
			for ( $j = 1; $j <= $traps_array[$i]["currMaxCnt"]; $j++ ) {
				$temp_level_new = $_POST["trap_" . $i . "_" . $j];
				$temp_level_old = $traps_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $traps_array[$i]["currMaxLvl"] ) {
						$traps_array[$i]["level"][$j] = $temp_level_new;
						$temp_ww_new = $traps_array[$i]["ww"][$temp_level_new];
						$temp_ww_old = $traps_array[$i]["ww"][$temp_level_old];
						$temp_ww_change = $temp_ww_new - $temp_ww_old;
						$traps_array[$i]["warWeight"] += $temp_ww_change;
						$warWeight['traps'] += $temp_ww_change;
						$warWeight['def'] += $temp_ww_change;									
						$warWeight['total'] += $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $traps_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $traps_array[$i]["currMaxLvl"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	return $error;
}		

function UpdateDefArray ( &$defense_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 20; $i++ ) {
		if ( !empty ( $defense_array[$i]["name"] )) {
			for ( $j = 1; $j <= $defense_array[$i]["currMaxCnt"]; $j++ ) {
				$temp_level_new = $_POST["def_" . $i . "_" . $j];
				$temp_level_old = $defense_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $defense_array[$i]["currMaxLvl"] ) {
						$defense_array[$i]["level"][$j] = $temp_level_new;
						$temp_ww_new = $defense_array[$i]["ww"][$temp_level_new];
						$temp_ww_old = $defense_array[$i]["ww"][$temp_level_old];
						$temp_ww_change = $temp_ww_new - $temp_ww_old;
						$defense_array[$i]["warWeight"] += $temp_ww_change;
						$warWeight['bldg_defense'] += $temp_ww_change;
						$warWeight['def'] += $temp_ww_change;									
						$warWeight['total'] += $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $defense_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $defense_array[$i]["currMaxLvl"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	// 100 == walls
	if ( !empty ( $defense_array[100]["name"] )) {
		for ( $j = 1; $j <= $defense_array[100]["currMaxLvl"]; $j++ ) {
			$temp_cnt_new = $_POST["def_" . 100 . "_" . $j];
			$temp_cnt_old = $defense_array[100]["cnt"][$j];
			if ( $temp_cnt_new != $temp_cnt_old ) {
				if ( $temp_cnt_new >= 0 && $temp_cnt_new != $defense_array[100]["cnt"][$j] ) {
					$defense_array[100]["cnt"][$j] = $temp_cnt_new;
					$temp_cnt_change = $temp_cnt_new - $temp_cnt_old;
					$defense_array[100]["cnt"]["total"] += $temp_cnt_change;
					$temp_ww_new = $defense_array[100]["ww"][$j] * $temp_cnt_new;
					$temp_ww_old = $defense_array[100]["ww"][$j] * $temp_cnt_old;
					$temp_ww_change = $temp_ww_new - $temp_ww_old;
					$defense_array[100]["warWeight"] +=  $temp_ww_change;
					$warWeight['wall_defense'] += $temp_ww_change;
					$warWeight['def'] += $temp_ww_change;									
					$warWeight['total'] += $temp_ww_change;									
					$warWeight['flag'] = 1;
				} else {
					echo "ERROR: " . $defense_array[$i]["name"] . " level " . $j;
					echo " is not a legal entry.<br>";
					$error = $error + 1;
					continue;
				}
			} 
		}
		if ( $defense_array[100]['currMaxCnt'] < $defense_array[100]["cnt"]["total"] ) {
			echo "ERROR: Total count for walls (" . $defense_array[100]["cnt"]["total"] . ")";
			echo " exceed Town Hall max count of " . $defense_array[100]['currMaxCnt'] . ". <br>";
			$error = $error + 1;
		}
				// && $temp_cnt_total < $defense_array[100]["maxTHLevel"] 
	}
	return $error;
}		
		
function maxTHLevel ( $inputName, $inputTH, $conn ) {
	$maxTHLevel = 0;
	
	// escape single quotes
	$inputName = addslashes($inputName);	
	
	// Freeze spell is special
	if ( $inputName == 'Freeze' && $inputTH = 9 ) {
		return 1;
	}
	
	$maxTHLevel_sql = "SELECT tww.`ww_level`, lww.`ww_thLevel`  ";
	$maxTHLevel_sql .= " FROM `API_WarWeight` AS tww ";
	$maxTHLevel_sql .= " INNER JOIN `API_WarWeight` AS lww ";
	$maxTHLevel_sql .= "   ON tww.`ww_labLevel` = lww.`ww_level` ";
	$maxTHLevel_sql .= " WHERE tww.`ww_name` LIKE '" . $inputName . "' ";
	$maxTHLevel_sql .= "   AND lww.`ww_name` LIKE 'Laboratory' ";
	$maxTHLevel_sql .= " ORDER BY tww.`ww_level` ASC;";
	$maxTHLevel_result = $conn->query($maxTHLevel_sql);
	
	while($maxTHLevel_row = $maxTHLevel_result->fetch_assoc()) {
		$temp_level = $maxTHLevel_row["ww_level"];
		$temp_thLevel = $maxTHLevel_row["ww_thLevel"];
		
		if ( $temp_thLevel <= $inputTH ) {
			$maxTHLevel = $temp_level;
		}
	}
	
	return $maxTHLevel;
}

