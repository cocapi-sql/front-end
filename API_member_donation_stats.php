<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>Donation Stats</title>
</head>
<body>
	<div id="Top 10 Highest Donations Sent">
		<a href="http://www.the-blacklist.ca/index.php/war-log/graphs/member-donation-stats?display=topSent">
			<h1 align="center">
				Top 10 Highest Donations Sent
			</h1>
		</a>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Current<br>Sent</b></th>
				<th align="center"><b>Total<br>Sent</b></th>
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donations_current" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$i]["trophies"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donations"]; ?></center>
						</td>
						<td>
							<center><font color="green"><?php echo $member_array[$i]["total_donations"]; ?></font></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
	<br>
	<div id="Top 10 Highest Donation Sent Ratio">
		<a href="http://www.the-blacklist.ca/index.php/war-log/graphs/member-donation-stats?display=topSentRat">
			<h1 align="center">
				Top 10 Highest Donation Sent Ratio
			</h1>
		</a>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Sent<br>Ratio*</b></th>
				<th align="center"><b>Total<br>Sent</b></th>				
				<th align="center"><b>Total<br>Received</b></th>				
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donations_ratio_current" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$i]["trophies"]; ?></center>
						</td>
						<td>
							<center><font color="green"><?php echo $member_array[$i]["total_donations_ratio"]; ?>%</font></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["total_donations"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["total_donationsReceived"]; ?></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
			* Sent Ratio = Total Donations Sent / Total Donations Received
		</left>
	</div>
	<br>
	<div id="Top 10 Highest Donation Received">
		<a href="http://www.the-blacklist.ca/index.php/war-log/graphs/member-donation-stats?display=topRec">
			<h1 align="center">
				Top 10 Highest Donation Received
			</h1>
		</a>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b<br>>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b><br>Current<br>Received</b></th>
				<th align="center"><b><br>Total<br>Received</b></th>
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donationsReceived_current" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$i]["trophies"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donationsReceived"]; ?></center>
						</td>
						<td>
							<center><font color="red"><?php echo $member_array[$i]["total_donationsReceived"]; ?></font></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
		</left>
	</div>
	<br>
	<div id="Top 10 Highest Donation Received Ratio">
		<a href="http://www.the-blacklist.ca/index.php/war-log/graphs/member-donation-stats?display=topRecRat">
			<h1 align="center">
				Top 10 Highest Donation Received Ratio
			</h1>
		</a>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Received<br>Ratio*</b></th>
				<th align="center"><b>Total<br>Sent</b></th>				
				<th align="center"><b>Total<br>Received</b></th>				
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donationsReceived_ratio_current" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$i]["trophies"]; ?></center>
						</td>
						<td>
							<center><font color="red"><?php echo $member_array[$i]["total_donationsReceived_ratio"]; ?>%</font></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["total_donations"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["total_donationsReceived"]; ?></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
			* Received Ratio = Total Donations Received / Total Donations Sent
		</left>
	</div>
					
<center>
	<b>This data was updated at <?php echo $timestamp_current; ?> GMT.</b>
</center>
</br>  
<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
</body>