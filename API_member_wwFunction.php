<?php

function UpdateWarWeight ( $member_selected, $member_townHallLevel, &$warWeight, $conn ) {
	$warWeight['unadjusted'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;

	if ( $member_townHallLevel == 1 ) {
		$warWeight['median'] = 0;
		$warWeight['priorTH'] = 0;
	} elseif ( $member_townHallLevel == 2 ) {
		$warWeight['median'] = 1000;
		$warWeight['priorTH'] = 0;
	} elseif ( $member_townHallLevel == 3 ) {
		$warWeight['median'] = 4000;
		$warWeight['priorTH'] = 2000;
	} elseif ( $member_townHallLevel == 4 ) {
		$warWeight['median'] = 8000;
		$warWeight['priorTH'] = 6000;
	} elseif ( $member_townHallLevel == 5 ) {
		$warWeight['median'] = 14000;
		$warWeight['priorTH'] = 10000;
	} elseif ( $member_townHallLevel == 6 ) {
		$warWeight['median'] = 22000;
		$warWeight['priorTH'] = 18000;
	} elseif ( $member_townHallLevel == 7 ) {
		$warWeight['median'] = 33000;
		$warWeight['priorTH'] = 26000;
	} elseif ( $member_townHallLevel == 8 ) {
		$warWeight['median'] = 51000;
		$warWeight['priorTH'] = 40000;
	} elseif ( $member_townHallLevel == 9 ) {
		$warWeight['median'] = 74000;
		$warWeight['priorTH'] = 62000;
	} elseif ( $member_townHallLevel == 10 ) {
		$warWeight['median'] = 103000;
		$warWeight['priorTH'] = 86000;
	} elseif ( $member_townHallLevel == 11 ) {
		$warWeight['median'] = 135000;
		$warWeight['priorTH'] = 120000;
	} else {
		echo "ERROR: Unknown Town Hall level: " . $member_townHallLevel . "<br>";
	}

	if (( $warWeight['unadjusted'] + 10000 < $warWeight['median'] )&&( $warWeight['total'] - 6 < $warWeight['priorTH'] ))  {
		$warWeight['adjusted'] = $warWeight['median'];
	} else {
		$warWeight['adjusted'] = $warWeight['unadjusted'];
	}

	$warWeight['penalty'] = $warWeight['adjusted'] - $warWeight['unadjusted'];

	$member_ww_sql = "SELECT  `memWW_unadjusted` ,  `memWW_adjusted` ,  `memWW_penalty` ";
	$member_ww_sql .= " FROM  `API_Mem_WarWeight` ";
	$member_ww_sql .= "WHERE  `memWW_member_tag` LIKE  '" . $member_selected . "' ";
	$member_ww_sql .= "ORDER BY  `memWW_timestamp` DESC ";
	$member_ww_sql .= "LIMIT 1;";
	$member_ww_result = $conn->query($member_ww_sql);

	while($member_ww_row = $member_ww_result->fetch_assoc()) {
		$warWeight['db_unadjusted'] = $member_ww_row['memWW_unadjusted'];
		$warWeight['db_adjusted'] = $member_ww_row['memWW_adjusted'];
		$warWeight['db_penalty'] = $member_ww_row['memWW_penalty'];
	}	
}

function UpdateMiscArray ( &$misc_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 7; $i++ ) {
		if ( !empty ( $misc_array[$i]["name"] )) {
			$ww_flag = 0;
			for ( $j = 1; $j <= $misc_array[$i]["thCnt"]; $j++ ) {
				$temp_level_new = $_POST["misc_" . $i . "_" . $j];
				$temp_level_old = $misc_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $misc_array[$i]["maxTHLevel"] ) {
						$misc_array[$i]["level"][$j] = $temp_level_new;
						if ( $ww_flag == 0 ) {
							$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
							$member_warWeight_sql .= " FROM `API_WarWeight` ";
							$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $misc_array[$i]["name"] . "' ";
							$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
							$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
							$member_warWeight_result = $conn->query($member_warWeight_sql);
	
							$temp_ww = array();
							$k = 1;
							while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
								$temp_ww[$k] = $member_warWeight_row["ww_weight"];
								$k++;
							}
							$ww_flag = 1;
						}
						$temp_ww_change = $temp_ww[$temp_level_new] - $temp_ww[$temp_level_old];
						$misc_array[$i]["warWeight"] = $misc_array[$i]["warWeight"] + $temp_ww_change;
						$warWeight['misc'] = $warWeight['misc'] + $temp_ww_change;
						$warWeight['total'] = $warWeight['total'] + $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $misc_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $misc_array[$i]["maxTHLevel"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	return $error;
}		
				
function UpdateTrapArray ( &$traps_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 20; $i++ ) {
		if ( !empty ( $traps_array[$i]["name"] )) {
			$ww_flag = 0;
			for ( $j = 1; $j <= $traps_array[$i]["thCnt"]; $j++ ) {
				$temp_level_new = $_POST["trap_" . $i . "_" . $j];
				$temp_level_old = $traps_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $traps_array[$i]["maxTHLevel"] ) {
						$traps_array[$i]["level"][$j] = $temp_level_new;
						if ( $ww_flag == 0 ) {
							$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
							$member_warWeight_sql .= " FROM `API_WarWeight` ";
							$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $traps_array[$i]["name"] . "' ";
							$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
							$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
							$member_warWeight_result = $conn->query($member_warWeight_sql);
	
							$temp_ww = array();
							$k = 1;
							while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
								$temp_ww[$k] = $member_warWeight_row["ww_weight"];
								$k++;
							}
							$ww_flag = 1;
						}
						$temp_ww_change = $temp_ww[$temp_level_new] - $temp_ww[$temp_level_old];
						$traps_array[$i]["warWeight"] = $traps_array[$i]["warWeight"] + $temp_ww_change;
						$warWeight['traps'] = $warWeight['traps'] + $temp_ww_change;
						$warWeight['total'] = $warWeight['total'] + $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $traps_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $traps_array[$i]["maxTHLevel"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	return $error;
}		

function UpdateDefArray ( &$defense_array, $member_array, &$warWeight, $conn ) {
	$error = 0;
	for ( $i = 1; $i < 20; $i++ ) {
		if ( !empty ( $defense_array[$i]["name"] )) {
			$ww_flag = 0;
			for ( $j = 1; $j <= $defense_array[$i]["thCnt"]; $j++ ) {
				$temp_level_new = $_POST["def_" . $i . "_" . $j];
				$temp_level_old = $defense_array[$i]["level"][$j];
				if ( $temp_level_new != $temp_level_old ) {
					if ( $temp_level_new >= 0 && $temp_level_new <= $defense_array[$i]["maxTHLevel"] ) {
						$defense_array[$i]["level"][$j] = $temp_level_new;
						if ( $ww_flag == 0 ) {
							$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
							$member_warWeight_sql .= " FROM `API_WarWeight` ";
							$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $defense_array[$i]["name"] . "' ";
							$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
							$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
							$member_warWeight_result = $conn->query($member_warWeight_sql);
	
							$temp_ww = array();
							$k = 1;
							while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
								$temp_ww[$k] = $member_warWeight_row["ww_weight"];
								$k++;
							}
							$ww_flag = 1;
						}
						$temp_ww_change = $temp_ww[$temp_level_new] - $temp_ww[$temp_level_old];
						$defense_array[$i]["warWeight"] = $defense_array[$i]["warWeight"] + $temp_ww_change;
						$warWeight['bldg_defense'] = $warWeight['bldg_defense'] + $temp_ww_change;
						$warWeight['total'] = $warWeight['total'] + $temp_ww_change;									
						$warWeight['flag'] = 1;
					} else {
						echo "ERROR: " . $defense_array[$i]["name"] . " number " . $j;
						echo " is not in allowable range for town hall level: ";
						echo "Range is 0 - " . $defense_array[$i]["maxTHLevel"] . "<br>";
						$error = $error + 1;
						continue;
					}
				}
			}
		}
	}
	// 20 == walls
	if ( !empty ( $defense_array[20]["name"] )) {
		$ww_flag = 0;
		$temp_cnt_total = 0;
		for ( $j = 1; $j <= $defense_array[20]["thCnt"]; $j++ ) {
			$temp_cnt_new = $_POST["def_" . 20 . "_" . $j];
			$temp_cnt_old = $defense_array[20]["cnt"][$j];
			$temp_cnt_total = $temp_cnt_total + $temp_cnt_new;
			if ( $temp_cnt_new != $temp_cnt_old ) {
				if ( $temp_cnt_new >= 0 && $temp_cnt_new <= $defense_array[20]["thCnt"] ) {
					$defense_array[20]["cnt"][$j] = $temp_cnt_new;
					if ( $ww_flag == 0 ) {
						$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
						$member_warWeight_sql .= " FROM `API_WarWeight` ";
						$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $defense_array[20]["name"] . "' ";
						$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
						$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
						$member_warWeight_result = $conn->query($member_warWeight_sql);

						$temp_ww = array();
						$k = 1;
						while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
							$temp_ww[$k] = $member_warWeight_row["ww_weight"];
							$k++;
						}
						$ww_flag = 1;
					}
				} else {
					echo "ERROR: " . $defense_array[$i]["name"] . " level " . $j;
					echo " is not a legal entry.<br>";
					$error = $error + 1;
					continue;
				}
				$temp_ww_change = ( $temp_ww[$j] * $temp_cnt_new ) - ( $temp_ww[$j] * $temp_cnt_old );
				$defense_array[20]["warWeight"] = $defense_array[20]["warWeight"] + $temp_ww_change;
				$warWeight['wall_defense'] = $warWeight['wall_defense'] + $temp_ww_change;
				$warWeight['bldg_defense'] = $warWeight['bldg_defense'] + $temp_ww_change;
				$warWeight['total'] = $warWeight['total'] + $temp_ww_change;									
				$warWeight['flag'] = 1;
			} 
		}
		$defense_array[20]["cnt"]['total'] = $temp_cnt_total;
		if ( $defense_array[20]['thCnt'] < $temp_cnt_total ) {
			echo "ERROR: Total count for walls (" . $temp_cnt_total . ") exceed Town Hall max count. <br>";
			$error = $error + 1;
		}
				// && $temp_cnt_total < $defense_array[20]["maxTHLevel"] 
	}
	return $error;
}		
		
function maxTHLevel ( $inputName, $inputTH, $conn ) {
	$maxTHLevel = 0;
	
	// escape single quotes
	$inputName = addslashes($inputName);	
	
	// Freeze spell is special
	if ( $inputName == 'Freeze' && $inputTH = 9 ) {
		return 1;
	}
	
	$maxTHLevel_sql = "SELECT tww.`ww_level`, lww.`ww_thLevel`  ";
	$maxTHLevel_sql .= " FROM `API_WarWeight` AS tww ";
	$maxTHLevel_sql .= " INNER JOIN `API_WarWeight` AS lww ";
	$maxTHLevel_sql .= "   ON tww.`ww_labLevel` = lww.`ww_level` ";
	$maxTHLevel_sql .= " WHERE tww.`ww_name` LIKE '" . $inputName . "' ";
	$maxTHLevel_sql .= "   AND lww.`ww_name` LIKE 'Laboratory' ";
	$maxTHLevel_sql .= " ORDER BY tww.`ww_level` ASC;";
	$maxTHLevel_result = $conn->query($maxTHLevel_sql);
	
	while($maxTHLevel_row = $maxTHLevel_result->fetch_assoc()) {
		$temp_level = $maxTHLevel_row["ww_level"];
		$temp_thLevel = $maxTHLevel_row["ww_thLevel"];
		
		if ( $temp_thLevel <= $inputTH ) {
			$maxTHLevel = $temp_level;
		}
	}
	
	return $maxTHLevel;
}

?>