<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$member_selected = "#R280P220";
	$clan_array = array();
	$member_array = array();
	$debug = 0;
	
	if ( $_GET['debug'] == "1" ) {
		# Show Top 10 Sent Detail
		echo "{jumi [API/API_member_donation_topSent.php]}";

	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# pull newest time stamp from sql
	$timestamp_sql = "SELECT `member_timestamp` ";
	$timestamp_sql .= " FROM `API_Member` ";
	$timestamp_sql .= " ORDER BY `member_timestamp` DESC ";
	$timestamp_sql .= " LIMIT 1 ;";
	$timestamp_result = $conn->query($timestamp_sql);
	
	while($timestamp_row = $timestamp_result->fetch_assoc()) {
		$timestamp_current = $timestamp_row["member_timestamp"];
	}
	
	# get past dates
	$timestamp_day = date_format ( date_sub ( date_create ( $timestamp_current ), date_interval_create_from_date_string('1 days' )), "Y-m-d H" );
	$timestamp_week = date_format ( date_sub ( date_create ( $timestamp_current ), date_interval_create_from_date_string('7 days' )), "Y-m-d H" );
	
	####################
	# SQL QUERIES
	####################
	# get total donations for everyone, with clan tag and league
	$totalDon_sql = "SELECT `member_tag`, `clan_name`, `league_iconUrls_tiny`, `member_name`, `member_role`, ";
	$totalDon_sql .= " `member_trophies`, `member_donations`, `member_total_donations`, ";
	$totalDon_sql .= " `member_donationsReceived`, `member_total_donationsReceived`";
	$totalDon_sql .= " FROM `API_Member` AS am ";
	$totalDon_sql .= " INNER JOIN `API_League` AS al ";
	$totalDon_sql .= "  ON am.`member_league_id`=al.`league_id` ";
	$totalDon_sql .= " INNER JOIN `API_Clan` AS ac ";
	$totalDon_sql .= "  ON am.`member_clan_tag`=ac.`clan_tag` ";
	$totalDon_sql .= " WHERE `member_timestamp`='" . $timestamp_current . "';";
	$totalDon_result = $conn->query($totalDon_sql);
	
	$count = 0;
	while($totalDon_row = $totalDon_result->fetch_assoc()) {
		$member_array[$count]["flag_current"] = 1;
		$member_array[$count]["tag"] = str_replace ( '#', '', $totalDon_row["member_tag"] );
		$member_array[$count]["clan_name"] = $totalDon_row["clan_name"];
		$member_array[$count]["league_iconUrls_tiny"] = $totalDon_row["league_iconUrls_tiny"];
		$member_array[$count]["name"] = $totalDon_row["member_name"];
		$member_array[$count]["role"] = $totalDon_row["member_role"];
		$member_array[$count]["trophies"] = $totalDon_row["member_trophies"];
		$member_array[$count]["donations"] = $totalDon_row["member_donations"];
		$member_array[$count]["total_donations"] = $totalDon_row["member_total_donations"];
		$member_array[$count]["donationsReceived"] = $totalDon_row["member_donationsReceived"];
		$member_array[$count]["total_donationsReceived"] = $totalDon_row["member_total_donationsReceived"];
		$count++;
	}	
	
	# get donations from one day ago
	$totalDon_day_sql = "SELECT `member_tag`, `member_total_donations`, `member_total_donationsReceived` ";
	$totalDon_day_sql .= "  FROM `API_Member` ";
	$totalDon_day_sql .= " WHERE `member_timestamp` LIKE '" . $timestamp_day . "%';";
	$totalDon_day_result = $conn->query($totalDon_day_sql);
	
	while($totalDon_day_row = $totalDon_day_result->fetch_assoc()) {
		$tag_temp = str_replace ( '#', '', $totalDon_day_row["member_tag"] );
		$key = array_search( $tag_temp, array_column($member_array, 'tag' ));
		if ( $key === 0 || $key ) {
			$member_array[$key]["flag_day"] = 1;
			$member_array[$key]["total_donations_day"] = $totalDon_day_row["member_total_donations"];
			$member_array[$key]["total_donationsReceived_day"] = $totalDon_day_row["member_total_donationsReceived"];
		}
	}
	
	# get donations from one week ago
	$totalDon_week_sql = "SELECT `member_tag`, `member_total_donations`, `member_total_donationsReceived` ";
	$totalDon_week_sql .= "  FROM `API_Member` ";
	$totalDon_week_sql .= " WHERE `member_timestamp` LIKE '" . $timestamp_week . "%';";
	$totalDon_week_result = $conn->query($totalDon_week_sql);
	
	while($totalDon_week_row = $totalDon_week_result->fetch_assoc()) {
		$tag_temp = str_replace ( '#', '', $totalDon_week_row["member_tag"] );
		$key = array_search( $tag_temp, array_column($member_array, 'tag' ));
		if ( $key === 0 || $key ) {
			$member_array[$key]["flag_week"] = 1;
			$member_array[$key]["total_donations_week"] = $totalDon_week_row["member_total_donations"];
			$member_array[$key]["total_donationsReceived_week"] = $totalDon_week_row["member_total_donationsReceived"];
		}
	}
	
	####################
	# Compute Ratios
	####################
	foreach ($member_array as $key => $value) {
		# check if current member
		if ( $member_array[$key]["flag_current"] == 1 ) {
			# donation send to receive ratio
			if ( $member_array[$key]["total_donationsReceived"] != 0 ) {
				$member_array[$key]["total_donations_ratio"] = strval ( round ( $member_array[$key]["total_donations"] / $member_array[$key]["total_donationsReceived"] * 100 ));
			} else {
				$member_array[$key]["total_donations_ratio"] = $member_array[$key]["total_donations"] * 100;
			}

			# donation receive to send ratio
			if ( $member_array[$key]["total_donations"] != 0 ) {
				$member_array[$key]["total_donationsReceived_ratio"] = strval ( round ( $member_array[$key]["total_donationsReceived"] / $member_array[$key]["total_donations"] * 100 ));
			} else {
				$member_array[$key]["total_donationsReceived_ratio"] = $member_array[$key]["total_donationsReceived"] * 100;
			}
		}
		
		# check if member one day ago
		if ( $member_array[$key]["flag_day"] == 1 ) {
			# get donations sent over last day
			$member_array[$key]["donations_day"] = $member_array[$key]["total_donations"] - $member_array[$key]["total_donations_day"];
			
			# get donations sent over last day
			$member_array[$key]["donationsReceived_day"] = $member_array[$key]["total_donationsReceived"] - $member_array[$key]["total_donationsReceived_day"];

			# donation send to receive ratio over last day
			if ( $member_array[$key]["donationsReceived_day"] != 0 ) {
				$member_array[$key]["total_donations_ratio_day"] = strval ( round ( $member_array[$key]["donations_day"] / $member_array[$key]["donationsReceived_day"] * 100 ));
			} else {
				$member_array[$key]["total_donations_ratio_day"] = $member_array[$key]["donations_day"] * 100;
			}

			# donation receive to send ratio over last day
			if ( $member_array[$key]["donations_day"] != 0 ) {
				$member_array[$key]["total_donationsReceived_ratio_day"] = strval ( round ( $member_array[$key]["donationsReceived_day"] / $member_array[$key]["donations_day"] * 100 ));
			} else {
				$member_array[$key]["total_donationsReceived_ratio_day"] = $member_array[$key]["donationsReceived_day"] * 100;
			}
		}
		
		# check if member one week ago
		if ( $member_array[$key]["flag_week"] == 1 ) {
			# get donations sent over last day
			$member_array[$key]["donations_week"] = $member_array[$key]["total_donations"] - $member_array[$key]["total_donations_week"];
			
			# get donations sent over last day
			$member_array[$key]["donationsReceived_week"] = $member_array[$key]["total_donationsReceived"] - $member_array[$key]["total_donationsReceived_week"];

			# donation send to receive ratio over last day
			if ( $member_array[$key]["donationsReceived_week"] != 0 ) {
				$member_array[$key]["total_donations_ratio_week"] = strval ( round ( $member_array[$key]["donations_week"] / $member_array[$key]["donationsReceived_week"] * 100 ));
			} else {
				$member_array[$key]["total_donations_ratio_week"] = $member_array[$key]["donations_week"] * 100;
			}

			# donation receive to send ratio over last day
			if ( $member_array[$key]["donations_week"] != 0 ) {
				$member_array[$key]["total_donationsReceived_ratio_week"] = strval ( round ( $member_array[$key]["donationsReceived_week"] / $member_array[$key]["donations_week"] * 100 ));
			} else {
				$member_array[$key]["total_donationsReceived_ratio_week"] = $member_array[$key]["donationsReceived_week"] * 100;
			}
		}
		if ( ( $debug )&&( $member_array[$key]["tag"]=="8LY009CGJ" ) ) {
			echo "name = " . $member_array[$key]["name"] . "<br>";
			echo "flag_current = " . $member_array[$key]["flag_current"] . "<br>";
			echo "total_donations = " . $member_array[$key]["total_donations"] . "<br>";
			echo "total_donations_week = " . $member_array[$key]["total_donations_week"] . "<br>";
			echo "donations_week = " . $member_array[$key]["donations_week"] . "<br>";
			echo "total_donations_ratio_week = " . $member_array[$key]["total_donations_ratio_week"] . "<br>";
			echo "total_donationsReceived = " . $member_array[$key]["total_donationsReceived"] . "<br>";
			echo "total_donationsReceived_week = " . $member_array[$key]["total_donationsReceived_week"] . "<br>";
			echo "donationsReceived_week = " . $member_array[$key]["donationsReceived_week"] . "<br>";
			echo "total_donationsReceived_ratio_week = " . $member_array[$key]["total_donationsReceived_ratio_week"] . "<br>";
		}
	
	} 
	
	####################
	# SETUP SEARCHES
	####################
	# top donation sent search
	function total_donations_current( $a, $b ){
		return $b['total_donations'] - $a['total_donations'];
	}

	# top donation received search
	function total_donationsReceived_current( $a, $b ){
		return $b['total_donationsReceived'] - $a['total_donationsReceived'];
	}

	# top donation sent ratio search
	function total_donations_ratio_current( $a, $b ){
		return $b['total_donations_ratio'] - $a['total_donations_ratio'];
	}

	# top donation received ratio search
	function total_donationsReceived_ratio_current( $a, $b ){
		return $b['total_donationsReceived_ratio'] - $a['total_donationsReceived_ratio'];
	}

	# top donation sent last day search
	function total_donations_day( $a, $b ){
		return $b['donations_day'] - $a['donations_day'];
	}

	# top donation received last day search
	function total_donationsReceived_day( $a, $b ){
		return $b['donationsReceived_day'] - $a['donationsReceived_day'];
	}

	# top donation sent ratio last day search
	function total_donations_ratio_day( $a, $b ){
		return $b['total_donations_ratio_day'] - $a['total_donations_ratio_day'];
	}

	# top donation received ratio last day search
	function total_donationsReceived_ratio_day( $a, $b ){
		return $b['total_donationsReceived_ratio_day'] - $a['total_donationsReceived_ratio_day'];
	}

	# top donation sent last week search
	function total_donations_week( $a, $b ){
		return $b['donations_week'] - $a['donations_week'];
	}

	# top donation received last week search
	function total_donationsReceived_week( $a, $b ){
		return $b['donationsReceived_week'] - $a['donationsReceived_week'];
	}

	# top donation sent ratio last week search
	function total_donations_ratio_week( $a, $b ){
		return $b['total_donations_ratio_week'] - $a['total_donations_ratio_week'];
	}

	# top donation received ratio last week search
	function total_donationsReceived_ratio_week( $a, $b ){
		return $b['total_donationsReceived_ratio_week'] - $a['total_donationsReceived_ratio_week'];
	}

	####################
	# DISPLAY HTML
	####################
	if ( $_GET['display'] == "topSent" ) {
		# Show Top 10 Sent Detail
		echo "{jumi [API/API_member_donation_topSent.php]}";
	} elseif ( $_GET['display'] == "topRec" ) {
		# Show Top 10 Receive Detail
		echo "{jumi [API/API_member_donation_topRec.php]}";
	} elseif ( $_GET['display'] == "topSentRat" ) {
		# Show Top 10 Sent Ratio Detail
		echo "{jumi [API/API_member_donation_topSentRat.php]}";
	} elseif ( $_GET['display'] == "topRecRat" ) {
		# Show Top 10 Receive Ratio Detail
		echo "{jumi [API/API_member_donation_topRecRat.php]}";
	} else {
		# Show Default Top 10 
		echo "{jumi [API/API_member_donation_stats.php]}";
	}
	
	if ( $debug ) {
		echo var_dump ( $member_array );
	}
?>
		
