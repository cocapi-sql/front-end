<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$clan_selected = "BLACK LIST";
	$clan_array = array();
	$members_array = array();
	$warWeight_array = array();
	$member_timestamp;
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# Get clan or use default set to BlackList
	if ( isset ( $_GET['clan'] ) ) {
		$clan_selected = $_GET['clan'];
	}
	
	$clan_sql = "SELECT * ";
	$clan_sql .= "FROM `API_Clan` AS ac ";
	$clan_sql .= "INNER JOIN `API_Clan_Info` AS aci ";
	$clan_sql .= "  ON ac.clan_tag=aci.clan_info_tag ";
	$clan_sql .= "WHERE clan_name LIKE '" . $clan_selected . "' ";
	$clan_sql .= "ORDER BY clan_info_time DESC ";
	$clan_sql .= "LIMIT 1;";
	$clan_result = $conn->query($clan_sql);
	

	while($clan_row = $clan_result->fetch_assoc()) {
		$clan_array["clan_tag"] = $clan_row["clan_tag"];
		$clan_array["clan_name"] = $clan_row["clan_name"];
		$clan_array["clan_description"] = str_replace( '\\', '', $clan_row["clan_description"] );
		$clan_array["clan_badgeUrls_medium"] = $clan_row["clan_badgeUrls_medium"];
	}

	$members_sql = "SELECT `member_tag`, `member_clan_tag`, `member_league_id`, `member_name`, ";
	$members_sql .= "`member_timestamp`, `member_clanRank`, `member_previousClanRank`, `member_expLevel`, ";
	$members_sql .= "`member_role`, `member_townHallLevel`, `member_trophies`, `member_bestTrophies`, ";
	$members_sql .= "`member_warStars`, `member_attackWins`, `member_defenseWins`, `member_donations`, ";
	$members_sql .= "`member_total_donations`, `member_donationsReceived`, `member_total_donationsReceived`, ";
	$members_sql .= "`league_iconUrls_tiny` ";
	$members_sql .= "FROM `API_Member` AS am ";
	$members_sql .= "INNER JOIN `API_Clan` AS ac ";
	$members_sql .= " ON am.`member_clan_tag`=ac.`clan_tag` ";
	$members_sql .= "INNER JOIN `API_League` AS al ";
	$members_sql .= " ON am.`member_league_id`=al.`league_id` ";
	$members_sql .= "WHERE ac.`clan_name` LIKE '" . $clan_selected . "' ";
	$members_sql .= " AND am.`member_timestamp` = ( ";
	$members_sql .= "  SELECT MAX(`member_timestamp`) ";
	$members_sql .= "  FROM `API_Member` ";
	$members_sql .= "  WHERE `member_clan_tag` LIKE '" . $clan_array["clan_tag"] . "' ";
	$members_sql .= "  ORDER BY `member_timestamp` DESC ";
	$members_sql .= "  LIMIT 1 ) ";
	$members_sql .= "ORDER BY am.`member_clanRank` ASC;";
	$members_result = $conn->query($members_sql);
	
	while($member_row = $members_result->fetch_assoc()) {
		$temp_id = $member_row["member_clanRank"];
		$member_timestamp = $member_row["member_timestamp"];
		$members_array[$temp_id]["member_tag"] = $member_row["member_tag"];
		$members_array[$temp_id]["member_name"] = $member_row["member_name"];
		$members_array[$temp_id]["member_clanRank"] = $member_row["member_clanRank"];
		$members_array[$temp_id]["member_previousClanRank"] = $member_row["member_previousClanRank"];
		$members_array[$temp_id]["member_expLevel"] = $member_row["member_expLevel"];
		$members_array[$temp_id]["member_role"] = $member_row["member_role"];
		$members_array[$temp_id]["member_townHallLevel"] = $member_row["member_townHallLevel"];
		$members_array[$temp_id]["member_trophies"] = $member_row["member_trophies"];
		$members_array[$temp_id]["member_bestTrophies"] = $member_row["member_bestTrophies"];
		$members_array[$temp_id]["member_warStars"] = $member_row["member_warStars"];
		$members_array[$temp_id]["member_attackWins"] = $member_row["member_attackWins"];
		$members_array[$temp_id]["member_defenseWins"] = $member_row["member_defenseWins"];
		$members_array[$temp_id]["member_donations"] = $member_row["member_donations"];
		$members_array[$temp_id]["member_total_donations"] = $member_row["member_total_donations"];
		$members_array[$temp_id]["member_donationsReceived"] = $member_row["member_donationsReceived"];
		$members_array[$temp_id]["member_total_donationsReceived"] = $member_row["member_total_donationsReceived"];
		$members_array[$temp_id]["league_iconUrls_tiny"] = $member_row["league_iconUrls_tiny"];
		$members_array[$temp_id]["get_member_id"] = str_replace ( '#', '', $member_row["member_tag"] );
		
		$member_ww_sql = "SELECT  `memWW_unadjusted` ,  `memWW_adjusted` ,  `memWW_penalty` ";
		$member_ww_sql .= " FROM  `API_Mem_WarWeight` ";
		$member_ww_sql .= "WHERE  `memWW_member_tag` LIKE  '" . $members_array[$temp_id]["member_tag"] . "' ";
		$member_ww_sql .= "ORDER BY  `memWW_timestamp` DESC ";
		$member_ww_sql .= "LIMIT 1;";
		$member_ww_result = $conn->query($member_ww_sql);
		
		$members_array[$temp_id]['ww_flag'] = 0;
		while($member_ww_row = $member_ww_result->fetch_assoc()) {
			$members_array[$temp_id]['ww_unadjusted'] = $member_ww_row['memWW_unadjusted'];
			$members_array[$temp_id]['ww_adjusted'] = $member_ww_row['memWW_adjusted'];
			$members_array[$temp_id]['ww_penalty'] = $member_ww_row['memWW_penalty'];
			$members_array[$temp_id]['ww_flag'] = 1;
		}	
	}	
	
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>

	<h1 align="center"><?php echo $clan_array["clan_name"]; ?></h1>
	<center>
		<img src="<?php echo $clan_array["clan_badgeUrls_medium"]; ?>" /><br>
		<?php echo $clan_array["clan_description"]; ?><br>
	</center>

	<table width="100%" class="sortable">
		<thead>
			<th align="center"><b>Rank</b></th>
			<th align="center"><b>Name</b></th>
			<th align="center"><b>League</b></th>
			<th align="center"><b>Town<br>Hall</b></th>
			<th align="center"><b>Unadjusted<br>War Weight</b></th>
			<th align="center"><b>Adjusted<br>War Weight</b></th>
			<th align="center"><b>War Weight<br>Penalty</b></th>
		</thead>
		<tbody>
			<?php foreach ( $members_array as $temp_id => $value ) { ?>
				<tr> 
					<td align="center">
						<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
					</td>
					<td>
						<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=69&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
					</td>
					<td align="center">
						<img src="<?php echo $members_array[$temp_id]["league_iconUrls_tiny"]; ?>" /> 
					</td>
					<td align="center">
						<?php echo $members_array[$temp_id]["member_townHallLevel"]; ?>
					</td>
					<?php if ( $members_array[$temp_id]['ww_flag'] == 1 ) { ?>
						<td align="center">
							<?php echo $members_array[$temp_id]["ww_unadjusted"]; ?>
						</td>
						<td align="center">
							<?php echo $members_array[$temp_id]["ww_adjusted"]; ?>
						</td>
						<td align="center">
							<?php 
								if ( $members_array[$temp_id]["ww_penalty"] == 0 ) {
									echo "<font color=\"green\">" . $members_array[$temp_id]["ww_penalty"] . "</font>"; 
								} elseif ( $members_array[$temp_id]["ww_penalty"] < 5000 ) {
									echo "<font color=\"yellow\">" . $members_array[$temp_id]["ww_penalty"] . "</font>"; 
								} else {
									echo "<font color=\"red\">" . $members_array[$temp_id]["ww_penalty"] . "</font>"; 
								}
							?>
						</td>
					<?php } else { ?>
						<td align="center">
							N/A
						</td>
						<td align="center">
							N/A
						</td>
						<td align="center">
							N/A
						</td>

					<?php } ?>
				</tr>
			<?php } ?>
		</tbody>
	</table>


<center>
	<b>This data was updated at <?php echo $member_timestamp; ?> GMT.</b>
</center>
</br>  

<h2 style="text-align: center;">Clan Picker</h2>
<form method="get">

	<center>
		<input type="radio" name="clan" value="BLACK LIST">&nbsp; Black List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="WHITE LIST">&nbsp; White List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="GOLD LIST">&nbsp; Gold List &nbsp;&nbsp;</input>
	</center>
	<center><input type="submit" name="submit" value="Submit"/></center>
</form>

<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
<!-- JQuery Script -->
<link rel="stylesheet" href="/images/jumi_code/jquery-ui.theme.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#accordion,#accordion_troop,#accordion_hero" ).accordion({
			heightStyle: "content"
		});
		$( "#accordion,#accordion_troop,#accordion_hero" ).accordion({
			collapsible: true
		});
		$( "#accordion" ).accordion({
			active: 7
		});
		$( "#accordion_troop" ).accordion({
			active: 3
		});
		$( "#accordion_hero" ).accordion({
			active: 1
		});
	});
</script>


</body>