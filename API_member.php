<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$clan_selected = "BLACK LIST";
	$clan_array = array();
	$members_array = array();
	$member_timestamp;
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# Get clan or use default set to BlackList
	if ( isset ( $_GET['clan'] ) ) {
		$clan_selected = $_GET['clan'];
	}
	
	$clan_sql = "SELECT * ";
	$clan_sql .= "FROM `API_Clan` AS ac ";
	$clan_sql .= "INNER JOIN `API_Clan_Info` AS aci ";
	$clan_sql .= "  ON ac.clan_tag=aci.clan_info_tag ";
	$clan_sql .= "WHERE clan_name LIKE '" . $clan_selected . "' ";
	$clan_sql .= "ORDER BY clan_info_time DESC ";
	$clan_sql .= "LIMIT 1;";
	$clan_result = $conn->query($clan_sql);
	

	while($clan_row = $clan_result->fetch_assoc()) {
		$clan_array["clan_tag"] = $clan_row["clan_tag"];
		$clan_array["clan_name"] = $clan_row["clan_name"];
		$clan_array["clan_description"] = str_replace( '\\', '', $clan_row["clan_description"] );
		$clan_array["clan_badgeUrls_medium"] = $clan_row["clan_badgeUrls_medium"];
	}

	$members_sql = "SELECT `member_tag`, `member_clan_tag`, `member_league_id`, `member_name`, ";
	$members_sql .= "`member_current_timestamp`, `member_clanRank`, `member_previousClanRank`, `member_expLevel`, ";
	$members_sql .= "`member_role`, `member_townHallLevel`, `member_trophies`, `member_bestTrophies`, ";
	$members_sql .= "`member_warStars`, `member_attackWins`, `member_defenseWins`, `member_donations`, ";
	$members_sql .= "`member_total_donations`, `member_donationsReceived`, `member_total_donationsReceived`, ";
	$members_sql .= "`league_iconUrls_tiny` ";
	$members_sql .= "FROM `API_Member` AS am ";
	$members_sql .= "INNER JOIN `API_Clan` AS ac ";
	$members_sql .= " ON am.`member_clan_tag`=ac.`clan_tag` ";
	$members_sql .= "INNER JOIN `API_League` AS al ";
	$members_sql .= " ON am.`member_league_id`=al.`league_id` ";
	$members_sql .= "WHERE ac.`clan_name` LIKE '" . $clan_selected . "' ";
	$members_sql .= " AND am.`member_current_timestamp` = ( ";
	$members_sql .= "  SELECT MAX(`member_current_timestamp`) ";
	$members_sql .= "  FROM `API_Member` ";
	$members_sql .= "  WHERE `member_clan_tag` LIKE '" . $clan_array["clan_tag"] . "' ";
	$members_sql .= "  ORDER BY `member_current_timestamp` DESC ";
	$members_sql .= "  LIMIT 1 ) ";
	$members_sql .= "ORDER BY am.`member_clanRank` ASC;";
	$members_result = $conn->query($members_sql);
	
	while($member_row = $members_result->fetch_assoc()) {
		$temp_id = $member_row["member_clanRank"];
		$member_timestamp = $member_row["member_current_timestamp"];
		$members_array[$temp_id]["member_tag"] = $member_row["member_tag"];
		$members_array[$temp_id]["member_name"] = $member_row["member_name"];
		$members_array[$temp_id]["member_clanRank"] = $member_row["member_clanRank"];
		$members_array[$temp_id]["member_previousClanRank"] = $member_row["member_previousClanRank"];
		$members_array[$temp_id]["member_expLevel"] = $member_row["member_expLevel"];
		$members_array[$temp_id]["member_role"] = $member_row["member_role"];
		$members_array[$temp_id]["member_townHallLevel"] = $member_row["member_townHallLevel"];
		$members_array[$temp_id]["member_trophies"] = $member_row["member_trophies"];
		$members_array[$temp_id]["member_bestTrophies"] = $member_row["member_bestTrophies"];
		$members_array[$temp_id]["member_warStars"] = $member_row["member_warStars"];
		$members_array[$temp_id]["member_attackWins"] = $member_row["member_attackWins"];
		$members_array[$temp_id]["member_defenseWins"] = $member_row["member_defenseWins"];
		$members_array[$temp_id]["member_donations"] = $member_row["member_donations"];
		$members_array[$temp_id]["member_total_donations"] = $member_row["member_total_donations"];
		$members_array[$temp_id]["member_donationsReceived"] = $member_row["member_donationsReceived"];
		$members_array[$temp_id]["member_total_donationsReceived"] = $member_row["member_total_donationsReceived"];
		$members_array[$temp_id]["league_iconUrls_tiny"] = $member_row["league_iconUrls_tiny"];
		$members_array[$temp_id]["get_member_id"] = str_replace ( '#', '', $member_row["member_tag"] );
	}	
		
	// loop threw members
	foreach ($members_array as $key => $value) {
		// get member's highest rank from sql
    	$sql = "SELECT `member_clanRank` ";
    	$sql .= "FROM `API_Member` ";
    	$sql .= "WHERE `member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "ORDER BY `member_clanRank` ASC ";
    	$sql .= "LIMIT 1; ";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$members_array[$key]["member_high_clanRank"] = $row["member_clanRank"];
		}
		
		// get member's lowest rank from sql
    	$sql = "SELECT `member_clanRank` ";
    	$sql .= "FROM `API_Member` ";
    	$sql .= "WHERE `member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "ORDER BY `member_clanRank` DESC ";
    	$sql .= "LIMIT 1; ";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$members_array[$key]["member_low_clanRank"] = $row["member_clanRank"];
		}
		
		// get member's hero info from sql
		$sql = "SELECT * ";
    	$sql .= "FROM (SELECT `heroToMem_heros_name`, `heroToMem_level` ";
    	$sql .= "  FROM `API_Mem_HerosToMem` ";
    	$sql .= "  WHERE `heroToMem_member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "  ORDER BY `heroToMem_level` DESC) as inside ";
    	$sql .= "GROUP BY `heroToMem_heros_name`;";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$temp_name = $row["heroToMem_heros_name"];
			$temp_level = $row["heroToMem_level"];
			$members_array[$key]["Hero"][$temp_name]["Level"] = $temp_level;
		}
		
		// get member's newest hero from sql
		$sql = "SELECT `heroToMem_id`, `heroToMem_heros_name`, `heroToMem_level`, `hero_maxLevel` ";
    	$sql .= "FROM `API_Mem_HerosToMem` AS amhm ";
    	$sql .= "INNER JOIN `API_Mem_Heros` AS amh ";
    	$sql .= "  ON amhm.heroToMem_heros_name=amh.hero_name ";
    	$sql .= "WHERE `heroToMem_member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "ORDER BY amhm.`heroToMem_id`  DESC ";
    	$sql .= "LIMIT 1;";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$temp_name = $row["heroToMem_heros_name"];
			$temp_level = $row["heroToMem_level"];
			$members_array[$key]["Hero"]["New"]["Name"] = $temp_name;
			$members_array[$key]["Hero"]["New"]["Level"] = $temp_level;
			$members_array[$key]["Hero"]["New"]["MaxLevel"] = $row["hero_maxLevel"];
		}

		// get member's troop info from sql
		$sql = "SELECT * ";
    	$sql .= "FROM (SELECT `troopsToMem_troops_name`, `troopsToMem_level` ";
    	$sql .= "  FROM `API_Mem_TroopsToMem` ";
    	$sql .= "  WHERE `troopsToMem_member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "  ORDER BY `troopsToMem_level` DESC) as inside ";
    	$sql .= "GROUP BY `troopsToMem_troops_name`;";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$temp_name = $row["troopsToMem_troops_name"];
			$temp_level = $row["troopsToMem_level"];
			$members_array[$key]["Troop"][$temp_name]["Level"] = $temp_level;
		}
		
		// get members newest troop from sql
		$sql = "SELECT `troopsToMem_id`, `troopsToMem_troops_name`, `troopsToMem_level`, `troops_maxLevel` ";
    	$sql .= "FROM `API_Mem_TroopsToMem` AS amtm ";
    	$sql .= "INNER JOIN `API_Mem_Troops` AS amt ";
    	$sql .= "  ON amtm.troopsToMem_troops_name=amt.troops_name ";
    	$sql .= "WHERE `troopsToMem_member_tag` LIKE '" . $members_array[$key]["member_tag"] . "' ";
    	$sql .= "ORDER BY amtm.`troopsToMem_id`  DESC ";
    	$sql .= "LIMIT 1;";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$temp_name = $row["troopsToMem_troops_name"];
			$temp_level = $row["troopsToMem_level"];
			$members_array[$key]["Troop"]["New"]["Name"] = $temp_name;
			$members_array[$key]["Troop"]["New"]["Level"] = $temp_level;
			$members_array[$key]["Troop"]["New"]["MaxLevel"] = $row["troops_maxLevel"];
		}

		$sql = "SELECT * ";
		$sql .= "FROM  `API_Mem_Wars` ";
		$sql .= "WHERE  `member_tag` LIKE '" . $members_array[$key]["member_tag"] . "';";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$members_array[$key]['War']['war'] = $row['member_war'];
			$members_array[$key]['War']['attack_used'] = $row['member_attack_used'];
			$members_array[$key]['War']['attack_stars'] = $row['member_attack_stars'];
			$members_array[$key]['War']['attack_newStars'] = $row['member_attack_newStars'];
			$members_array[$key]['War']['attack_3Stars'] = $row['member_attack_3Stars'];
			$members_array[$key]['War']['attack_2Stars'] = $row['member_attack_2Stars'];
			$members_array[$key]['War']['attack_1Stars'] = $row['member_attack_1Stars'];
			$members_array[$key]['War']['attack_0Stars'] = $row['member_attack_0Stars'];
			$members_array[$key]['War']['attack_3NewStars'] = $row['member_attack_3NewStars'];
			$members_array[$key]['War']['attack_2NewStars'] = $row['member_attack_2NewStars'];
			$members_array[$key]['War']['attack_1NewStars'] = $row['member_attack_1NewStars'];
			$members_array[$key]['War']['attack_0NewStars'] = $row['member_attack_0NewStars'];
			$members_array[$key]['War']['defense_used'] = $row['member_defense_used'];
			$members_array[$key]['War']['defense_stars'] = $row['member_defense_stars'];
			$members_array[$key]['War']['defense_newStars'] = $row['member_defense_newStars'];
			$members_array[$key]['War']['defense_destruction'] = $row['member_defense_destruction'];
			$members_array[$key]['War']['defense_3Stars'] = $row['member_defense_3Stars'];
			$members_array[$key]['War']['defense_2Stars'] = $row['member_defense_2Stars'];
			$members_array[$key]['War']['defense_1Stars'] = $row['member_defense_1Stars'];
			$members_array[$key]['War']['defense_0Stars'] = $row['member_defense_0Stars'];
			$members_array[$key]['War']['defense_3NewStars'] = $row['member_defense_3NewStars'];
			$members_array[$key]['War']['defense_2NewStars'] = $row['member_defense_2NewStars'];
			$members_array[$key]['War']['defense_1NewStars'] = $row['member_defense_1NewStars'];
			$members_array[$key]['War']['defense_0NewStars'] = $row['member_defense_0NewStars'];
			$members_array[$key]['War']['clean_cnt'] = $row['member_clean_cnt'];
			$members_array[$key]['War']['loot_cnt'] = $row['member_loot_cnt'];
			$members_array[$key]['War']['earlyLoot_cnt'] = $row['member_earlyLoot_cnt'];
			$members_array[$key]['War']['noCC_cnt'] = $row['member_noCC_cnt'];
		}

		$sql = "SELECT * ";
		$sql .= "FROM  `API_Mem_WarWeight` ";
		$sql .= "WHERE  `memWW_member_tag` LIKE  '" . $members_array[$key]["member_tag"] . "' ";
		$sql .= "ORDER BY  `memWW_timestamp` DESC ";
		$sql .= "LIMIT 1;";
		$result = $conn->query($sql);
		
		while($row = $result->fetch_assoc()) {
			$temp_step4 = $row['memWW_Step4'];
			$temp_rounded = $row['memWW_rounded'];
			$temp_def = $row['memWW_defBldg'];
			if ( $temp_rounded == 0 ) {
				$temp_rounded = ' ';
			}
			if ( $temp_def == 0 ) {
				$members_array[$key]['WarWeight']['flag'] = 0;
			} else {
				$members_array[$key]['WarWeight']['flag'] = 1;
			}
			$members_array[$key]['WarWeight']['defense'] = $row['memWW_defense'];
			$members_array[$key]['WarWeight']['offense'] = $row['memWW_offense'];
			$members_array[$key]['WarWeight']['total'] = $row['memWW_total'];
			$members_array[$key]['WarWeight']['rounded'] = $temp_rounded;
			$members_array[$key]['WarWeight']['step4'] = $temp_step4;
			if ( $temp_step4 == NULL ) {
				$members_array[$key]['WarWeight']['step4_text'] = ' ';
			} elseif ( $temp_step4 == 0 ) {
				$members_array[$key]['WarWeight']['step4_text'] = '<font color="green">No</font>';
			} elseif ( $temp_step4 == 1 ) {
				$members_array[$key]['WarWeight']['step4_text'] = '<font color="yellow">Low</font>';
			} elseif ( $temp_step4 == 2 ) {
				$members_array[$key]['WarWeight']['step4_text'] = '<font color="orange">Medium</font>';
			} elseif ( $temp_step4 == 3 ) {
				$members_array[$key]['WarWeight']['step4_text'] = '<font color="red">High</font>';
			}
		}
		
		if ( !isset ( $members_array[$key]['WarWeight']['rounded'] )) {
			$sql = "SELECT * ";
			$sql .= "FROM  `API_WarWeight_TH` ";
			$sql .= "WHERE  `TH` =" . $members_array[$key]["member_townHallLevel"] . ";";
			$result = $conn->query($sql);
		
			while($row = $result->fetch_assoc()) {
				$members_array[$key]['WarWeight']['min'] = $row['WarWeight_Min'];
				$members_array[$key]['WarWeight']['median'] = $row['WarWeight_Median'];
				$members_array[$key]['WarWeight']['max'] = $row['WarWeight_Max'];
				$members_array[$key]['WarWeight']['rounded'] = $row['WarWeight_Rounded'];
			}
		}
    }
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>

	<h1 align="center"><?php echo $clan_array["clan_name"]; ?></h1>
	<center>
		<img src="<?php echo $clan_array["clan_badgeUrls_medium"]; ?>" /><br>
		<?php echo $clan_array["clan_description"]; ?><br>
	</center>

	<div id="tabs">
	  	<ul>
			<li><a href="#tabs-1">General Info.</a></li>
			<li><a href="#tabs-2">War Info.</a></li>
			<li><a href="#tabs-3">Home Village</a></li>
			<li><a href="#tabs-4">Builders Base</a></li>
	  	</ul>
		<div id="tabs-1">
			<center>
				<div id="accordion-general">
					<h3 align="center">Clan Info</h3>
					<div id="Clan Info">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Role</b></th>
								<th align="center"><b>Previous<br>Rank</b></th>
								<th align="center"><b>Highest<br>Rank</b></th>
								<th align="center"><b>Lowest<br>Rank</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td>
											<?php echo $members_array[$temp_id]["member_role"]; ?>
										</td>
										<td align="center">
											<?php if ( $members_array[$temp_id]["member_clanRank"] > $members_array[$temp_id]["member_previousClanRank"] ) {
												echo "<font color=\"red\">";
											} else {
												echo "<font color=\"green\">";
											}
											echo $members_array[$temp_id]["member_previousClanRank"]; ?>
											</font> 
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_high_clanRank"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_low_clanRank"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Member Info</h3>
					<div id="Member Info">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>League</b></th>
								<th align="center"><b>Trophies</b></th>
								<th align="center"><b>Best<br>Trophies</b></th>
								<th align="center"><b>Exp.<br>Level</b></th>
								<th align="center"><b>Town<br>Hall</b></th>
								<th align="center"><b>War<br>Stars</b></th>
								<th align="center"><b>Attack<br>Wins</b></th>
								<th align="center"><b>Defense<br>Wins</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<img src="<?php echo $members_array[$temp_id]["league_iconUrls_tiny"]; ?>" /> 
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_trophies"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_bestTrophies"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_expLevel"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_townHallLevel"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_warStars"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_attackWins"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["member_defenseWins"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Donation Info</h3>
					<div id="Donation Info">	
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div>
				</div>
			</center>
		</div>
		<div id="tabs-2">
			<center>
				<div id="accordion_war">
					<h3 align="center">War Offense</h3>
					<div id="War Offense">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Wars</b></th>
								<th align="center"><b>Attacks</b></th>
								<th align="center"><b>Stars</b></th>
								<th align="center"><b><br>3</b></th>
								<th align="center"><b><br>2</b></th>
								<th align="center"><b><br>1</b></th>
								<th align="center"><b><br>0</b></th>
								<th align="center"><b>New<br>Stars</b></th>
								<th align="center"><b><br>3</b></th>
								<th align="center"><b><br>2</b></th>
								<th align="center"><b><br>1</b></th>
								<th align="center"><b><br>0</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['war']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_used']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_3Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_2Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_1Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_0Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_newStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_3NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_2NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_1NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['attack_0NewStars']; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">War Defense</h3>
					<div id="War Defense">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Wars</b></th>
								<th align="center"><b>Defense</b></th>
								<th align="center"><b>Stars</b></th>
								<th align="center"><b><br>3</b></th>
								<th align="center"><b><br>2</b></th>
								<th align="center"><b><br>1</b></th>
								<th align="center"><b><br>0</b></th>
								<th align="center"><b>New<br>Stars</b></th>
								<th align="center"><b><br>3</b></th>
								<th align="center"><b><br>2</b></th>
								<th align="center"><b><br>1</b></th>
								<th align="center"><b><br>0</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['war']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_used']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_3Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_2Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_1Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_0Stars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_newStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_3NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_2NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_1NewStars']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['defense_0NewStars']; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">War Flags</h3>
					<div id="War Flags">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Wars</b></th>
								<th align="center"><b>Clean</b></th>
								<th align="center"><b>Loot</b></th>
								<th align="center"><b>Early Loot</b></th>
								<th align="center"><b>No Clan Castle</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['war']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['clean_cnt']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['loot_cnt']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['earlyLoot_cnt']; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]['War']['noCC_cnt']; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">War Weight</h3>
					<div id="War Weight">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Defense</b></th>
								<th align="center"><b>Offense</b></th>
								<th align="center"><b>Total</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
											<?php 
												if ( $members_array[$temp_id]['WarWeight']['flag'] === 0 ) {
													echo '*';
												}
											?>
										</td>
										<td align="right">
											<?php echo number_format ( $members_array[$temp_id]["WarWeight"]["defense"], 1, ".", "," ); ?>
										</td>
										<td align="right">
											<?php echo number_format ( $members_array[$temp_id]["WarWeight"]["offense"], 1, ".", "," ); ?>
										</td>
										<td align="right">
											<?php echo number_format ( $members_array[$temp_id]["WarWeight"]["total"], 1, ".", "," ); ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						* War Weight has not been entered for this member, contact Gary for help.
					</div>
					<h3 align="center">Engineered</h3>
					<div id="Engineered">	
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Total<br>War Weight</b></th>
								<th align="center"><b>Rounded<br>War Weight</b></th>
								<th align="center"><b>Engineered</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
											<?php 
												if ( $members_array[$temp_id]['WarWeight']['flag'] === 0 ) {
													echo '*';
												}
											?>
										</td>
										<td align="right">
											<?php echo number_format ( $members_array[$temp_id]["WarWeight"]["total"], 1, ".", "," ); ?>
										</td>
										<td align="right">
											<?php echo number_format ( $members_array[$temp_id]["WarWeight"]["rounded"], 0, ".", "," ); ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["WarWeight"]["step4_text"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						* War Weight has not been entered for this member, contact Gary for help.
					</div>
				</div> 
			</center> 
		</div>
		<div id="tabs-3">
			<center>
				<div id="accordion_home">
					<h3 align="center">Elixir Troop 1</h3>
					<div id="Elixir Troop 1">
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Barbarian</b></th>
								<th align="center"><b>Archer</b></th>
								<th align="center"><b>Giant</b></th>
								<th align="center"><b>Goblin</b></th>
								<th align="center"><b>Wall<br>Breaker</b></th>
								<th align="center"><b>Balloon</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Barbarian"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Archer"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Giant"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Goblin"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Wall Breaker"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Balloon"]["Level"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Elixir Troop 2</h3>
					<div id="Elixir Troop 2">
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Wizard</b></th>
								<th align="center"><b>Healer</b></th>
								<th align="center"><b>Dragon</b></th>
								<th align="center"><b>P.E.K.K.A</b></th>
								<th align="center"><b>Baby<br>Dragon</b></th>
								<th align="center"><b>Miner</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Wizard"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Healer"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Dragon"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["P.E.K.K.A"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Baby Dragon"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Miner"]["Level"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Dark Elixir Troop</h3>
					<div id="Dark Elixir Troop">
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Minion</b></th>
								<th align="center"><b>Hog<br>Rider</b></th>
								<th align="center"><b>Valkyrie</b></th>
								<th align="center"><b>Golem</b></th>
								<th align="center"><b>Witch</b></th>
								<th align="center"><b>Lava<br>Hound</b></th>
								<th align="center"><b>Bowler</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Minion"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Hog Rider"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Valkyrie"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Golem"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Witch"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Lava Hound"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["Bowler"]["Level"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Newest Troop</h3>
					<div id="Newest Troop">
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Troop</b></th>
								<th align="center"><b>Level</b></th>
								<th align="center"><b>Max Level</b></th>
								<th align="center"><b>War Weight</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["New"]["Name"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["New"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["New"]["MaxLevel"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Troop"]["New"]["Weight"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Heros</h3>
					<div id="Heros">				
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Barbarian King</b></th>
								<th align="center"><b>Archer Queen</b></th>
								<th align="center"><b>Grand Warden</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["Barbarian King"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["Archer Queen"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["Grand Warden"]["Level"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Newest Heros</h3>
					<div id="Newest Heros">				
						<table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Hero</b></th>
								<th align="center"><b>Level</b></th>
								<th align="center"><b>Max Level</b></th>
								<th align="center"><b>War Weight</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["New"]["Name"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["New"]["Level"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["New"]["MaxLevel"]; ?>
										</td>
										<td align="center">
											<?php echo $members_array[$temp_id]["Hero"]["New"]["Weight"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<h3 align="center">Spell Info</h3>
					<div id="Spell Info">	
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
						<!-- <table width="100%" class="sortable">
							<thead>
								<th align="center"><b>Rank/<br>Pevious</b></th>
								<th align="center"><b>League</b></th>
								<th align="center"><b>Name</b></th>
								<th align="center"><b>Role</b></th>
								<th align="center"><b>Trophies</b></th>
								<th align="center"><b>Donations</b></th>
								<th align="center"><b>Donations<br>Received</b></th>
							</thead>
							<tbody>
								<?php foreach ( $members_array as $temp_id => $value ) { ?>
									<tr> 
										<td align="center">
											<?php echo $members_array[$temp_id]["member_clanRank"]; ?>/
											<?php if ( $members_array[$temp_id]["member_clanRank"] > $members_array[$temp_id]["member_previousClanRank"] ) {
												echo "<font color=\"red\">";
											} else {
												echo "<font color=\"green\">";
											}
											echo $members_array[$temp_id]["member_previousClanRank"]; ?>
											</font> 
										</td>
										<td align="center">
											<img src="<?php echo $members_array[$temp_id]["league_iconUrls_tiny"]; ?>" /> 
										</td>
										<td>
											<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $members_array[$temp_id]["get_member_id"] ?>"><?php echo $members_array[$temp_id]["member_name"] ?></a>
										</td>
										<td>
											<?php echo $members_array[$temp_id]["member_role"]; ?>
										</td>
										<td>
											<?php echo $members_array[$temp_id]["member_trophies"]; ?>
										</td>
										<td>
											<?php echo $members_array[$temp_id]["member_donations"]; ?>
										</td>
										<td>
											<?php echo $members_array[$temp_id]["member_donationsReceived"]; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table> -->
					</div>
					<h3 align="center">Achievement Info</h3>
					<div id="Achievement Info">	
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div> 
				</div>
			</center>
		</div>
		<div id="tabs-4">
			<center>
				<div id="accordion_builder">
					<h3 align="center">Builder Base</h3>
					<div id="Builder Base">	
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div> 
				</div> 
			</center>
		</div>
	</div>
<center>
	<b>This data was updated at <?php echo $member_timestamp; ?> GMT.</b>
</center>
</br>  

<h2 style="text-align: center;">Clan Picker</h2>
<form method="get">

	<center>
		<input type="radio" name="clan" value="BLACK LIST">&nbsp; Black List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="RED LIST">&nbsp; Red List &nbsp;&nbsp;</input>
	</center>
	<center><input type="submit" name="submit" value="Submit"/></center>
</form>

<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
<!-- JQuery Script -->
<link rel="stylesheet" href="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.css">
<script src="http://www.the-blacklist.ca/images/jumi_code/jquery.js"></script>
<script src="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#accordion-general,#accordion_war,#accordion_home,#accordion_builder" ).accordion({
			heightStyle: "content"
		});
		$( "#accordion-general,#accordion_war,#accordion_home,#accordion_builder" ).accordion({
			collapsible: true
		});
		$( "#accordion-general" ).accordion({
			active: 2
		});
		$( "#accordion_war" ).accordion({
			active: 4
		});
		$( "#accordion_home" ).accordion({
			active: 7
		});
		$( "#accordion_builder" ).accordion({
			active: 0
		});
		$( "#tabs" ).tabs({
				active: 1
		});
	});
</script>


</body>