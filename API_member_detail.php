<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$member_selected = "#R280P220";
	$clan_array = array();
	$member_array = array();
	$defBldg_array = array();
	$walls_array = array();
	$trap_array = array();
	$misc_array = array();
	$achiev_array = array();
	$troops_array = array();
	$heros_array = array();
	$spells_array = array();
	$war_array = array();
	$attack_array = array();
	$defense_array = array();
	$accordion_home_cnt = 8;
	$accordion_bldg_cnt = 6;
	
	
	// setup and zero war weight
	$warWeight = array();
	$warWeight['defBldg'] = 0;
	$warWeight['walls'] = 0;
	$warWeight['traps'] = 0;
	$warWeight['miscBldg'] = 0;
	$warWeight['defense'] = 0;
	$warWeight['troops_elixir'] = 0;
	$warWeight['troops_de'] = 0;
	$warWeight['troops_bldg'] = 0;
	$warWeight['troops'] = 0;
	$warWeight['heros']['off'] = 0;
	$warWeight['heros']['def'] = 0;
	$warWeight['spells'] = 0;
	$warWeight['offense'] = 0;
	$warWeight['total'] = 0;
	$ww_sql_array = array();
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	// Get member or use default set to BlackList
	if ( isset ( $_GET['member'] ) ) {
		$member_selected = '#' . $_GET['member'];
	}
			
	$member_sql = "SELECT * ";
	$member_sql .= " FROM `API_Member` AS am ";
	$member_sql .= " INNER JOIN `API_League` AS al ";
	$member_sql .= "  ON am.`member_league_id`=al.`league_id` ";
	$member_sql .= " INNER JOIN `API_Clan` AS ac ";
	$member_sql .= "  ON am.`member_clan_tag`=ac.`clan_tag` ";
	$member_sql .= " WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_sql .= " ORDER BY `member_current_timestamp` DESC ";
	$member_sql .= " LIMIT 1;";
	$member_result = $conn->query($member_sql);
	

	while($member_row = $member_result->fetch_assoc()) {
		$member_array["member_tag"] = $member_row["member_tag"];
		$member_array["clan_tag"] = $member_row["clan_tag"];
		$member_array["league_id"] = $member_row["league_id"];
		$member_array["member_name"] = quotemeta( $member_row["member_name"] );
		$member_array["member_timestamp"] = $member_row["member_current_timestamp"];
		$member_array["member_clanRank"] = $member_row["member_clanRank"];
		$member_array["member_previousClanRank"] = $member_row["member_previousClanRank"];
		$member_array["member_expLevel"] = $member_row["member_expLevel"];
		$member_array["member_role"] = $member_row["member_role"];
		$member_array["member_townHallLevel"] = $member_row["member_townHallLevel"];
		$member_array["member_trophies"] = $member_row["member_trophies"];
		$member_array["member_bestTrophies"] = $member_row["member_bestTrophies"];
		$member_array["member_warStars"] = $member_row["member_warStars"];
		$member_array["member_attackWins"] = $member_row["member_attackWins"];
		$member_array["member_defenseWins"] = $member_row["member_defenseWins"];
		$member_array["member_builderHallLevel"] = $member_row["member_builderHallLevel"];
		$member_array["member_versusTrophies"] = $member_row["member_versusTrophies"];
		$member_array["member_bestVersusTrophies"] = $member_row["member_bestVersusTrophies"];
		$member_array["member_versusBattleWinCount"] = $member_row["member_versusBattleWinCount"];
		$member_array["member_versusBattleWins"] = $member_row["member_versusBattleWins"];
		$member_array["member_donations"] = $member_row["member_donations"];
		$member_array["member_total_donations"] = $member_row["member_total_donations"];
		$member_array["member_donationsReceived"] = $member_row["member_donationsReceived"];
		$member_array["member_total_donationsReceived"] = $member_row["member_total_donationsReceived"];
		$member_array["clan_name"] = quotemeta( $member_row["clan_name"] );
		$member_array["clan_description"] = $member_row["clan_description"];
		$member_array["clan_badgeUrls_small"] = $member_row["clan_badgeUrls_small"];
		$member_array["clan_badgeUrls_medium"] = $member_row["clan_badgeUrls_medium"];
		$member_array["clan_badgeUrls_large"] = $member_row["clan_badgeUrls_large"];
		$member_array["league_name"] = quotemeta( $member_row["league_name"] );
		$member_array["league_iconUrls_tiny"] = $member_row["league_iconUrls_tiny"];
		$member_array["league_iconUrls_small"] = $member_row["league_iconUrls_small"];
		$member_array["league_iconUrls_medium"] = $member_row["league_iconUrls_medium"];
		$member_array["get_member_id"] = str_replace ( '#', '', $member_row["member_tag"] );
		$member_array["th_image"] = 'http://www.the-blacklist.ca/images/jumi_code/API/TownHall/Town_Hall';
		$member_array["th_image"] .= $member_array["member_townHallLevel"] . '.png';
	}	

	$member_oldest_sql = "SELECT `member_timestamp` AS oldest ";
	$member_oldest_sql .= "FROM `API_Member` ";
	$member_oldest_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_oldest_sql .= "ORDER BY `member_timestamp` ASC ";
	$member_oldest_sql .= "LIMIT 1;";
	$member_oldest_result = $conn->query($member_oldest_sql);
	
	while($member_oldest_row = $member_oldest_result->fetch_assoc()) {
		$member_array["oldest_timestamp"] = $member_oldest_row["oldest"];
	}
	
	$member_start_sql = "SELECT `member_timestamp` AS start ";
	$member_start_sql .= "FROM `API_Member` ";
	$member_start_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_start_sql .= "ORDER BY `member_timestamp` ASC ";
	$member_start_sql .= "LIMIT 1;";
	$member_start_result = $conn->query($member_start_sql);
	
	while($member_start_row = $member_start_result->fetch_assoc()) {
		$member_array["start"] = $member_start_row["start"];
	}
	
	$member_lowRank_sql = "SELECT `member_clanRank` AS low_rank, `member_current_timestamp` ";
	$member_lowRank_sql .= "FROM `API_Member` ";
	$member_lowRank_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_lowRank_sql .= "ORDER BY `member_clanRank` DESC, `member_current_timestamp` DESC ";
	$member_lowRank_sql .= "LIMIT 1;";
	$member_lowRank_result = $conn->query($member_lowRank_sql);
	
	while($member_lowRank_row = $member_lowRank_result->fetch_assoc()) {
		$member_array["lowRank"] = $member_lowRank_row["low_rank"];
		$member_array["lowRank_timestamp"] = $member_lowRank_row["member_current_timestamp"];
	}

	$member_highRank_sql = "SELECT `member_clanRank` AS high_rank, `member_current_timestamp` ";
	$member_highRank_sql .= "FROM `API_Member` ";
	$member_highRank_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_highRank_sql .= "ORDER BY `member_clanRank` ASC, `member_current_timestamp` DESC ";
	$member_highRank_sql .= "LIMIT 1;";
	$member_highRank_result = $conn->query($member_highRank_sql);
	

	while($member_highRank_row = $member_highRank_result->fetch_assoc()) {
		$member_array["highRank"] = $member_highRank_row["high_rank"];
		$member_array["highRank_timestamp"] = $member_highRank_row["member_current_timestamp"];
	}
	
	$member_maxDonation_sql = "SELECT `member_donations` AS maxDonation, `member_current_timestamp` ";
	$member_maxDonation_sql .= "FROM `API_Member` ";
	$member_maxDonation_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_maxDonation_sql .= "ORDER BY `member_donations` DESC, `member_current_timestamp` DESC ";
	$member_maxDonation_sql .= "LIMIT 1;";
	$member_maxDonation_result = $conn->query($member_maxDonation_sql);
	

	while($member_maxDonation_row = $member_maxDonation_result->fetch_assoc()) {
		$member_array["maxDonation"] = $member_maxDonation_row["maxDonation"];
		$member_array["maxDonation_timestamp"] = $member_maxDonation_row["member_current_timestamp"];
	}

	$member_maxReceived_sql = "SELECT `member_donationsReceived` AS maxReceived, `member_current_timestamp` ";
	$member_maxReceived_sql .= "FROM `API_Member` ";
	$member_maxReceived_sql .= "WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_maxReceived_sql .= "ORDER BY `member_donationsReceived` DESC, `member_current_timestamp` DESC ";
	$member_maxReceived_sql .= "LIMIT 1;";
	$member_maxReceived_result = $conn->query($member_maxReceived_sql);
	

	while($member_maxReceived_row = $member_maxReceived_result->fetch_assoc()) {
		$member_array["maxReceived"] = $member_maxReceived_row["maxReceived"];
		$member_array["maxReceived_timestamp"] = $member_maxReceived_row["member_current_timestamp"];
	}

	$member_troops_sql = "SELECT * ";
	$member_troops_sql .= "FROM (SELECT * ";
	$member_troops_sql .= "  FROM `API_Mem_TroopsToMem` ";
	$member_troops_sql .= "  WHERE `troopsToMem_member_tag` ";
	$member_troops_sql .= "  LIKE '" . $member_selected . "'";
	$member_troops_sql .= "  ORDER BY `troopsToMem_timestamp` DESC) AS atm";
	$member_troops_sql .= "INNER JOIN `API_Mem_Troops`";
	$member_troops_sql .= "  ON `troopsToMem_troop_id` LIKE `troops_id`";
	$member_troops_sql .= "GROUP BY `troopsToMem_troop_id`";
	$member_troops_sql .= "ORDER BY `troops_order`;";
	$member_troops_result = $conn->query($member_troops_sql);
	
	while($member_troops_row = $member_troops_result->fetch_assoc()) {
		$temp_base = $member_troops_row["troops_base"];
		$temp_order = $member_troops_row["troops_order"];
		$troops_array[$temp_base][$temp_order]["name"] = $member_troops_row["troops_name"];
		$troops_array[$temp_base][$temp_order]["id"] = $member_troops_row["troops_id"];
		$troops_array[$temp_base][$temp_order]["timestamp"] = $member_troops_row["troopsToMem_timestamp"];
		$troops_array[$temp_base][$temp_order]["level"] = $member_troops_row["troopsToMem_level"];
		$troops_array[$temp_base][$temp_order]["maxLevel"] = $member_troops_row["troops_maxLevel"];
		$troops_array[$temp_base][$temp_order]["warWeight"] = $member_troops_row["troopsToMem_warWeight"];
		
		# total war weights
		if ( $temp_base == 1 ) {
			if ( $temp_order < 99 ) {
				$warWeight['troops_elixir'] += $troops_array[$temp_base][$temp_order]["warWeight"];
			} else {
				$warWeight['troops_de'] += $troops_array[$temp_base][$temp_order]["warWeight"];
			}
			$warWeight['troops'] += $troops_array[$temp_base][$temp_order]["warWeight"];
		} else {
			$warWeight['troops_bldg'] += $troops_array[$temp_base][$temp_order]["warWeight"];
		}
			
		# get max th for troop
		if ( $temp_base == 1 ) {
			$maxTh_sql = "SELECT tww.`ww_level` ";
			$maxTh_sql .= "	FROM `API_WarWeight` AS tww ";
			$maxTh_sql .= "	INNER JOIN `API_WarWeight` AS lww ";
			$maxTh_sql .= "		ON tww.`ww_labLevel` = lww.`ww_level` ";
			$maxTh_sql .= "			AND lww.`ww_name` LIKE 'Laboratory' ";
			$maxTh_sql .= "	WHERE tww.`ww_troops_id` = " . $troops_array[$temp_base][$temp_order]["id"] . " ";
			$maxTh_sql .= "		AND lww.`ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
			$maxTh_sql .= "	ORDER BY lww.`ww_thLevel` DESC, tww.`ww_level` DESC ";
			$maxTh_sql .= "	LIMIT 1;";
			$maxTh_result = $conn->query($maxTh_sql);
			while($maxTH_row = $maxTh_result->fetch_assoc()) {
				$troops_array[$temp_base][$temp_order]["maxTH"] = $maxTH_row["ww_level"];
			}
		} else {
			$troops_array[$temp_base][$temp_order]["maxTH"] = 'N/A';
		}
	}

	$member_def_sql = "SELECT *, ";
	$member_def_sql .= "   (SELECT `ww_level`  ";
	$member_def_sql .= "    FROM `API_WarWeight` ";
	$member_def_sql .= "    WHERE `ww_build_id` = `def_id` ";
	$member_def_sql .= "      AND `ww_thLevel` = " . $member_array["member_townHallLevel"] . " ";
	$member_def_sql .= "    ORDER BY `ww_level` DESC ";
	$member_def_sql .= "    LIMIT 1) AS maxTH ";
	$member_def_sql .= "FROM (SELECT * ";
	$member_def_sql .= "  FROM `API_Mem_DefToMem` ";
	$member_def_sql .= "  WHERE `defToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_def_sql .= "  ORDER BY `defToMem_timestamp` DESC) AS ahm ";
	$member_def_sql .= "INNER JOIN `API_Mem_Defense` ";
	$member_def_sql .= "  ON `defToMem_defense_id` LIKE `def_id` ";
	$member_def_sql .= "GROUP BY `defToMem_defense_id` ";
	$member_def_sql .= "ORDER BY `def_order` ASC;";
	$member_def_result = $conn->query($member_def_sql);
	
	while($member_def_row = $member_def_result->fetch_assoc()) {
		$temp_base = $member_def_row["def_base"];
		$temp_order = $member_def_row["def_order"];
		$defBldg_array[$temp_base][$temp_order]["name"] = $member_def_row["def_name"];
		$defBldg_array[$temp_base][$temp_order]["id"] = $member_def_row["def_id"];
		$defBldg_array[$temp_base][$temp_order]["timestamp"] = $member_def_row["defToMem_timestamp"];
		$defBldg_array[$temp_base][$temp_order]["pos1"] = $member_def_row["defToMem_pos1"];
		$defBldg_array[$temp_base][$temp_order]["pos2"] = $member_def_row["defToMem_pos2"];
		$defBldg_array[$temp_base][$temp_order]["pos3"] = $member_def_row["defToMem_pos3"];
		$defBldg_array[$temp_base][$temp_order]["pos4"] = $member_def_row["defToMem_pos4"];
		$defBldg_array[$temp_base][$temp_order]["pos5"] = $member_def_row["defToMem_pos5"];
		$defBldg_array[$temp_base][$temp_order]["pos6"] = $member_def_row["defToMem_pos6"];
		$defBldg_array[$temp_base][$temp_order]["pos7"] = $member_def_row["defToMem_pos7"];
		$defBldg_array[$temp_base][$temp_order]["pos8"] = $member_def_row["defToMem_pos8"];
		$defBldg_array[$temp_base][$temp_order]["pos9"] = $member_def_row["defToMem_pos9"];
		$defBldg_array[$temp_base][$temp_order]["pos10"] = $member_def_row["defToMem_pos10"];
		$defBldg_array[$temp_base][$temp_order]["pos11"] = $member_def_row["defToMem_pos11"];
		$defBldg_array[$temp_base][$temp_order]["pos12"] = $member_def_row["defToMem_pos12"];
		$defBldg_array[$temp_base][$temp_order]["cntTH"] = $member_def_row["def_cnt_th" . $member_array["member_townHallLevel"]];
		$defBldg_array[$temp_base][$temp_order]["warWeight"] = $member_def_row["defToMem_warWeight"];
		$defBldg_array[$temp_base][$temp_order]["maxTH"] = $member_def_row["maxTH"];
		$defBldg_array[$temp_base][$temp_order]["maxLevel"] = $member_def_row["def_maxLevel"];
		
		# check if home base
		if ( $temp_base == 1 ) {
			# total war weight
			if ( $temp_order < 99 ) {
				$warWeight['defBldg'] += $defBldg_array[$temp_base][$temp_order]["warWeight"];
			} else {
				$warWeight['walls'] += $defBldg_array[$temp_base][$temp_order]["warWeight"];
				$defBldg_array[$temp_base][$temp_order]["cnt"] = $member_def_row["defToMem_pos1"] + $member_def_row["defToMem_pos2"] +
								$member_def_row["defToMem_pos3"] + $member_def_row["defToMem_pos4"] +
								$member_def_row["defToMem_pos5"] + $member_def_row["defToMem_pos6"] +
								$member_def_row["defToMem_pos7"] + $member_def_row["defToMem_pos8"] +
								$member_def_row["defToMem_pos9"] + $member_def_row["defToMem_pos10"] +
								$member_def_row["defToMem_pos11"] + $member_def_row["defToMem_pos12"];
			}
		} else {
			# Need to add building weight for builders base
		}

	}

	# subtract one from accordion if not defined
	if ( !$defBldg_array[1] ) {
		$accordion_home_cnt -= 1;
	}
	if ( !$defBldg_array[2] ) {
		$accordion_bldg_cnt -= 1;
	}

	$member_trap_sql = "SELECT *, ";
	$member_trap_sql .= "   (SELECT `ww_level`  ";
	$member_trap_sql .= "    FROM `API_WarWeight` ";
	$member_trap_sql .= "    WHERE `ww_trap_id` = `trap_id` ";
	$member_trap_sql .= "      AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_trap_sql .= "    ORDER BY `ww_level` DESC ";
	$member_trap_sql .= "    LIMIT 1) AS maxTH ";
	$member_trap_sql .= "FROM (SELECT * ";
	$member_trap_sql .= "  FROM `API_Mem_TrapToMem` ";
	$member_trap_sql .= "  WHERE `trapToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_trap_sql .= "  ORDER BY `trapToMem_timestamp` DESC) AS ahm ";
	$member_trap_sql .= "INNER JOIN `API_Mem_Traps` ";
	$member_trap_sql .= "  ON `trapToMem_trap_id` LIKE `trap_id` ";
	$member_trap_sql .= "GROUP BY `trapToMem_trap_id` ";
	$member_trap_sql .= "ORDER BY `trap_order` ASC;";
	$member_trap_result = $conn->query($member_trap_sql);
	
	while($member_trap_row = $member_trap_result->fetch_assoc()) {
		$temp_base = $member_trap_row["trap_base"];
		$temp_order = $member_trap_row["trap_order"];
		$trap_array[$temp_base][$temp_order]["name"] = $member_trap_row["trap_name"];
		$trap_array[$temp_base][$temp_order]["id"] = $member_trap_row["trap_id"];
		$trap_array[$temp_base][$temp_order]["timestamp"] = $member_trap_row["defToMem_timestamp"];
		$trap_array[$temp_base][$temp_order]["pos1"] = $member_trap_row["trapToMem_pos1"];
		$trap_array[$temp_base][$temp_order]["pos2"] = $member_trap_row["trapToMem_pos2"];
		$trap_array[$temp_base][$temp_order]["pos3"] = $member_trap_row["trapToMem_pos3"];
		$trap_array[$temp_base][$temp_order]["pos4"] = $member_trap_row["trapToMem_pos4"];
		$trap_array[$temp_base][$temp_order]["pos5"] = $member_trap_row["trapToMem_pos5"];
		$trap_array[$temp_base][$temp_order]["pos6"] = $member_trap_row["trapToMem_pos6"];
		$trap_array[$temp_base][$temp_order]["cntTH"] = $member_trap_row["trap_cnt_th" . $member_array["member_townHallLevel"]];
		$trap_array[$temp_base][$temp_order]["warWeight"] = $member_trap_row["trapToMem_warWeight"];
		$trap_array[$temp_base][$temp_order]["maxTH"] = $member_trap_row["maxTH"];
		$trap_array[$temp_base][$temp_order]["maxLevel"] = $member_trap_row["trap_maxLevel"];

		# check if home base
		if ( $temp_base == 1 ) {
			# total war weight
			$warWeight['traps'] += $trap_array[$temp_base][$temp_order]["warWeight"];
		} else {
			# Need to add building weight for builders base
		}
	}

	# subtract one from accordion if not defined
	if ( !$trap_array[1] ) {
		$accordion_home_cnt -= 1;
	}
	if ( !$trap_array[2] ) {
		$accordion_bldg_cnt -= 1;
	}

	$member_misc_sql = "SELECT *, ";
	$member_misc_sql .= "   (SELECT `ww_level`  ";
	$member_misc_sql .= "    FROM `API_WarWeight` ";
	$member_misc_sql .= "    WHERE `ww_misc_id` = `misc_id` ";
	$member_misc_sql .= "      AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_misc_sql .= "    ORDER BY `ww_level` DESC ";
	$member_misc_sql .= "    LIMIT 1) AS maxTH ";
	$member_misc_sql .= "FROM (SELECT * ";
	$member_misc_sql .= "  FROM `API_Mem_MiscToMem` ";
	$member_misc_sql .= "  WHERE `miscToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_misc_sql .= "  ORDER BY `miscToMem_timestamp` DESC) AS ahm ";
	$member_misc_sql .= "INNER JOIN `API_Mem_Misc` ";
	$member_misc_sql .= "  ON `miscToMem_misc_id` LIKE `misc_id` ";
	$member_misc_sql .= "GROUP BY `miscToMem_misc_id` ";
	$member_misc_sql .= "ORDER BY `misc_order` ASC;";
	$member_misc_result = $conn->query($member_misc_sql);
	
	while($member_misc_row = $member_misc_result->fetch_assoc()) {
		$temp_base = $member_misc_row["misc_base"];
		$temp_order = $member_misc_row["misc_order"];
		$misc_array[$temp_base][$temp_order]["name"] = $member_misc_row["misc_name"];
		$misc_array[$temp_base][$temp_order]["id"] = $member_misc_row["misc_id"];
		$misc_array[$temp_base][$temp_order]["timestamp"] = $member_misc_row["miscToMem_timestamp"];
		$misc_array[$temp_base][$temp_order]["pos1"] = $member_misc_row["miscToMem_pos1"];
		$misc_array[$temp_base][$temp_order]["pos2"] = $member_misc_row["miscToMem_pos2"];
		$misc_array[$temp_base][$temp_order]["pos3"] = $member_misc_row["miscToMem_pos3"];
		$misc_array[$temp_base][$temp_order]["pos4"] = $member_misc_row["miscToMem_pos4"];
		$misc_array[$temp_base][$temp_order]["pos5"] = $member_misc_row["miscToMem_pos5"];
		$misc_array[$temp_base][$temp_order]["pos6"] = $member_misc_row["miscToMem_pos6"];
		$misc_array[$temp_base][$temp_order]["cntTH"] = $member_misc_row["misc_cnt_th" . $member_array["member_townHallLevel"]];
		$misc_array[$temp_base][$temp_order]["warWeight"] = $member_misc_row["miscToMem_warWeight"];
		$misc_array[$temp_base][$temp_order]["maxTH"] = $member_misc_row["maxTH"];
		$misc_array[$temp_base][$temp_order]["maxLevel"] = $member_misc_row["misc_maxLevel"];

		# check if home base
		if ( $temp_base == 1 ) {
			# total war weight
			$warWeight['miscBldg'] += $misc_array[$temp_base][$temp_order]["warWeight"];
		} else {
			# Need to add building weight for builders base
		}
	}

	# subtract one from accordion if not defined
	if ( !$misc_array[1] ) {
		$accordion_home_cnt -= 1;
	}
	if ( !$misc_array[2] ) {
		$accordion_bldg_cnt -= 1;
	}

	$member_heros_sql = "SELECT * ";
	$member_heros_sql .= "FROM (SELECT * ";
	$member_heros_sql .= "  FROM `API_Mem_HerosToMem` ";
	$member_heros_sql .= "  WHERE `heroToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_heros_sql .= "  ORDER BY `heroToMem_timestamp` DESC) AS ahm ";
	$member_heros_sql .= "INNER JOIN `API_Mem_Heros` ";
	$member_heros_sql .= "  ON `heroToMem_heros_id` LIKE `hero_id` ";
	$member_heros_sql .= "GROUP BY `heroToMem_heros_id` ";
	$member_heros_sql .= "ORDER BY `hero_order`;";
	$member_heros_result = $conn->query($member_heros_sql);
	
	while($member_heros_row = $member_heros_result->fetch_assoc()) {
		$temp_base = $member_heros_row["hero_base"];
		$temp_order = $member_heros_row["hero_order"];
		$heros_array[$temp_base][$temp_order]["name"] = $member_heros_row["hero_name"];
		$heros_array[$temp_base][$temp_order]["id"] = $member_heros_row["hero_id"];
		$heros_array[$temp_base][$temp_order]["timestamp"] = $member_heros_row["heroToMem_timestamp"];
		$heros_array[$temp_base][$temp_order]["level"] = $member_heros_row["heroToMem_level"];
		$heros_array[$temp_base][$temp_order]["maxLevel"] = $member_heros_row["hero_maxLevel"];
		$heros_array[$temp_base][$temp_order]["warWeight"] = $member_heros_row["heroToMem_warWeight"];
		$heros_array[$temp_base][$temp_order]["warWeight_off"] = $member_heros_row["heroToMem_warWeight_off"];
		$heros_array[$temp_base][$temp_order]["warWeight_def"] = $member_heros_row["heroToMem_warWeight_def"];

		# check if home base
		if ( $temp_base == 1 ) {
			# get max hero for th level
			$maxTh_sql = "SELECT `ww_level` ";
			$maxTh_sql .= "	FROM `API_WarWeight` ";
			$maxTh_sql .= "	WHERE `ww_heros_id` = " . $heros_array[$temp_base][$temp_order]["id"] . " ";
			$maxTh_sql .= "		AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
			$maxTh_sql .= "	ORDER BY `ww_thLevel` DESC, `ww_level` DESC ";
			$maxTh_sql .= "	LIMIT 1;";
			$maxTh_result = $conn->query($maxTh_sql);
			while($maxTH_row = $maxTh_result->fetch_assoc()) {
				$heros_array[$temp_base][$temp_order]["maxTH"] = $maxTH_row["ww_level"];
			}

			# total war weight
			$warWeight['heros']['off'] += $heros_array[$temp_base][$temp_order]["warWeight_off"];
			$warWeight['heros']['def'] += $heros_array[$temp_base][$temp_order]["warWeight_def"];
			} else {
			# Need to add hero weight and max for th for builder base
		}
	}
	
	# subtract one from accordion cnt if not defined
	if ( !$heros_array[1] ) {
		$accordion_home_cnt -= 1;
	}
	if ( !$heros_array[2] ) {
		$accordion_bldg_cnt -= 1;
	}

	$member_spells_sql = "SELECT *, ";
	$member_spells_sql .= "   (SELECT spell.`ww_level`  ";
	$member_spells_sql .= "    FROM `API_WarWeight` AS spell ";
	$member_spells_sql .= "     INNER JOIN `API_WarWeight` AS lab ";
 	$member_spells_sql .= "      ON spell.ww_labLevel = lab.ww_level ";
	$member_spells_sql .= "    WHERE spell.`ww_spells_id` = `spells_id` ";
	$member_spells_sql .= "      AND lab.`ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
	$member_spells_sql .= "      AND lab.ww_name LIKE 'Laboratory' ";
	$member_spells_sql .= "    ORDER BY spell.`ww_level` DESC ";
	$member_spells_sql .= "    LIMIT 1) AS maxTH ";
	$member_spells_sql .= "FROM (SELECT * ";
	$member_spells_sql .= "  FROM `API_Mem_SpellsToMem` ";
	$member_spells_sql .= "  WHERE `spellsToMem_member_tag` LIKE '" . $member_selected . "' ";
	$member_spells_sql .= "  ORDER BY `spellsToMem_timestamp` DESC) AS asm ";
	$member_spells_sql .= "INNER JOIN `API_Mem_Spells` ";
	$member_spells_sql .= "  ON `spellsToMem_spells_name` LIKE `spells_name` ";
	$member_spells_sql .= "GROUP BY `spellsToMem_spells_id` ";
	$member_spells_sql .= "ORDER BY `spells_order`;";
	$member_spells_result = $conn->query($member_spells_sql);
	
	while($member_spells_row = $member_spells_result->fetch_assoc()) {
		$temp_base = $member_spells_row["spells_base"];
		$temp_order = $member_spells_row["spells_order"];
		$spells_array[$temp_base][$temp_order]["name"] = $member_spells_row["spells_name"];
		$spells_array[$temp_base][$temp_order]["id"] = $member_spells_row["spells_id"];
		$spells_array[$temp_base][$temp_order]["timestamp"] = $member_spells_row["spellsToMem_timestamp"];
		$spells_array[$temp_base][$temp_order]["level"] = $member_spells_row["spellsToMem_level"];
		$spells_array[$temp_base][$temp_order]["maxTH"] = $member_spells_row["maxTH"];
		$spells_array[$temp_base][$temp_order]["maxLevel"] = $member_spells_row["spells_maxLevel"];
		$spells_array[$temp_base][$temp_order]["warWeight"] = $member_spells_row["spellsToMem_warWeight"];

		# check if home base
		if ( $temp_base == 1 ) {
			# total war weight
			if ( $temp_order < 99 ) {
				$warWeight['spells_elixir'] += $spells_array[$temp_base][$temp_order]["warWeight"];
			} else {
				$warWeight['spells_de'] += $spells_array[$temp_base][$temp_order]["warWeight"];
			}
			$warWeight['spells'] += $spells_array[$temp_base][$temp_order]["warWeight"];
		} else {
			# Need to add spell weight for builders base
		}
	}
	
	# subtract one from accordion if not defined
	if ( !$spells_array[1] ) {
		$accordion_home_cnt -= 1;
	}
	if ( !$spells_array[2] ) {
		$accordion_bldg_cnt -= 1;
	}

	$member_achiev_sql = "SELECT * ";
	$member_achiev_sql .= "FROM (SELECT * ";
	$member_achiev_sql .= "  FROM `API_Mem_AchievToMem` ";
	$member_achiev_sql .= "  WHERE `achievToMem_member_tag` ";
	$member_achiev_sql .= "  LIKE '" . $member_selected . "' ";
	$member_achiev_sql .= "  ORDER BY `achievToMem_timestamp` DESC) AS aam ";
	$member_achiev_sql .= "INNER JOIN `API_Mem_Achiev` ";
	$member_achiev_sql .= "  ON `achievToMem_achiev_id` LIKE `achiev_id` ";
	$member_achiev_sql .= "GROUP BY `achievToMem_achiev_id` ";
	$member_achiev_sql .= "ORDER BY `achiev_order`;";
	$member_achiev_result = $conn->query($member_achiev_sql);
	
	while($member_achiev_row = $member_achiev_result->fetch_assoc()) {
		$temp_base = $member_achiev_row["achiev_base"];
		$temp_order = $member_achiev_row["achiev_order"];
		$achiev_array[$temp_base][$temp_order]["name"] = $member_achiev_row["achiev_name"];
		$achiev_array[$temp_base][$temp_order]["id"] = $member_achiev_row["achiev_id"];
		$achiev_array[$temp_base][$temp_order]["info"] = $member_achiev_row["achiev_info"];
		$achiev_array[$temp_base][$temp_order]["timestamp"] = $member_achiev_row["achievToMem_timestamp"];
		$achiev_array[$temp_base][$temp_order]["stars"] = $member_achiev_row["achievToMem_stars"];
		$achiev_array[$temp_base][$temp_order]["value"] = $member_achiev_row["achievToMem_value"];
		$achiev_array[$temp_base][$temp_order]["target"] = $member_achiev_row["achievToMem_target"];
		$achiev_array[$temp_base][$temp_order]["completionInfo"] = $member_achiev_row["achievToMem_completionInfo"];
		
		// Correct 'Keep your village safe' achiev
		if (( $achiev_array[$temp_base][$temp_order]["id"] == 25 )&&
					( $achiev_array[$temp_base][$temp_order]["stars"] == 1 )) {
			$achiev_array[$temp_base][$temp_order]["stars"] = 3;
		}

		// Setup star image
		$achiev_array[$temp_base][$temp_order]['star_image'] = 'http://www.the-blacklist.ca/images/jumi_code/API/Stars/';
		$achiev_array[$temp_base][$temp_order]['star_image'] .= $achiev_array[$temp_base][$temp_order]['stars'] . 'Star.png';
	}
	
	$warWeight['defense'] = $warWeight['defBldg'] + $warWeight['walls'] + $warWeight['traps'] + $warWeight['miscBldg'] + $warWeight['heros']['def'];
	$warWeight['offense'] = $warWeight['troops'] + $warWeight['heros']['off'] + $warWeight['spells'];
	$warWeight['total'] = $warWeight['defense'] + $warWeight['offense'];


	$member_ww_sql = "SELECT *  ";
	$member_ww_sql .= "FROM `API_Mem_WarWeight` ";
	$member_ww_sql .= "WHERE `memWW_member_tag` LIKE '" . $member_selected . "' ";
	$member_ww_sql .= "ORDER BY `memWW_timestamp` DESC ";
	$member_ww_sql .= "LIMIT 1;";
	$member_ww_result = $conn->query($member_ww_sql);

	while($member_ww_row = $member_ww_result->fetch_assoc()) {
		$ww_sql_array["defBldg"] = $member_ww_row["memWW_defBldg"];
		$ww_sql_array["traps"] = $member_ww_row["memWW_trap"];
		$ww_sql_array["miscBldg"] = $member_ww_row["memWW_miscBldg"];
		$ww_sql_array["heros"] = $member_ww_row["memWW_hero"];
		$ww_sql_array["troops"] = $member_ww_row["memWW_troop"];
		$ww_sql_array["spells"] = $member_ww_row["memWW_spell"];
		$ww_sql_array["total"] = $member_ww_row["memWW_total"];
		$warWeight['rounded'] = $member_ww_row["memWW_rounded"];
		$warWeight['step1'] = $member_ww_row["memWW_Step1"];
		$warWeight['step2'] = $member_ww_row["memWW_Step2"];
		$warWeight['step3_offRatio'] = $member_ww_row["memWW_Step3_OffRatio"];
		$warWeight['step3_defRatio'] = $member_ww_row["memWW_Step3_DefRatio"];
		$warWeight['step3_diffRatio'] = $member_ww_row["memWW_Step3_DiffRatio"];
		$warWeight['step3'] = $member_ww_row["memWW_Step3"];
		$warWeight['step4_diff'] = $member_ww_row["memWW_Step4_Diff"];
		$warWeight['step4'] = $member_ww_row["memWW_Step4"];
		if ( $warWeight['step4'] == NULL ) {
			$warWeight['step4_text'] = 'N/A';
		} elseif ( $warWeight['step4'] == 0 ) {
			$warWeight['step4_text'] = '<font color="green">No</font>';
		} elseif ( $warWeight['step4'] == 1 ) {
			$warWeight['step4_text'] = '<font color="yellow">Low</font>';
		} elseif ( $warWeight['step4'] == 2 ) {
			$warWeight['step4_text'] = '<font color="orange">Medium</font>';
		} elseif ( $warWeight['step4'] == 3 ) {
			$warWeight['step4_text'] = '<font color="red">High</font>';
		}
		








	}

	if ( $ww_sql_array["defBldg"] === $warWeight['defBldg'] ) {
		echo "Warning: Defensive Building War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['defBldg'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['defBldg'] . "<br>";
	}
	if ( $ww_sql_array["traps"] === $warWeight['traps'] ) {
		echo "Warning: Traps War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['traps'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['traps'] . "<br>";
	}
	if ( $ww_sql_array["miscBldg"] === $warWeight['miscBldg'] ) {
		echo "Warning: Misc. Building War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['miscBldg'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['miscBldg'] . "<br>";
	}
	if ( $ww_sql_array["troops"] === $warWeight['troops'] ) {
		echo "Warning: Troops War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['troops'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['troops'] . "<br>";
	}
	if ( $ww_sql_array["spells"] === $warWeight['spells'] ) {
		echo "Warning: Spells War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['spells'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['spells'] . "<br>";
	}
	if ( $ww_sql_array["total"] === $warWeight['total'] ) {
		echo "Warning: Total War Weight not equal.<br>";
		echo "&emsp;CAL: " . $warWeight['total'] . "<br>";
		echo "&emsp;SQL: " . $ww_sql_array['total'] . "<br>";
	}

	$member_wwth_sql = "SELECT * ";
	$member_wwth_sql .= "FROM `API_WarWeight_TH`;";
	$member_wwth_result = $conn->query($member_wwth_sql);
	
	while($member_wwth_row = $member_wwth_result->fetch_assoc()) {
		$temp_th = $member_wwth_row["TH"];
		$ww_sql_array["min"][$temp_th] = $member_wwth_row["WarWeight_Min"];
		$ww_sql_array["med"][$temp_th] = $member_wwth_row["WarWeight_Median"];
		$ww_sql_array["max"][$temp_th] = $member_wwth_row["WarWeight_Max"];
	}





	$sql = "SELECT * ";
	$sql .= "FROM  `API_Mem_Wars` ";
	$sql .= "WHERE  `member_tag` LIKE '" . $member_selected . "';";
	$result = $conn->query($sql);
	
	while($row = $result->fetch_assoc()) {
		$war_array['war'] = $row['member_war'];
		$war_array['attack_used'] = $row['member_attack_used'];
		$war_array['attack_stars'] = $row['member_attack_stars'];
		$war_array['attack_3Stars'] = $row['member_attack_3Stars'];
		$war_array['attack_2Stars'] = $row['member_attack_2Stars'];
		$war_array['attack_1Stars'] = $row['member_attack_1Stars'];
		$war_array['attack_0Stars'] = $row['member_attack_0Stars'];
		$war_array['attack_newStars'] = $row['member_attack_newStars'];
		$war_array['attack_3NewStars'] = $row['member_attack_3NewStars'];
		$war_array['attack_2NewStars'] = $row['member_attack_2NewStars'];
		$war_array['attack_1NewStars'] = $row['member_attack_1NewStars'];
		$war_array['attack_0NewStars'] = $row['member_attack_0NewStars'];
		$war_array['attack_destruction'] = $row['member_attack_destruction'];
		$war_array['defense_used'] = $row['member_defense_used'];
		$war_array['defense_stars'] = $row['member_defense_stars'];
		$war_array['defense_3Stars'] = $row['member_defense_3Stars'];
		$war_array['defense_2Stars'] = $row['member_defense_2Stars'];
		$war_array['defense_1Stars'] = $row['member_defense_1Stars'];
		$war_array['defense_0Stars'] = $row['member_defense_0Stars'];
		$war_array['defense_newStars'] = $row['member_defense_newStars'];
		$war_array['defense_3NewStars'] = $row['member_defense_3NewStars'];
		$war_array['defense_2NewStars'] = $row['member_defense_2NewStars'];
		$war_array['defense_1NewStars'] = $row['member_defense_1NewStars'];
		$war_array['defense_0NewStars'] = $row['member_defense_0NewStars'];
		$war_array['defense_destruction'] = $row['member_defense_destruction'];
		$war_array['clean_cnt'] = $row['member_clean_cnt'];
		$war_array['loot_cnt'] = $row['member_loot_cnt'];
		$war_array['earlyLoot_cnt'] = $row['member_earlyLoot_cnt'];
		$war_array['noCC_cnt'] = $row['member_noCC_cnt'];
	}
	if ( $war_array['attack_used'] != 0 ) {
		$war_array['attack_avgStars'] = number_format( $war_array['attack_stars'] / $war_array['attack_used'], 2 );
		$war_array['attack_avgNewStars'] = number_format( $war_array['attack_newStars'] / $war_array['attack_used'], 2 );
		$war_array['attack_avgDest'] = number_format( $war_array['attack_destruction'] / $war_array['attack_used'], 0 );
	} else {
		$war_array['attack_avgStars'] = 0;
		$war_array['attack_avgNewStars'] = 0;
	}
	if ( $war_array['defense_used'] != 0 ) {
		$war_array['defense_avgStars'] = number_format( $war_array['defense_stars'] / $war_array['defense_used'], 2 );
		$war_array['defense_avgNewStars'] = number_format( $war_array['defense_newStars'] / $war_array['defense_used'], 2 );
		$war_array['defense_avgDest'] = number_format( $war_array['defense_destruction'] / $war_array['defense_used'], 0 );
	} else {
		$war_array['defense_avgStars'] = 0;
		$war_array['defense_avgNewStars'] = 0;
	}
	
	$sql = "SELECT att.`member_warlog_id`, bat.`battle_order`, bat.`battle_stars`, bat.`battle_newStars`, ";
	$sql .= "   bat.`battle_destructionPercentage`, bat.`battle_clean_flag`, bat.`battle_loot_flag`, ";
	$sql .= "   bat.`battle_earlyLoot_flag`, bat.`battle_noCC_flag`, war.`warlog_result`, war.`warlog_clan_stars`, ";
	$sql .= "   war.`warlog_clan_destructionPercentage`, war.`warlog_endTime`, war.`warlog_opp_name`, ";
	$sql .= "   war.`warlog_opp_clanLevel`, war.`warlog_opp_stars`, war.`warlog_opp_destructionPercentage`, ";
	$sql .= "   att.`member_mapPosition` AS att_mapPosition, att.`member_townhallLevel` AS att_th, ";
	$sql .= "   def.`member_name` AS def_name, def.`member_townhallLevel` AS def_th, ";
	$sql .= "   def.`member_mapPosition` AS def_mapPosition ";
	$sql .= " FROM `API_WarLog_Member` AS att ";
	$sql .= " LEFT JOIN `API_WarLog_Battle` AS bat ";
	$sql .= "   ON att.`member_tag` LIKE bat.`battle_attackerTag` ";
	$sql .= "    AND att.`member_warlog_id` LIKE bat.`battle_warlog_id` ";
	$sql .= " LEFT JOIN `API_WarLog` AS war ";
	$sql .= "   ON att.`member_warlog_id` LIKE war.`warlog_id` ";
	$sql .= " LEFT JOIN `API_WarLog_Member` AS def ";
	$sql .= "   ON def.`member_tag` LIKE bat.`battle_defenderTag` ";
	$sql .= "     AND att.`member_warlog_id` LIKE def.`member_warlog_id` ";
	$sql .= " WHERE att.`member_tag` LIKE '" . $member_selected . "' ";
	$sql .= " ORDER BY att.`member_warlog_id` DESC, bat.`battle_order` ASC;";
	$result = $conn->query($sql);
	
	while($row = $result->fetch_assoc()) {
		$temp_warID = $row['member_warlog_id'];
		$temp_battle_order = $row['battle_order'];
		if ( isset ( $attack_array[$temp_warID] )) {
			$att_no = 'att_2';
		} else {
			$att_no = 'att_1';
		}
		$attack_array[$temp_warID]['att_th'] = $row['att_th'];
		$attack_array[$temp_warID]['att_mapPosition'] = $row['att_mapPosition'];
		$attack_array[$temp_warID]['warlog_result'] = $row['warlog_result'];
		$attack_array[$temp_warID]['warlog_clan_stars'] = $row['warlog_clan_stars'];
		$attack_array[$temp_warID]['warlog_clan_destructionPercentage'] = number_format( $row['warlog_clan_destructionPercentage'], 0 );
		$attack_array[$temp_warID]['warlog_endTime'] = $row['warlog_endTime'];
		$attack_array[$temp_warID]['warlog_opp_name'] = quotemeta( $row['warlog_opp_name'] );
		$attack_array[$temp_warID]['warlog_opp_clanLevel'] = $row['warlog_opp_clanLevel'];
		$attack_array[$temp_warID]['warlog_opp_stars'] = $row['warlog_opp_stars'];
		$attack_array[$temp_warID]['warlog_opp_destructionPercentage'] = number_format( $row['warlog_opp_destructionPercentage'], 0 );
		
		if ( $temp_battle_order != 0 ) {
			$attack_array[$temp_warID][$att_no]['battle_order'] = $temp_battle_order;
			$attack_array[$temp_warID][$att_no]['battle_stars'] = $row['battle_stars'];
			$attack_array[$temp_warID][$att_no]['battle_newStars'] = $row['battle_newStars'];
			$attack_array[$temp_warID][$att_no]['battle_destructionPercentage'] = $row['battle_destructionPercentage'];
			$attack_array[$temp_warID][$att_no]['battle_clean_flag'] = $row['battle_clean_flag'];
			$attack_array[$temp_warID][$att_no]['battle_loot_flag'] = $row['battle_loot_flag'];
			$attack_array[$temp_warID][$att_no]['battle_earlyLoot_flag'] = $row['battle_earlyLoot_flag'];
			$attack_array[$temp_warID][$att_no]['battle_noCC_flag'] = $row['battle_noCC_flag'];
			$attack_array[$temp_warID][$att_no]['warlog_result'] = $row['warlog_result'];
			$attack_array[$temp_warID][$att_no]['def_name'] = quotemeta( $row['def_name'] );
			$attack_array[$temp_warID][$att_no]['def_th'] = $row['def_th'];
			$attack_array[$temp_warID][$att_no]['def_mapPosition'] = $row['def_mapPosition'];
		
			// Setup star image
			$attack_array[$temp_warID][$att_no]['star_image'] = 'http://www.the-blacklist.ca/images/jumi_code/API/Stars/';
			$attack_array[$temp_warID][$att_no]['star_image'] .= $row['battle_stars'] . 'Star';
			$attack_array[$temp_warID][$att_no]['star_image'] .= $row['battle_newStars'] . 'New.png';
		
			// setup battle flags
			$attack_array[$temp_warID][$att_no]['battle_flag'] = '&emsp;';
			if ( $attack_array[$temp_warID][$att_no]['battle_earlyLoot_flag'] ) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= 'Early Loot';
			} elseif ( $attack_array[$temp_warID][$att_no]['battle_loot_flag'] ) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= 'Loot';
			}
		
			if ((( $attack_array[$temp_warID][$att_no]['battle_earlyLoot_flag'] == 1 )&&( $attack_array[$temp_warID][$att_no]['battle_clean_flag'] == 1 ))||
					(( $attack_array[$temp_warID][$att_no]['battle_loot_flag'] == 1 )&&( $attack_array[$temp_warID][$att_no]['battle_clean_flag'] == 1 ))) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= ', Clean Up';
			} elseif ( $attack_array[$temp_warID][$att_no]['battle_clean_flag'] == 1 ) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= 'Clean Up';
			}
		
			if ((( $attack_array[$temp_warID][$att_no]['battle_earlyLoot_flag'] == 1 )&&( $attack_array[$temp_warID][$att_no]['battle_noCC_flag'] == 1 ))||
					(( $attack_array[$temp_warID][$att_no]['battle_loot_flag'] == 1 )&&( $attack_array[$temp_warID][$att_no]['battle_noCC_flag'] == 1 ))) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= ', No CC';
			} elseif ( $attack_array[$temp_warID][$att_no]['battle_noCC_flag'] == 1 ) {
				$attack_array[$temp_warID][$att_no]['battle_flag'] .= 'No CC';
			}
			
			// count up town hall hits for current th level
			if (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 11 )) {
				$war_array['att_th11']++;
				$war_array['att_th11_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th11_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th11_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 10 )) {
				$war_array['att_th10']++;
				$war_array['att_th10_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th10_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th10_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 9 )) {
				$war_array['att_th9']++;
				$war_array['att_th9_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th9_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th9_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 8 )) {
				$war_array['att_th8']++;
				$war_array['att_th8_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th8_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th8_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 7 )) {
				$war_array['att_th7']++;
				$war_array['att_th7_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th7_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th7_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 6 )) {
				$war_array['att_th6']++;
				$war_array['att_th6_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th6_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th6_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 5 )) {
				$war_array['att_th5']++;
				$war_array['att_th5_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th5_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th5_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			} elseif (( $attack_array[$temp_warID]['att_th'] == $member_array["member_townHallLevel"] )&&
						( $attack_array[$temp_warID][$att_no]['def_th'] == 4 )) {
				$war_array['att_th4']++;
				$war_array['att_th4_totalStars'] += $attack_array[$temp_warID][$att_no]['battle_stars'];
				$war_array['att_th4_totalNewStars'] += $attack_array[$temp_warID][$att_no]['battle_newStars'];
				$war_array['att_th4_totalDest'] += $attack_array[$temp_warID][$att_no]['battle_destructionPercentage'];
			}

			// add up all th hits
			$war_array['att_th'][$attack_array[$temp_warID]['att_th']][$attack_array[$temp_warID][$att_no]['def_th']]['att'] += 
					1;
			$war_array['att_th'][$attack_array[$temp_warID]['att_th']][$attack_array[$temp_warID][$att_no]['def_th']]['stars'] += 
					$attack_array[$temp_warID][$att_no]['battle_stars'];
		}
	}

	// compute average stars for all th level
	for ( $i = 4; $i <= 11; $i++ ) {
		for ( $j = 4; $j <= 11; $j++ ) {
			if ( $war_array['att_th'][$i][$j]['att'] > 0 ) {
				$war_array['att_th'][$i][$j]['avg'] = round (( $war_array['att_th'][$i][$j]['stars'] / $war_array['att_th'][$i][$j]['att'] ), 1 );
			}
		}
	}
	
	// compute town hall average stars
	if ( $war_array['att_th11'] != 0 ) {
		$war_array['att_th11_avgStars'] = number_format( $war_array['att_th11_totalStars'] / $war_array['att_th11'], 2 );
		$war_array['att_th11_avgNewStars'] = number_format( $war_array['att_th11_totalNewStars'] / $war_array['att_th11'], 2 );
		$war_array['att_th11_avgDest'] = number_format( $war_array['att_th11_totalDest'] / $war_array['att_th11'], 0 ) . '%';
	}
	if ( $war_array['att_th10'] != 0 ) {
		$war_array['att_th10_avgStars'] = number_format( $war_array['att_th10_totalStars'] / $war_array['att_th10'], 2 );
		$war_array['att_th10_avgNewStars'] = number_format( $war_array['att_th10_totalNewStars'] / $war_array['att_th10'], 2 );
		$war_array['att_th10_avgDest'] = number_format( $war_array['att_th10_totalDest'] / $war_array['att_th10'], 0 ) . '%';
	}
	if ( $war_array['att_th9'] != 0 ) {
		$war_array['att_th9_avgStars'] = number_format( $war_array['att_th9_totalStars'] / $war_array['att_th9'], 2 );
		$war_array['att_th9_avgNewStars'] = number_format( $war_array['att_th9_totalNewStars'] / $war_array['att_th9'], 2 );
		$war_array['att_th9_avgDest'] = number_format( $war_array['att_th9_totalDest'] / $war_array['att_th9'], 0 ) . '%';
	}
	if ( $war_array['att_th8'] != 0 ) {
		$war_array['att_th8_avgStars'] = number_format( $war_array['att_th8_totalStars'] / $war_array['att_th8'], 2 );
		$war_array['att_th8_avgNewStars'] = number_format( $war_array['att_th8_totalNewStars'] / $war_array['att_th8'], 2 );
		$war_array['att_th8_avgDest'] = number_format( $war_array['att_th8_totalDest'] / $war_array['att_th8'], 0 ) . '%';
	}
	if ( $war_array['att_th7'] != 0 ) {
		$war_array['att_th7_avgStars'] = number_format( $war_array['att_th7_totalStars'] / $war_array['att_th7'], 2 );
		$war_array['att_th7_avgNewStars'] = number_format( $war_array['att_th7_totalNewStars'] / $war_array['att_th7'], 2 );
		$war_array['att_th7_avgDest'] = number_format( $war_array['att_th7_totalDest'] / $war_array['att_th7'], 0 ) . '%';
	}
	if ( $war_array['att_th6'] != 0 ) {
		$war_array['att_th6_avgStars'] = number_format( $war_array['att_th6_totalStars'] / $war_array['att_th6'], 2 );
		$war_array['att_th6_avgNewStars'] = number_format( $war_array['att_th6_totalNewStars'] / $war_array['att_th6'], 2 );
		$war_array['att_th6_avgDest'] = number_format( $war_array['att_th6_totalDest'] / $war_array['att_th6'], 0 ) . '%';
	} 
	if ( $war_array['att_th5'] != 0 ) {
		$war_array['att_th5_avgStars'] = number_format( $war_array['att_th5_totalStars'] / $war_array['att_th5'], 2 );
		$war_array['att_th5_avgNewStars'] = number_format( $war_array['att_th5_totalNewStars'] / $war_array['att_th5'], 2 );
		$war_array['att_th5_avgDest'] = number_format( $war_array['att_th5_totalDest'] / $war_array['att_th5'], 0 ) . '%';
	}
	if ( $war_array['att_th4'] != 0 ) {
		$war_array['att_th4_avgStars'] = number_format( $war_array['att_th4_totalStars'] / $war_array['att_th4'], 2 );
		$war_array['att_th4_avgNewStars'] = number_format( $war_array['att_th4_totalNewStars'] / $war_array['att_th4'], 2 );
		$war_array['att_th4_avgDest'] = number_format( $war_array['att_th4_totalDest'] / $war_array['att_th4'], 0 ) . '%';
	}
		
	$sql = "SELECT def.`member_warlog_id`, bat.`battle_order`, bat.`battle_stars`, bat.`battle_newStars`, ";
	$sql .= "   bat.`battle_destructionPercentage`, war.`warlog_result`,  war.`warlog_clan_stars`, ";
	$sql .= "   war.`warlog_clan_destructionPercentage`, war.`warlog_endTime`, war.`warlog_opp_name`, ";
	$sql .= "   war.`warlog_opp_clanLevel`, war.`warlog_opp_stars`, war.`warlog_opp_destructionPercentage`, ";
	$sql .= "   att.`member_mapPosition` AS att_mapPosition, att.`member_townhallLevel` AS att_th, ";
	$sql .= "   att.`member_name` AS att_name, def.`member_townhallLevel` AS def_th, ";
	$sql .= "   def.`member_mapPosition` AS def_mapPosition ";
	$sql .= " FROM `API_WarLog_Member` AS def ";
	$sql .= " LEFT JOIN `API_WarLog_Battle` AS bat ";
	$sql .= "   ON def.`member_tag` LIKE bat.`battle_defenderTag` ";
	$sql .= "    AND def.`member_warlog_id` LIKE bat.`battle_warlog_id` ";
	$sql .= " LEFT JOIN `API_WarLog` AS war ";
	$sql .= "   ON def.`member_warlog_id` LIKE war.`warlog_id` ";
	$sql .= " LEFT JOIN `API_WarLog_Member` AS att ";
	$sql .= "   ON bat.`battle_attackerTag` LIKE att.`member_tag` ";
	$sql .= "     AND def.`member_warlog_id` LIKE att.`member_warlog_id` ";
	$sql .= " WHERE def.`member_tag` LIKE '" . $member_selected . "' ";
	$sql .= " ORDER BY def.`member_warlog_id` DESC, bat.`battle_order` ASC;";
	$result = $conn->query($sql);
	
	while($row = $result->fetch_assoc()) {
		$temp_warID = $row['member_warlog_id'];
		$temp_battle_order = $row['battle_order'];
		if ( $defense_array[$temp_warID]['def'][1] != 0 ) {
			$def_no++;
		} else {
			$def_no = 1;
			$defense_array[$temp_warID]['total_stars'] = 0;
			$defense_array[$temp_warID]['total_newStars'] = 0;
			$defense_array[$temp_warID]['total_destruction'] = 0;
		}
		$defense_array[$temp_warID]['def_th'] = $row['def_th'];
		$defense_array[$temp_warID]['def_mapPosition'] = $row['def_mapPosition'];
		$defense_array[$temp_warID]['warlog_result'] = $row['warlog_result'];
		$defense_array[$temp_warID]['warlog_clan_stars'] = $row['warlog_clan_stars'];
		$defense_array[$temp_warID]['warlog_clan_destructionPercentage'] = number_format( $row['warlog_clan_destructionPercentage'], 0 );
		$defense_array[$temp_warID]['warlog_endTime'] = $row['warlog_endTime'];
		$defense_array[$temp_warID]['warlog_opp_name'] = quotemeta( $row['warlog_opp_name'] );
		$defense_array[$temp_warID]['warlog_opp_clanLevel'] = $row['warlog_opp_clanLevel'];
		$defense_array[$temp_warID]['warlog_opp_stars'] = $row['warlog_opp_stars'];
		$defense_array[$temp_warID]['warlog_opp_destructionPercentage'] = number_format( $row['warlog_opp_destructionPercentage'], 0 );
		
		if ( $temp_battle_order != 0 ) {
			$defense_array[$temp_warID]['def'][$def_no]['battle_order'] = $temp_battle_order;
			$defense_array[$temp_warID]['def'][$def_no]['battle_stars'] = $row['battle_stars'];
			$defense_array[$temp_warID]['def'][$def_no]['battle_newStars'] = $row['battle_newStars'];
			$defense_array[$temp_warID]['def'][$def_no]['battle_destructionPercentage'] = $row['battle_destructionPercentage'];
			$defense_array[$temp_warID]['def'][$def_no]['att_name'] = quotemeta( $row['att_name'] );
			$defense_array[$temp_warID]['def'][$def_no]['att_th'] = $row['att_th'];
			$defense_array[$temp_warID]['def'][$def_no]['att_mapPosition'] = $row['att_mapPosition'];
		
			// Setup star image
			$defense_array[$temp_warID]['def'][$def_no]['star_image'] = 'http://www.the-blacklist.ca/images/jumi_code/API/Stars/';
			$defense_array[$temp_warID]['def'][$def_no]['star_image'] .= $row['battle_stars'] . 'Star';
			$defense_array[$temp_warID]['def'][$def_no]['star_image'] .= $row['battle_newStars'] . 'New.png';
			
			// Total stars
			$defense_array[$temp_warID]['total_stars'] += $defense_array[$temp_warID]['def'][$def_no]['battle_stars'];
			$defense_array[$temp_warID]['total_newStars'] += $defense_array[$temp_warID]['def'][$def_no]['battle_newStars'];
					
			if ( $defense_array[$temp_warID]['def'][$def_no]['battle_destructionPercentage'] > $defense_array[$temp_warID]['total_destruction'] ) {
				$defense_array[$temp_warID]['total_destruction'] = $defense_array[$temp_warID]['def'][$def_no]['battle_destructionPercentage'];
			}
		}
	}
	
	// echo var_dump ( $war_array['att_th'] ) . '<br>';
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>
	<h1 align="center"><?php echo $member_array["member_name"]; ?></h1>
	<center><img src="<?php echo $member_array["league_iconUrls_medium"]; ?>" /><br><c/enter>
	<div id="tabs">
	  	<ul>
			<li><a href="#tabs-1">General Info.</a></li>
			<li><a href="#tabs-2">War Info.</a></li>
			<li><a href="#tabs-3">Home Village</a></li>
			<li><a href="#tabs-4">Builders Base</a></li>
	  	</ul>
		<div id="tabs-1">
			<center>
				<h3 align="center">Clan Info.</h3>
				<table width="75%">
					<tr>
						<td align="right" width="40%">
							<b>Clan:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["clan_name"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Role:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_role"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Member Start*:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["start"]; ?>&nbsp;GMT
						</td>
					</tr>
				</table>
				<h3 align="center">Member Info.</h3>
				<table width="75%">
					<tr>
						<td align="right" width="40%">
							<b>Tag:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_tag"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>War Stars:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_warStars"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Exp. Level:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_expLevel"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>League Name:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["league_name"]; ?>
						</td>
					</tr>
				</table>
				<h3 align="center">Home Village</h3>
				<table width="75%">
					<tr>
						<td align="right" width="40%">
							<b>Town Hall:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_townHallLevel"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=64&member=<?php echo $member_array["get_member_id"] ?>"><b>Rank/Previous Rank:</b></a>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_clanRank"]; ?>&nbsp;/&nbsp;
							<?php if ( $member_array["member_clanRank"] > $member_array["member_previousClanRank"] ) {
								echo "<font color=\"red\">";
							 } else {
								echo "<font color=\"green\">";
							 }
							echo $member_array["member_previousClanRank"]; ?>
							</font> 
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Trophies:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_trophies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Best Trophies:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_bestTrophies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Attack Wins:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_attackWins"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Defense Wins:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_defenseWins"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Lowest Rank:<br>Time:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["lowRank"] . "<br>&nbsp;" . $member_array["lowRank_timestamp"] . " GMT"; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Highest Rank:<br>Time:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["highRank"] . "<br>&nbsp;" . $member_array["highRank_timestamp"] . " GMT"; ?>
						</td>
					</tr>
				</table>
				<h3 align="center">Builders Base</h3>
				<table width="75%">
					<tr>
						<td align="right" width="40%">
							<b>Builder Hall:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_builderHallLevel"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Rank/Previous Rank:</b>
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Trophies:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_versusTrophies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Best Trophies:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_bestVersusTrophies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Battle Win Count:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_versusBattleWinCount"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Battle Wins:</b>
						</td>
						<td>
							&nbsp;<?php echo $member_array["member_versusBattleWins"]; ?>
						</td>
					</tr>
				</table>
				<h3 align="center">Donation Info.</h3>
				<table width="75%">
					<tr>
						<td align="right" width="40%">
							<center><b>Donated</b></center>
						</td>
						<td>
							<center>
								<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=65&member=<?php echo $member_array["get_member_id"] ?>"><b>Chart</b></a>						
							</center>
						</td>
						<td>
							<center><b>Received</b></center>
						</td>
					</tr>
					<tr>
						<td align="right">
							<center><?php echo $member_array["member_donations"]; ?></center>
						</td>
						<td>
							<center><b>Current</b></center>
						</td>
						<td>
							<center><?php echo $member_array["member_donationsReceived"]; ?></center>
						</td>
					</tr>
					<tr>
						<td align="right">
							<center><?php echo $member_array["member_total_donations"]; ?></center>
						</td>
						<td>
							<center><b>Total</b></center>
						</td>
						<td>
							<center><?php echo $member_array["member_total_donationsReceived"]; ?></center>
						</td>
					</tr>
					<tr>
						<td align="right">
							<center><?php echo $member_array["maxDonation"] . "<br>" . $member_array["maxDonation_timestamp"]; ?></center>
						</td>
						<td>
							<center><b>Highest<br>Time</b></center>
						</td>
						<td>
							<center><?php echo $member_array["maxReceived"] . "<br>" . $member_array["maxReceived_timestamp"]; ?></center>
						</td>
					</tr>
				</table>
			</center>
		</div>
		<div id="tabs-2">	
			<table width="75%">
				<tr width="20%" align="center">
					<td valign="center">
						<img src="<?php echo $member_array["th_image"]; ?>">
					</td>
					<td>
					<table width=100%">
						<tr>
							<td align="right">
								<b>War Stars:</b>
							</td>
							<td width="30%">
								&nbsp;<?php echo $member_array["member_warStars"]; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Wars:</b>
							</td>
							<td>
								&nbsp;<?php echo $war_array['war']; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Attacks:</b>
							</td>
							<td>
								&nbsp;<?php echo $war_array['attack_used']; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Avg. Stars:</b>
							</td>
							<td>
								&nbsp;<?php echo $war_array['attack_avgStars']; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Avg. New Stars:</b>
							</td>
							<td>
								&nbsp;<?php echo $war_array['attack_avgNewStars']; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Avg. Destruction:</b>
							</td>
							<td>
								&nbsp;<?php echo $war_array['attack_avgDest']; ?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<div id="accordion_war">
				<h3 align="center">War Stats</h3>
				<div id="Member War Stats">
					<table width="75%">
						<tr> 
							<th width="50%"></th>
							<th align="center" width="25%">
								<b>Offense</b>
							</th>
							<th align="center" width="25%">
								<b>Defense</b>
							</th>
						</tr>
						<tr> 
							<th align="right">
								Wars:
							</th>
							<td align="center" colspan="2">
								<?php echo $war_array['war']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Attacks:
							</th>
							<td align="center">
								<?php echo $war_array['attack_used']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_used']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_stars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_stars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Avg. Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_avgStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								3 Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_3Stars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_3Stars']; ?>
							</td>
						</tr>
						</tr>
						<tr> 
							<th align="right">
								2 Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_2Stars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_2Stars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								1 Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_1Stars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_1Stars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								0 Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_0Stars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_0Stars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_newStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_newStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Avg. New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_avgNewStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								3 New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_3NewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_3NewStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								2 New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_2NewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_2NewStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								1 New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_1NewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_1NewStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								0 New Stars:
							</th>
							<td align="center">
								<?php echo $war_array['attack_0NewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['defense_0NewStars']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Avg. Destruction:
							</th>
							<td align="center">
								<?php echo $war_array['attack_avgDest']; ?>%
							</td>
							<td align="center">
								<?php echo $war_array['defense_avgDest']; ?>%
							</td>
						</tr>
						<tr> 
							<th align="right">
								Total Destruction:
							</th>
							<td align="center">
							<?php echo $war_array['attack_avgDest'] * $war_array['attack_used']; ?>%
						</td>
						<td align="center">
							<?php echo $war_array['defense_avgDest'] * $war_array['defense_used']; ?>%
						</td>
					</tr>
					</table>
					<h3 align="center">Attack Counters</h3>
					<table width="75%">
						<tr> 
							<th align="right" width="50%">
								Clean Up Hits:
							</th>
							<td align="center">
								<?php echo $war_array['clean_cnt']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Loot Hits:
							</th>
							<td align="center">
								<?php echo $war_array['loot_cnt']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Early Loot Hits:
							</th>
							<td align="center">
								<?php echo $war_array['earlyLoot_cnt']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Empty Clan Castle Hits:
							</th>
							<td align="center">
								<?php echo $war_array['noCC_cnt']; ?>
							</td>
						</tr>
					</table>
					<h3 align="center">Current Town Hall VS Town Hall Level</h3>
					<table width="75%">
						<tr>
							<th></th>
							<th align="center">
								Hits
							</th>
							<th align="center">
								Avg.<br>Stars
							</th>
							<th align="center">
								Avg.<br>New Stars
							</th>
							<th align="center">
								Avg.<br>Dest.
							</th>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 11:
							</th>
							<td align="center">
								<?php echo $war_array['att_th11']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th11_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th11_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th11_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 10:
							</th>
							<td align="center">
								<?php echo $war_array['att_th10']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th10_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th10_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th10_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 9:
							</th>
							<td align="center">
								<?php echo $war_array['att_th9']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th9_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th9_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th9_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 8:
							</th>
							<td align="center">
								<?php echo $war_array['att_th8']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th8_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th8_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th8_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 7:
							</th>
							<td align="center">
								<?php echo $war_array['att_th7']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th7_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th7_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th7_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 6:
							</th>
							<td align="center">
								<?php echo $war_array['att_th6']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th6_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th6_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th6_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 5:
							</th>
							<td align="center">
								<?php echo $war_array['att_th5']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th5_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th5_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th5_avgDest']; ?>
							</td>
						</tr>
						<tr> 
							<th align="right">
								Town Hall 4:
							</th>
							<td align="center">
								<?php echo $war_array['att_th4']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th4_avgStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th4_avgNewStars']; ?>
							</td>
							<td align="center">
								<?php echo $war_array['att_th4_avgDest']; ?>
							</td>
						</tr>
					</table>
					<h3 align="center">All Town Hall VS Town Hall Level</h3>
					<table width="75%">
					
						<tr>
							<th></th>
							<th align="center" colspan="8">
							 	Enemy TH Level
							</th>
						<tr>
							<th></th>
							<th align="center">
								4
							</th>
							<th align="center">
								5
							</th>
							<th align="center">
								6
							</th>
							<th align="center">
								7
							</th>
							<th align="center">
								8
							</th>
							<th align="center">
								9
							</th>
							<th align="center">
								10
							</th>
							<th align="center">
								11
							</th>
						</tr>
						<?php for ( $i = 11; $i >= 4; $i-- ) { ?>
							<tr> 
								<th align="right">
									<?php echo $i ?>
								</th>
								<?php for ( $j = 4; $j <= 11; $j++ ) { ?>
									<td align="center" >
										<?php if ( $war_array['att_th'][$i][$j]['att'] > 0 ) { ?>
											<?php echo $war_array['att_th'][$i][$j]['avg']; ?>
										<?php } else { ?>
											--
										<?php } ?>
									</td>
								<?php } ?>
							</tr>
						<?php } ?>
					</table>
				</div>
				<h3 align="center">War Attack</h3>
				<div id="Member War Attack">
					<table width="100%">
						<tbody>
							<?php foreach ( $attack_array as $temp_id => $value ) { ?>
								<tr valign="middle">
									<th align="center" colspan="3">
										<?php echo $member_array["clan_name"]; ?>
										&emsp;VS&emsp;
										<?php echo $attack_array[$temp_id]["warlog_opp_name"]; ?>
									</th>
								</tr>
								<tr valign="middle">
									<td align="center">
										<?php if ( $attack_array[$temp_id]["warlog_result"] == 'win' ) {; ?>
											<font color="green">Win</font>
										<?php } elseif ( $attack_array[$temp_id]["warlog_result"] == 'tie' ) {; ?>
											<font color="yellow">Tie</font>
										<?php } elseif ( $attack_array[$temp_id]["warlog_result"] == 'lose' ) {; ?>
											<font color="red">Lose</font>
										<?php } elseif ( $attack_array[$temp_id]["warlog_result"] == 'inWar' ) {; ?>
											<font color="yellow">In War</font>
										<?php } ?>
										<br>
										<?php echo $attack_array[$temp_id]["warlog_clan_stars"]; ?>
										&emsp;Stars&emsp;
										<?php echo $attack_array[$temp_id]["warlog_opp_stars"]; ?>
										<br>
										<?php echo $attack_array[$temp_id]["warlog_clan_destructionPercentage"]; ?>%
										&emsp;Destruction&emsp;
										<?php echo $attack_array[$temp_id]["warlog_opp_destructionPercentage"]; ?>%
										<br>
										<?php echo $attack_array[$temp_id]["warlog_endTime"]; ?>
									</td>
									<td>
										<table width="100%">
											<tr>
												<td align="center">
													<?php if ( isset ( $attack_array[$temp_id]['att_1'] )) { ?>
														<table width="100%">
															<tr>
																<td align="left">
																	<?php echo $attack_array[$temp_id]['att_1']{'def_mapPosition'} . ". " . 
																					$attack_array[$temp_id]['att_1']{'def_name'}; ?>
																</td>
																<th align="center" width=60>
																	<img src="<?php echo $attack_array[$temp_id]['att_1']['star_image'] ?>" width="60" height="20">
																</th>
															</tr> 
															<tr>
																<td align="left">
																	<?php echo '&emsp;TH: ' . $attack_array[$temp_id]['att_1']{'def_th'} .
																			'&emsp;' .$attack_array[$temp_id]['att_1']{'battle_flag'}; ?>
																</td>
																<th align="center">
																	<?php echo $attack_array[$temp_id]['att_1']{'battle_destructionPercentage'} . '%'; ?>
																</th>
															</tr>
														</table>
													<?php } else { ?>
														<center>
															Attack 1 Not Used<br>&emsp;
														</center> 
													<?php } ?>
												</td>
											</tr>
											<tr>
												<td align="center">
													<?php if ( isset ( $attack_array[$temp_id]['att_2'] )) { ?>
														<table width="100%">
															<tr>
																<td align="left">
																	<?php echo $attack_array[$temp_id]['att_2']{'def_mapPosition'} . ". " . 
																					$attack_array[$temp_id]['att_2']{'def_name'}; ?>
																</td>
																<th align="center" width=60>
																	<img src="<?php echo $attack_array[$temp_id]['att_2']['star_image']; ?>" width="60" height="20">
																</th>
															</tr> 
															<tr>
																<td align="left">
																	<?php echo '&emsp;TH: ' . $attack_array[$temp_id]['att_2']{'def_th'} .
																			'&emsp;' .$attack_array[$temp_id]['att_2']{'battle_flag'}; ?>
																</td>
																<th align="center">
																	<?php echo $attack_array[$temp_id]['att_2']{'battle_destructionPercentage'} . '%'; ?>
																</th>
															</tr>
														</table>
													<?php } else { ?>
														<center>
															Attack 2 Not Used<br>&emsp;
														</center> 
													<?php } ?>
												</td>
											</tr>
										</table>
									</td>
									<td align="center">
										Pos: <?php echo $attack_array[$temp_id]{'att_mapPosition'}; ?>
										<br>
										TH: <?php echo $attack_array[$temp_id]{'att_th'}; ?>
										<br>
										Stars: <?php echo $attack_array[$temp_id]['att_1']{'battle_stars'} + 
																$attack_array[$temp_id]['att_2']{'battle_stars'}; ?>
										<br>
										New Stars: <?php echo ( $attack_array[$temp_id]['att_1']{'battle_newStars'} + 
																$attack_array[$temp_id]['att_2']{'battle_newStars'} ); ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>			
				</div>
				<h3 align="center">War Defense</h3>
				<div id="Member Defense">
					<table width="100%">
						<tbody>
							<?php foreach ( $defense_array as $temp_id => $value ) { ?>
								<tr valign="middle">
									<th align="center" colspan="3">
										<?php echo $member_array["clan_name"]; ?>
										&emsp;VS&emsp;
										<?php echo $defense_array[$temp_id]["warlog_opp_name"]; ?>
									</th>
								</tr>
								<tr valign="middle">
									<td align="center">
										Pos: <?php echo $defense_array[$temp_id]{'def_mapPosition'}; ?>
										<br>
										TH: <?php echo $defense_array[$temp_id]{'def_th'}; ?>
										<br>
										Stars: <?php echo $defense_array[$temp_id]['total_stars']; ?>
										<br>
										New Stars: <?php echo ( $defense_array[$temp_id]['total_newStars'] ); ?>
										<br>
										Destruction: <?php echo ( $defense_array[$temp_id]['total_destruction'] ); ?>%
									</td>
									<td>
										<?php if ( isset ( $defense_array[$temp_id]['def'] )) { ?>
											<table width="100%">
												<?php foreach ( $defense_array[$temp_id]['def'] as $temp_def => $value ) { ?>
													<tr>
														<td align="center">
															<table width="100%">
																<tr>
																	<th align="center" width=60>
																		<img src="<?php echo $defense_array[$temp_id]['def'][$temp_def]['star_image']; ?>" width="60" height="20">
																	</th>
																	<td align="left">
																		<?php echo $defense_array[$temp_id]['def'][$temp_def]['att_mapPosition'] . ". " . 
																						$defense_array[$temp_id]['def'][$temp_def]['att_name']; ?>
																	</td>
																</tr> 
																<tr>
																	<th align="center">
																		<?php echo $defense_array[$temp_id]['def'][$temp_def]{'battle_destructionPercentage'} . '%'; ?>
																	</th>
																	<td align="left">
																		<?php echo 'TH: ' . $defense_array[$temp_id]['def'][$temp_def]{'att_th'} . '&emsp;'; ?>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												<?php } ?>
											</table>
										<?php } else { ?>
											No attacks this war.
										<?php } ?>
									</td>
									<td align="center">
										<?php if ( $defense_array[$temp_id]["warlog_result"] == 'win' ) {; ?>
											<font color="green">Win</font>
										<?php } elseif ( $defense_array[$temp_id]["warlog_result"] == 'tie' ) {; ?>
											<font color="yellow">Tie</font>
										<?php } elseif ( $defense_array[$temp_id]["warlog_result"] == 'lose' ) {; ?>
											<font color="red">Lose</font>
										<?php } elseif ( $defense_array[$temp_id]["warlog_result"] == 'inWar' ) {; ?>
											<font color="yellow">In War</font>
										<?php } ?>
										<br>
										<?php echo $defense_array[$temp_id]["warlog_clan_stars"]; ?>
										&emsp;Stars&emsp;
										<?php echo $defense_array[$temp_id]["warlog_opp_stars"]; ?>
										<br>
										<?php echo $defense_array[$temp_id]["warlog_clan_destructionPercentage"]; ?>%
										&emsp;Destruction&emsp;
										<?php echo $defense_array[$temp_id]["warlog_opp_destructionPercentage"]; ?>%
										<br>
										<?php echo $defense_array[$temp_id]["warlog_endTime"]; ?>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>					
				</div>		
			</div>			
		</div>		
		<div id="tabs-3">
			<table width="75%">
				<tr width="20%" align="center">
					<td valign="center">
						<img src="<?php echo $member_array["th_image"]; ?>">
					</td>
					<td>
					<table width=100%">
						<tr>
							<td align="right">
								<b>Town Hall:</b>
							</td>
							<td width="30%">
								&nbsp;<?php echo $member_array["member_townHallLevel"]; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=64&member=<?php echo $member_array["get_member_id"] ?>"><b>Rank/Previous Rank:</b></a>
							</td>
							<td>
								&nbsp;<?php echo $member_array["member_clanRank"]; ?>&nbsp;/&nbsp;
								<?php if ( $member_array["member_clanRank"] > $member_array["member_previousClanRank"] ) {
									echo "<font color=\"red\">";
								 } else {
									echo "<font color=\"green\">";
								 }
								echo $member_array["member_previousClanRank"]; ?>
								</font> 
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Trophies:</b>
							</td>
							<td>
								&nbsp;<?php echo $member_array["member_trophies"]; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Best Trophies:</b>
							</td>
							<td>
								&nbsp;<?php echo $member_array["member_bestTrophies"]; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Attack Wins:</b>
							</td>
							<td>
								&nbsp;<?php echo $member_array["member_attackWins"]; ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								<b>Defense Wins:</b>
							</td>
							<td>
								&nbsp;<?php echo $member_array["member_defenseWins"]; ?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<div id="accordion_home">
				<h3 align="center">Achievement</h3>
				<div id="Member Achievement Info">
					<center>
						<table width="100%">
							<tbody>
								<?php foreach ( $achiev_array[1] as $key => $value ) { ?>
									<tr>
										<td align="center" valign="middle">
											<img src="<?php echo $achiev_array[1][$key]['star_image']; ?>" width="90" height="40">
										</td>
										<td align="left">
											<font size="4"><?php echo $achiev_array[1][$key]["name"] ?></font>
											<br>
											<font size="2"><?php echo $achiev_array[1][$key]["info"] ?></font>
										</td>
										<td align="right" valign="middle">
											<?php echo $achiev_array[1][$key]["completionInfo"] ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</center>
				</div>
				<?php if ( $defBldg_array[1] ) { ?>
					<h3 align="center">Defense Building</h3>
					<div id="Member DefBldg Info">
						<h3>Buildings</h3>
						<table width="100%">
							<thead>
								<tr>
									<th align="center">
										<b></b>
									</th>
									<?php for ( $i = 1; $i <= 8; $i++ ) { ?>
										<th align="center">
											<b><?php echo $i ?></b>
										</th>
									<?php } ?>
									<th align="center">
										<b>Max Level<br>For TH</b>
									</th>
									<th align="center">
										<b>Max<br>Level</b>
									</th>
									<th align="center">
										<b>War<br>Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $defBldg_array[1] as $key => $value ) { ?>
									<?php if ( $key > 99 ) { break; }; ?>
									<tr>
										<td align="right"><?php echo $defBldg_array[1][$key]["name"] ?></td>
										<?php for ( $i = 1; $i <= 8; $i++ ) { ?>
											<td align="center">
												<?php 
													if ( is_null ( $defBldg_array[1][$key]["pos" . $i] )) {
														echo "--";
													} else {
														if ( $defBldg_array[1][$key]["pos" . $i] == $defBldg_array[1][$key]["maxTH"] ) {
															echo "<font color=\"red\">" . $defBldg_array[1][$key]["pos" . $i] . "</font>";
														} else {
															echo $defBldg_array[1][$key]["pos" . $i];
														}
													}
												?>
											</td>
										<?php } ?>
										<td align="center"><?php echo $defBldg_array[1][$key]["maxTH"] ?></td>
										<td align="center"><?php echo $defBldg_array[1][$key]["maxLevel"] ?></td>
										<td align="right"><?php echo number_format ( $defBldg_array[1][$key]["warWeight"], 1 ) ?></td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th align="right" colspan="11">
										<b>Sub Total</b>
									</th>
									<th align="right">
										<b><?php echo number_format ( $warWeight['defBldg'], 1, ".", "," );?></b>
									</th>
								</tr>
							</tfoot>
						</table>
						<h3>Walls</h3>
						<table width="100%">
							<thead>
								<tr>
									<?php for ( $i = 1; $i <= 12; $i++ ) { ?>
										<th align="center">
											<b><?php echo $i ?></b>
										</th>
									<?php } ?>
									<th align="center">
										<b>Total<br>Count</b>
									</th>
									<th align="center">
										<b>Max Cnt.<br>For TH</b>
									</th>
									<th align="center">
										<b>Max Level<br>For TH</b>
									</th>
									<th align="center">
										<b>War<br>Weight</b>
									</th>
								</tr>
								</thead>
									<tbody>
										<?php foreach ( $defBldg_array[1] as $key => $value ) { ?>
											<?php if ( $key < 99 ) { continue; }; ?>
											<tr>
												<?php for ( $i = 1; $i <= 12; $i++ ) { ?>
													<td align="center">
														<?php 
															if ( is_null ( $defBldg_array[1][$key]["pos" . $i] )) {
																echo "--";
															} else {
																if ( $i == $defBldg_array[1][$key]["maxTH"] ) {
																	echo "<font color=\"red\">" . $defBldg_array[1][$key]["pos" . $i] . "</font>";
																} else {
																	echo $defBldg_array[1][$key]["pos" . $i];
																}
															}
														?>
													</td>
												<?php } ?>
												<td align="center"><?php echo $defBldg_array[1][$key]["cnt"] ?></td>
												<td align="center"><?php echo $defBldg_array[1][$key]["cntTH"] ?></td>
												<td align="center"><?php echo $defBldg_array[1][$key]["maxTH"] ?></td>
												<td align="right"><?php echo number_format ( $defBldg_array[1][$key]["warWeight"], 1 ) ?></td>
											</tr>
										<?php } ?>
									</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="15">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['walls'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
						<h3>Totals</h3>
						<table width="50%">
							<thead>
								<tr>
									<th align="center">
										<b></b>
									</th>
									<th align="center">
										<b>War Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="right">
										Building
									</td>
									<td align="right">
										<?php echo number_format ( $warWeight['defBldg'], 1, ".", "," ); ?>
									</td>
								</tr>
								<tr>
									<td align="right">
										Wall
									</td>
									<td align="right">
										<?php echo number_format ( $warWeight['walls'], 1, ".", "," ); ?>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th align="right">
										<b>Total</b>
									</th>
									<th align="right">
										<b><?php echo number_format ( $warWeight['defense'], 1, ".", "," ); ?></b>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
				<?php } ?>
				<?php if ( $trap_array[1] ) { ?>
					<h3 align="center">Trap</h3>
					<div id="Member Trap Info">
						<center>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<?php for ( $i = 1; $i <= 6; $i++ ) { ?>
											<th align="center">
												<b><?php echo $i; ?></b>
											</th>
										<?php } ?>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>War<br>Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $trap_array[1] as $key => $value ) { ?>
										<tr>
											<td align="right"><?php echo $trap_array[1][$key]["name"] ?></td>
											<?php for ( $i = 1; $i <= 6; $i++ ) { ?>
												<td align="center">
													<?php 
														if ( is_null ( $trap_array[1][$key]["pos" . $i] )) {
															echo "--";
														} else {
															if ( $trap_array[1][$key]["pos" . $i] == $trap_array[1][$key]["maxTH"] ) {
																echo "<font color=\"red\">" . $trap_array[1][$key]["pos" . $i] . "</font>";
															} else {
																echo $trap_array[1][$key]["pos" . $i];
															}
														}
													?>
												</td>
											<?php } ?>
										<td align="center"><?php echo $trap_array[1][$key]["maxTH"] ?></td>
											<td align="center"><?php echo $trap_array[1][$key]["maxLevel"] ?></td>
											<td align="right"><?php echo number_format ( $trap_array[1][$key]["warWeight"], 1, ".", "," ); ?></td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="9">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['traps'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
						</center>
					</div>
				<?php } ?>
				<?php if ( $misc_array[1] ) { ?>
					<h3 align="center">Misc Building</h3>
					<div id="Member Misc Info">
						<center>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<?php for ( $i = 1; $i <= 6; $i++ ) { ?>
											<th align="center">
												<b><?php echo $i; ?></b>
											</th>
										<?php } ?>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>War<br>Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $misc_array[1] as $key => $value ) { ?>
										<tr>
											<td align="right"><?php echo $misc_array[1][$key]["name"] ?></td>
											<?php for ( $i = 1; $i <= 6; $i++ ) { ?>
												<td align="center">
													<?php 
														if ( is_null ( $misc_array[1][$key]["pos" . $i] )) {
															echo "--";
														} else {
															if ( $misc_array[1][$key]["pos" . $i] == $misc_array[1][$key]["maxTH"] ) {
																echo "<font color=\"red\">" . $misc_array[1][$key]["pos" . $i] . "</font>";
															} else {
																echo $misc_array[1][$key]["pos" . $i];
															}
														}
													?>
												</td>
											<?php } ?>
										<td align="center"><?php echo $misc_array[1][$key]["maxTH"] ?></td>
										<td align="center"><?php echo $misc_array[1][$key]["maxLevel"] ?></td>
										<td align="right"><?php echo number_format ( $misc_array[1][$key]["warWeight"], 1, ".", "," ); ?></td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="9">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['miscBldg'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
						</center>
					</div>
				<?php } ?>
				<?php if ( $heros_array[1] ) { ?>
					<h3 align="center">Hero</h3>
					<div id="Member Hero Info">
						<center>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<th align="center">
											<b>Level</b>
										</th>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>Def.<br>War Wgt.</b>
										</th>
										<th align="center">
											<b>Off.<br>War Wgt.</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $heros_array[1] as $key => $value ) { ?>
										<tr>
											<td align="right"><?php echo $heros_array[1][$key]["name"] ?></td>
											<td align="center">
												<?php 
													if ( $heros_array[1][$key]["level"] == $heros_array[1][$key]["maxTH"] ) {
														echo "<font color=\"red\">" . $heros_array[1][$key]["level"] . "</font>";
													} else {
														echo $heros_array[1][$key]["level"];
													}
												?>
											</td>
											<td align="center"><?php echo $heros_array[1][$key]["maxTH"] ?></td>
											<td align="center"><?php echo $heros_array[1][$key]["maxLevel"] ?></td>
											<td align="right"><?php echo number_format ( $heros_array[1][$key]["warWeight_def"], 1, ".", "," ); ?></td>
											<td align="right"><?php echo number_format ( $heros_array[1][$key]["warWeight_off"], 1, ".", "," ); ?></td>
											</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="4">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['heros']['def'], 1, ".", "," );?></b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['heros']['off'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
						</center>
					</div>
				<?php } ?>
				<h3 align="center">Troop</h3>
				<div id="Member Troop Info">
					<center>
						<h3>Elixir Troops</h3>
						<table width="100%">
							<thead>
								<tr>
									<th align="center">
										<b></b>
									</th>
									<th align="center">
										<b>Level</b>
									</th>
									<th align="center">
										<b>Max Level<br>For TH</b>
									</th>
									<th align="center">
										<b>Max<br>Level</b>
									</th>
									<th align="center">
										<b>War<br>Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $troops_array[1] as $key => $value ) { ?>
									<?php if ( $key > 99 ) { break; } ?>
									<tr>
										<td align="right"><?php echo $troops_array[1][$key]["name"] ?></td>
										<td align="center">
											<?php 
												if ( $troops_array[1][$key]["level"] == $troops_array[1][$key]["maxTH"] ) {
													echo "<font color=\"red\">" . $troops_array[1][$key]["level"] . "</font>";
												} else {
													echo $troops_array[1][$key]["level"];
												}
											?>
										</td>
										<td align="center"><?php echo $troops_array[1][$key]["maxTH"] ?></td>
										<td align="center"><?php echo $troops_array[1][$key]["maxLevel"] ?></td>
										<td align="right"><?php echo number_format ( $troops_array[1][$key]["warWeight"], 1, ".", "," ) ?></td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th align="right" colspan="4">
										<b>Sub Total</b>
									</th>
									<th align="right">
										<b><?php echo number_format ( $warWeight['troops_elixir'], 1, ".", "," );?></b>
									</th>
								</tr>
							</tfoot>
						</table>
						<h3>Dark Elixir Troops</h3>
						<table width="100%">
							<thead>
								<tr>
									<th align="center">
										<b></b>
									</th>
									<th align="center">
										<b>Level</b>
									</th>
									<th align="center">
										<b>Max Level<br>For TH</b>
									</th>
									<th align="center">
										<b>Max<br>Level</b>
									</th>
									<th align="center">
										<b>War<br>Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $troops_array[1] as $key => $value ) { ?>
									<?php if ( $key < 99 ) { continue; } ?>
									<tr>
										<td align="right"><?php echo $troops_array[1][$key]["name"] ?></td>
										<td align="center">
											<?php 
												if ( $troops_array[1][$key]["level"] == $troops_array[1][$key]["maxTH"] ) {
													echo "<font color=\"red\">" . $troops_array[1][$key]["level"] . "</font>";
												} else {
													echo $troops_array[1][$key]["level"];
												}
											?>
										</td>
										<td align="center"><?php echo $troops_array[1][$key]["maxTH"] ?></td>
										<td align="center"><?php echo $troops_array[1][$key]["maxLevel"] ?></td>
										<td align="right"><?php echo number_format ( $troops_array[1][$key]["warWeight"], 1, ".", "," ); ?></td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th align="right" colspan="4">
										<b>Sub Total</b>
									</th>
									<th align="right">
										<b><?php echo number_format ( $warWeight['troops_de'], 1, ".", "," );?></b>
									</th>
								</tr>
							</tfoot>
						</table>
						<h3>Totals</h3>
						<table width="50%">
							<thead>
								<tr>
									<th align="center">
										<b></b>
									</th>
									<th align="center">
										<b>War Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="right">
										Elixir Troops
									</td>
									<td align="right">
										<?php echo number_format ( $warWeight['troops_elixir'], 1, ".", "," ); ?>
									</td>
								</tr>
								<tr>
									<td align="right">
										Dark Elixir Troops
									</td>
									<td align="right">
										<?php echo number_format ( $warWeight['troops_de'], 1, ".", "," ); ?>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<th align="right">
										<b>Total</b>
									</th>
									<th align="right">
										<b><?php echo number_format ( $warWeight['troops'], 1, ".", "," ); ?></b>
									</th>
								</tr>
							</tfoot>
						</table>
					</center>
				</div>
				<?php if ( $spells_array[1] ) { ?>
					<h3 align="center">Spell</h3>
					<div id="Member Spell Info">
						<center>
							<h3>Elixir Spells</h3>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<th align="center">
											<b>Level</b>
										</th>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>War<br>Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $spells_array[1] as $key => $value ) { ?>
										<?php if ( $key > 99 ) { break; } ?>
										<tr>
											<td align="right"><?php echo $spells_array[1][$key]["name"] ?></td>
											<td align="center">
												<?php 
													if ( $spells_array[1][$key]["level"] == $spells_array[1][$key]["maxTH"] ) {
														echo "<font color=\"red\">" . $spells_array[1][$key]["level"] . "</font>";
													} else {
														echo $spells_array[1][$key]["level"];
													}
												?>
											</td>
											<td align="center"><?php echo $spells_array[1][$key]["maxTH"] ?></td>
											<td align="center"><?php echo $spells_array[1][$key]["maxLevel"] ?></td>
											<td align="right"><?php echo number_format ( $spells_array[1][$key]["warWeight"], 1, ".", "," ); ?></td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="4">
											<b>Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['spells_elixir'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
							<h3>Dark Elixir Spells</h3>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<th align="center">
											<b>Level</b>
										</th>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>War<br>Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $spells_array[1] as $key => $value ) { ?>
										<?php if ( $key < 99 ) { continue; } ?>
										<tr>
											<td align="right"><?php echo $spells_array[1][$key]["name"] ?></td>
											<td align="center">
												<?php 
													if ( $spells_array[1][$key]["level"] == $spells_array[1][$key]["maxTH"] ) {
														echo "<font color=\"red\">" . $spells_array[1][$key]["level"] . "</font>";
													} else {
														echo $spells_array[1][$key]["level"];
													}
												?>
											</td>
											<td align="center"><?php echo $spells_array[1][$key]["maxTH"] ?></td>
											<td align="center"><?php echo $spells_array[1][$key]["maxLevel"] ?></td>
											<td align="right"><?php echo $spells_array[1][$key]["warWeight"] ?></td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="4">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['spells_de'], 1, ".", "," );?></b>
										</th>
									</tr>
								</tfoot>
							</table>
							<h3>Totals</h3>
							<table width="50%">
								<thead>
									<tr>
										<th align="center">
											<b></b>
										</th>
										<th align="center">
											<b>War Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td align="right">
											Elixir Spells
										</td>
										<td align="right">
											<?php echo number_format ( $warWeight['spells_elixir'], 1, ".", "," ); ?>
										</td>
									</tr>
									<tr>
										<td align="right">
											Dark Elixir Spells
										</td>
										<td align="right">
											<?php echo number_format ( $warWeight['spells_de'], 1, ".", "," ); ?>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th align="right">
											<b>Total</b>
										</th>
										<th align="right">
											<b><?php echo number_format ( $warWeight['spells'], 1, ".", "," ); ?></b>
										</th>
									</tr>
								</tfoot>
							</table>
						</center>
					</div>
				<?php } ?>
				<h3 align="center">War Weight</h3>
				<div id="Member WarWeight Info">
					<center>
						<h3>Defensive War Weight</h3>
						<table width="50%">
							<tr>
								<th align="right" width="60%">
									Def. Bldg.:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['defBldg'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Walls:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['walls'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Trap:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['traps'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Misc. Bldg.:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['miscBldg'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Hero:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['heros']['def'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Subtotal:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['defense'], 1, ".", "," ); ?>
								</th>
							</tr>
						</table>
					<h3 align="center">Offensive War Weight</h3>
						<table width="50%">
							<tr>
								<th align="right">
									Elixir Troop:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['troops_elixir'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Dark Elixir Troop:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['troops_de'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Spell War Weight:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['spells'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Hero:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['heros']['off'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Subtotal:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['offense'], 1, ".", "," ); ?>
								</th>
							</tr>
						</table>
						<h3>Total War Weight*</h3>
						<table width="50%">
							<tr>
								<th align="right" width="60%">
									Defensive:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['defense'], 0, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Offensive:
								</th>
								<td align="right">
									<?php echo number_format ( $warWeight['offense'], 0, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<th align="right">
									Total:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['total'], 0, ".", "," ); ?>
								</th>
							</tr>
							<tr>
								<th align="right">
									Rounded:
								</th>
								<th align="right">
									<?php echo number_format ( $warWeight['rounded'], 0, ".", "," ); ?>
								</th>
							</tr>
						</table>
						<br><h4>Notes</h4>
						<font size="2">
							Total War Weight is used by COC to list the base order in wars.<br>
							Rounded War Weight is used by COC in war match making process.<br>
						</font> 
					<h3>-- Information Only --</h3>
						<table width="75%">
							<tr>
								<th align="center">
									<b>Town Hall</b>
								</th>
								<th align="center">
									<b>Min. Adj.<br>War Weight</b>
								</th>
								<th align="center">
									<b>Median<br>War Weight</b>
								</th>
								<th align="center">
									<b>Max<br>War Weight</b>
								</th>
							</tr>
							<?php for ( $i = 1; $i <= 11; $i++ ) { ?>
								<tr>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
									<td align="center">
										<b><?php echo number_format ( $ww_sql_array["min"][$i], 0, ".", "," ) ?></b>
									</td>
									<td align="center">
										<b><?php echo number_format ( $ww_sql_array["med"][$i], 0, ".", "," ) ?></b>
									</td>
									<td align="center">
										<b><?php echo $ww_sql_array["max"][$i] ?></b>
									</td>
								</tr>
							<?php } ?>
						</table>
					</center>
					<br>*War Weight calculations from <a href="http://www.clashkings2.com/th-8-5--amp--th-9-5.html">ClashKings2</a><br>
				</div>
				<h3 align="center">Engineered</h3>
				<div id="Member Engineered Info">
					<center>
						<h3>Engineered</h3>
						<table 100%>
							<tr>
								<td align="center">
									<b>Step 1</b>
								</td>
								<td align="right" colspan="3">
									Is there anything missing on the base?
								</td>
								<td align="center" width="15%">
									<?php if ( $warWeight['step1'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="center" width="10%">
									<b>Step 2</b>
								</td>
								<td align="right" colspan="3">
									Did the base NOT max out at the previous Townhall level?
								</td>
								<td align="center" width="5%">
									<?php if ( $warWeight['step2'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="center" width="10%" rowspan="3">
									<b>Step 3</b>
								</td>
								<td align="right">
									Offensive ratio
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_offRatio'], 2 ) ?>%
								</td>
								<td align="right" rowspan="3">
									Level ratio > 30%?
								</td>
								<td align="center" rowspan="3">
									<?php if ( $warWeight['step3'] == 0 ) { ?>
										<font color="green">No</font>
									<?php } else { ?>
										<font color="red">Yes</font>
									<?php } ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Defensive ratio
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_defRatio'], 2 ) ?>%
								</td>
							</tr>
							<tr>
								<td align="right">
									Difference	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step3_diffRatio'], 2 ) ?>%
								</td>
							</tr>
							<tr>
								<td align="center" width="10%" rowspan="3">
									<b>Step 4</b>
								</td>
								<td align="right">
									Offensive weight	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['offense'], 1, ".", "," ); ?>
								</td>
								<td align="right" rowspan="3">
									Classification:	
								</td>
								<td align="center" rowspan="3">
									<?php echo $warWeight['step4_text'] ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Defensive weight
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['defense'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Difference	
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['step4_diff'], 1, ".", "," ); ?>
								</td>
							</tr>
						
						</table>
					</center> 
					<left>
						<font size="2">
							<br> 
							Step 1: Does the base have all items, troops, & spells at the CURRENT Townhall level, or are any missing?<br>
							Step 2: Did the base max out everything at the PREVIOUS Townhall level, or was anything missing?<br> 
							Step 3: Is the difference between offensive & defensive total level ratios more than 30%?<br> 
							Step 4: If (Step 1 AND Step 3 equal YES) <b>OR</b> (Step 2 AND Step 3 equal YES)<br> 
							The base is <em>ENGINEERED</em>. <br> 
							Then use the Classification table below.<br><br>									
						</font>
					</left>
					<center> 
						<table width="50%" class="sortable">
							<tr>
								<td align="center">
									Classification
								</td>
								<td align="center">
									Bracket
								</td>
							</tr>
							<tr>
								<td align="center">
									Low
								</td>
								<td align="center">
									0-4,999	
								</td>
							</tr>
							<tr>
								<td align="center">
									Medium
								</td>
								<td align="center">
									5,000-14,999	
								</td>
							</tr>
							<tr>
								<td align="center">
									High
								</td>
								<td align="center">
									15,000-max	
								</td>
							</tr>
						</table>
					</center>
				</div>
			</div>
		</div>
		<div id="tabs-4">
			<table width="75%">
				<tr width="20%" align="center">
					<td valign="center">
						<img src="<?php echo $member_array["th_image"]; ?>">
					</td>
					<td>
						<table width=100%">
							<tr>
								<td align="right">
									<b>Builder Hall:</b>
								</td>
								<td width="30%">
									&nbsp;<?php echo $member_array["member_builderHallLevel"]; ?>
								</td>
							</tr>
							<tr>
								<td align="right">
								<b>Rank/Previous Rank:</b>
							</td>
							<td>
							</td>
							</tr>
							<tr>
								<td align="right">
									<b>Trophies:</b>
								</td>
								<td>
									&nbsp;<?php echo $member_array["member_versusTrophies"]; ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									<b>Best Trophies:</b>
								</td>
								<td>
									&nbsp;<?php echo $member_array["member_bestVersusTrophies"]; ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									<b>Battle Win Count:</b>
								</td>
								<td>
									&nbsp;<?php echo $member_array["member_versusBattleWinCount"]; ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									<b>Battle Wins:</b>
								</td>
								<td>
									&nbsp;<?php echo $member_array["member_versusBattleWins"]; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<div id="accordion_bldg">
				<h3 align="center">Achievement</h3>
				<div id="Member Achievement Info">
					<center>
						<table width="100%">
							<tbody>
								<?php foreach ( $achiev_array[2] as $key => $value ) { ?>
									<tr>
										<td align="center" valign="middle">
											<img src="<?php echo $achiev_array[2][$key]['star_image']; ?>" width="90" height="40">
										</td>
										<td align="left">
											<font size="4"><?php echo $achiev_array[2][$key]["name"] ?></font>
											<br>
											<font size="2"><?php echo $achiev_array[2][$key]["info"] ?></font>
										</td>
										<td align="right" valign="middle">
											<?php echo $achiev_array[2][$key]["completionInfo"] ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</center>
				</div>
				<?php if ( $defBldg_array[2] ) { ?>
					<h3 align="center">Defense Building</h3>
					<div id="Member DefBldg Info">
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div>
				<?php } ?>
				<?php if ( $trap_array[2] ) { ?>
					<h3 align="center">Trap</h3>
					<div id="Member Trap Info">
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div>
				<?php } ?>
				<?php if ( $misc_array[2] ) { ?>
					<h3 align="center">Misc Building</h3>
					<div id="Member MiscBldg Info">
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div>
				<?php } ?>
				<?php if ( $heros_array[2] ) { ?>
					<h3 align="center">Hero</h3>
					<div id="Member Hero Info">
						<center>
							<table width="100%">
								<thead>
									<tr>
										<th align="center">
											<b>Hero</b>
										</th>
										<th align="center">
											<b>Level</b>
										</th>
										<th align="center">
											<b>Max Level<br>For TH</b>
										</th>
										<th align="center">
											<b>Max<br>Level</b>
										</th>
										<th align="center">
											<b>War<br>Weight</b>
										</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ( $heros_array[2] as $key => $value ) { ?>
										<tr>
											<td align="right"><?php echo $heros_array[2][$key]["name"] ?></td>
											<td align="center">
												<?php 
													if ( $heros_array[2][$key]["level"] == $heros_array[2][$key]["maxTH"] ) {
														echo "<font color=\"red\">" . $heros_array[2][$key]["level"] . "</font>";
													} else {
														echo $heros_array[2][$key]["level"];
													}
												?>
											</td>
											<td align="center"><?php echo $heros_array[2][$key]["maxTH"] ?></td>
											<td align="center"><?php echo $heros_array[2][$key]["maxLevel"] ?></td>
											<td align="right">
												N/A
												<!-- <?php echo $heros_array[2][$key]["warWeight"] ?> -->
											</td>
										</tr>
									<?php } ?>
								</tbody>
								<tfoot>
									<tr>
										<th align="right" colspan="4">
											<b>Sub Total</b>
										</th>
										<th align="right">
											<b>
												N/A
												<!-- <?php echo number_format ( $warWeight['heros']['off'], 1, ".", "," );?> -->
											</b>
										</th>
									</tr>
								</tfoot>
							</table>
						</center>
					</div>
				<?php } ?>
				<h3 align="center">Troop</h3>
				<div id="Member Troop Info">
					<center>
						<table width="100%">
							<thead>
								<tr>
									<th align="center">
										<b>Troop</b>
									</th>
									<th align="center">
										<b>Level</b>
									</th>
									<th align="center">
										<b>Max Level<br>For BH</b>
									</th>
									<th align="center">
										<b>Max<br>Level</b>
									</th>
									<th align="center">
										<b>War<br>Weight</b>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ( $troops_array[2] as $key => $value ) { ?>
									<tr>
										<td align="right"><?php echo $troops_array[2][$key]["name"] ?></td>
										<td align="center">
											<?php 
												if ( $troops_array[2][$key]["level"] == $troops_array[2][$key]["maxTH"] ) {
													echo "<font color=\"red\">" . $troops_array[2][$key]["level"] . "</font>";
												} else {
													echo $troops_array[2][$key]["level"];
												}
											?>
										</td>
										<td align="center"><?php echo $troops_array[2][$key]["maxTH"] ?></td>
										<td align="center"><?php echo $troops_array[2][$key]["maxLevel"] ?></td>
										<td align="right">
											N/A
											<!-- <?php echo $troops_array[2][$key]["warWeight"] ?> -->
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th align="right" colspan="4">
										<b>Sub Total</b>
									</th>
									<th align="right">
										<b>
											N/A
											<!-- <?php echo number_format ( $warWeight['troops_bldg'], 1, ".", "," );?> -->
										</b>
									</th>
								</tr>
							</tfoot>
						</table>
					</center>
				</div>
				<?php if ( $spells_array[2] ) { ?>
					<h3 align="center">Spell</h3>
					<div id="Member Spell Info">
						<h2 align="center">
							<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
							<br>Under Construction
						</h2>
					</div> 
				<?php } ?>
				<!-- <h3 align="center">War Weight</h3>
				<div id="Member WarWeight Info">
					<h2 align="center">
						<img src="http://www.the-blacklist.ca/images/jumi_code/API/Builder.png" width="125" height="125">
						<br>Under Construction
					</h2>
				</div> -->
			</div>
		</div>
	</div>	
	<center>
		<img src="<?php echo $member_array["clan_badgeUrls_medium"]; ?>" /><br>
	</center>
	<center>
		<b>This data was updated at <?php echo $member_array["member_timestamp"]; ?> GMT.</b><br>
		<b>This data only goes back to <?php echo $member_array["oldest_timestamp"]; ?> GMT.</b>
	</center>
	</br>  
</body>	

<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
<!-- JQuery Script -->
	<link rel="stylesheet" href="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.css">
	<script src="http://www.the-blacklist.ca/images/jumi_code/jquery.js"></script>
	<script src="http://www.the-blacklist.ca/images/jumi_code/jquery-ui.js"></script>
	<script>
		$(function() {
			$( "#accordion_war,#accordion_home,#accordion_bldg" ).accordion({
				heightStyle: "content"
			});
			$( "#accordion_war,#accordion_home,#accordion_bldg" ).accordion({
				collapsible: true
			});
			$( "#accordion_war" ).accordion({
				active: 2
			});
			$( "#accordion_home" ).accordion({
				active: <?php echo $accordion_home_cnt ?>
			});
			$( "#accordion_bldg" ).accordion({
				 active: <?php echo $accordion_bldg_cnt ?> 
			});
			$( "#tabs" ).tabs({
				active: 2
			});
		} );

	</script>

</body>

