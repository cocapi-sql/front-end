<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>Donations Received Ratio</title>
</head>
<body>
	<div id="Top 10 Donations Received Ratio Last Day">
		<h1 align="center">
			Top 10 Donations Received Ratio Last Day
		</h1>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Received<br>Ratio*</b></th>
				<th align="center"><b>Total<br>Sent</b></th>				
				<th align="center"><b>Total<br>Received</b></th>				
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donationsReceived_ratio_day" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$key]["trophies"]; ?></center>
						</td>
						<td>
							<center><font color="red"><?php echo $member_array[$i]["total_donationsReceived_ratio_day"]; ?>%</font></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donations_day"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donationsReceived_day"]; ?></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
			* Received Ratio = Total Donations Received / Total Donations Sent
		</left>
	</div>
	<br>
	<div id="Top 10 Donations Received Ratio Last Week">
		<h1 align="center">
			Top 10 Donations Received Ratio Last Week
		</h1>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Received<br>Ratio*</b></th>
				<th align="center"><b>Total<br>Sent</b></th>				
				<th align="center"><b>Total<br>Received</b></th>				
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donationsReceived_ratio_week" ); ?>
				<?php for ( $i = 0; $i < 10; $i++ ) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$i]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$i]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$i]["tag"] ?>"><?php echo $member_array[$i]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$i]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$key]["trophies"]; ?></center>
						</td>
						<td>
							<center><font color="red"><?php echo $member_array[$i]["total_donationsReceived_ratio_week"]; ?>%</font></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donations_week"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$i]["donationsReceived_week"]; ?></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
			* Received Ratio = Total Donations Received / Total Donations Sent
		</left>
	</div>
	<br>
	<div id="All Donations Received Ratio">
		<h1 align="center">
			All Donations Received Ratio
		</h1>
		<table width="100%" class="sortable">
			<thead>
				<th align="center"><b><br>Clan</b></th>
				<th align="center"><b><br>League</b></th>
				<th align="center"><b><br>Name</b></th>
				<th align="center"><b><br>Role</b></th>
				<th align="center"><b><br>Trophies</b></th>
				<th align="center"><b>Received<br>Ratio*</b></th>
				<th align="center"><b>Total<br>Sent</b></th>				
				<th align="center"><b>Total<br>Received</b></th>				
			</thead>
			<tbody>
				<?php usort( $member_array, "total_donationsReceived_ratio_current" ); ?>
				<?php foreach ($member_array as $key => $value) { ?>
					<tr> 
						<td>
							<?php echo $member_array[$key]["clan_name"]; ?>
						</td>
						<td align="center">
							<img src="<?php echo $member_array[$key]["league_iconUrls_tiny"]; ?>" /> 
						</td>
						<td>
							<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=63&member=<?php echo $member_array[$key]["tag"] ?>"><?php echo $member_array[$key]["name"] ?></a>
						</td>
						<td>
							<?php echo $member_array[$key]["role"]; ?>
						</td>
						<td>
							<center><?php echo $member_array[$key]["trophies"]; ?></center>
						</td>
						<td>
							<center><font color="red"><?php echo $member_array[$key]["total_donationsReceived_ratio"]; ?>%</font></center>
						</td>
						<td>
							<center><?php echo $member_array[$key]["total_donations"]; ?></center>
						</td>
						<td>
							<center><?php echo $member_array[$key]["total_donationsReceived"]; ?></center>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
		<left>
			* Received Ratio = Total Donations Received / Total Donations Sent
		</left>
	</div>







<center>
	<b>This data was updated at <?php echo $timestamp_current; ?> GMT.</b>
</center>
</br>  
<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
</body>