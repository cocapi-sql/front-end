<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$clan_selected = "BLACK LIST";
	$clan_array = array();
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	# Get clan or use default set to BlackList
	if ( isset ( $_GET['clan'] ) ) {
		$clan_selected = $_GET['clan'];
	}
			
	$clan_sql = "SELECT * ";
	$clan_sql .= "FROM `API_Clan` AS ac ";
	$clan_sql .= "INNER JOIN `API_Clan_Info` AS aci ";
	$clan_sql .= "  ON ac.clan_tag=aci.clan_info_tag ";
	$clan_sql .= "WHERE clan_name LIKE '" . $clan_selected . "' ";
	$clan_sql .= "ORDER BY clan_info_time DESC ";
	$clan_sql .= "LIMIT 1;";
	$clan_result = $conn->query($clan_sql);
	

	while($clan_row = $clan_result->fetch_assoc()) {
		$clan_array["clan_tag"] = $clan_row["clan_tag"];
		$clan_array["clan_name"] = $clan_row["clan_name"];
		$clan_array["clan_description"] = str_replace( '\\', '', $clan_row["clan_description"] );
		$clan_array["clan_badgeUrls_small"] = $clan_row["clan_badgeUrls_small"];
		$clan_array["clan_badgeUrls_medium"] = $clan_row["clan_badgeUrls_medium"];
		$clan_array["clan_badgeUrls_large"] = $clan_row["clan_badgeUrls_large"];
		$clan_array["clan_location_id"] = $clan_row["clan_location_id"];
		$clan_array["clan_location_name"] = $clan_row["clan_location_name"];
		$clan_array["clan_location_countryCode"] = $clan_row["clan_location_countryCode"];
		$clan_array["clan_warFrequency"] = $clan_row["clan_warFrequency"];
		$clan_array["clan_info_time"] = $clan_row["clan_info_time"];
		$clan_array["clan_info_clanLevel"] = $clan_row["clan_info_clanLevel"];
		$clan_array["clan_info_clanPoints"] = $clan_row["clan_info_clanPoints"];
		$clan_array["clan_info_warWins"] = $clan_row["clan_info_warWins"];
		$clan_array["clan_info_warWinStreak"] = $clan_row["clan_info_warWinStreak"];
		$clan_array["clan_info_warTies"] = $clan_row["clan_info_warTies"];
		$clan_array["clan_info_warLosses"] = $clan_row["clan_info_warLosses"];
		$clan_array["clan_info_members"] = $clan_row["clan_info_members"];
		$clan_array["clan_info_type"] = $clan_row["clan_info_type"];
		$clan_array["clan_info_requiredTrophies"] = $clan_row["clan_info_requiredTrophies"];
		$clan_array["clan_info_isWarLogPublic"] = $clan_row["clan_info_isWarLogPublic"];
		$clan_array["get_clan_id"] = str_replace ( '#', '', $clan_row["clan_tag"] );
	}
	
	if ( $clan_array["clan_info_warLosses"] == 0 ) {
		
		$clan_sql = "SELECT clan_info_warTies, clan_info_warLosses ";
		$clan_sql .= "FROM `API_Clan_Info` ";
		$clan_sql .= "WHERE clan_info_tag LIKE '" . $clan_array["clan_tag"] . "' ";
		$clan_sql .= " AND clan_info_warLosses > 0 ";
		$clan_sql .= "ORDER BY clan_info_time ";
		$clan_sql .= "DESC LIMIT 1;";
		$clan_result = $conn->query($clan_sql);
	

		while($clan_row = $clan_result->fetch_assoc()) {
			$clan_array["clan_info_warTies"] = $clan_row["clan_info_warTies"];
			$clan_array["clan_info_warLosses"] = $clan_row["clan_info_warLosses"];
		}
	}
	
	
	$clan_highWinStreak_sql = "SELECT `clan_info_time`, `clan_info_warWinStreak` AS highWinStreak ";
	$clan_highWinStreak_sql .= "FROM `API_Clan_Info` ";
	$clan_highWinStreak_sql .= "WHERE `clan_info_tag` LIKE '#P9GYVGCP' ";
	$clan_highWinStreak_sql .= "ORDER BY `clan_info_warWinStreak` DESC, `clan_info_time` DESC ";
	$clan_highWinStreak_sql .= "LIMIT 1;";
	$clan_highWinStreak_result = $conn->query($clan_highWinStreak_sql);
	

	while($clan_highWinStreak_row = $clan_highWinStreak_result->fetch_assoc()) {
		$clan_array["highWinStreak"] = $clan_highWinStreak_row["highWinStreak"];
		$clan_array["highWinStreak_timestamp"] = $clan_highWinStreak_row["clan_info_time"];
	}
	
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>

	<h1 align="center"><?php echo $clan_array["clan_name"]; ?></h1>
	<center>
		<img src="<?php echo $clan_array["clan_badgeUrls_medium"]; ?>" /><br>
		<h3>Description:</h3>
		<?php echo $clan_array["clan_description"]; ?><br>
	</center>

	<div id="Clan General Info">
		<center>
			<h3 align="center">General Info.</h3>
			<table width="50%">
				<tr>
					<td align="right" width="40%">
						<b>Tag:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_tag"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Level:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_clanLevel"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Points:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_clanPoints"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Members:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_members"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Type:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_type"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Trophies:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_requiredTrophies"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Frequency:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_warFrequency"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Location:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_location_name"]; ?>
					</td>
				</tr>
			</table>
		</center>
	</div>
	<div id="Clan War Info">
		<center>
			<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=59&clan=<?php echo $clan_array["get_clan_id"] ?>">
				<h3 align="center">War Info.</h3>
			</a>
			<table width="50%">
				<tr>
					<td align="right" width="40%">
						<b>Wars Won :</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_warWins"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>War Win Streak :</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_warWinStreak"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>War Tied :</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_warTies"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>War Lost :</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["clan_info_warLosses"]; ?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<b>Highest Win Streak:<br>Time:</b>
					</td>
					<td>
						&nbsp;<?php echo $clan_array["highWinStreak"] . "<br>&nbsp;" . $clan_array["highWinStreak_timestamp"] . " GMT"; ?>
					</td>
				</tr>
			</table>
		</center>
	</div>
  
<center>
	<b>This data was updated at <?php echo $clan_array["clan_info_time"]; ?> GMT.</b>
</center>
</br>  

<h2 style="text-align: center;">Clan Picker</h2>
<form method="get">

	<center>
		<input type="radio" name="clan" value="BLACK LIST">&nbsp; Black List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="WHITE LIST">&nbsp; White List &nbsp;&nbsp;</input>
		<input type="radio" name="clan" value="GOLD LIST">&nbsp; Gold List &nbsp;&nbsp;</input>
	</center>
	<center><input type="submit" name="submit" value="Submit"/></center>
</form>

</body>