<?php
function warWeight ( $inputName, $inputLevel ) {
	$output = 0;
	
	$warweight_sql = "SELECT `ww_weight` ";
	$warweight_sql .= "FROM `API_WarWeight` ";
	$warweight_sql .= "WHERE `ww_name` LIKE '" . $inputName . "' ";
	$warweight_sql .= "AND `ww_level` = " . $inputLevel . ";";
	$warweight_result = $conn->query($warweight_sql);
	
	while($row = $warweight_result->fetch_assoc()) {
		$output = $row["ww_weight"];
	}
	
	return $output;
}
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	// Variables
	$troops_array = array();
	$heros_array = array();
	$spells_array = array();



	$troop_sql = "SELECT amt.`troops_name` AS 'name', amt.`troops_maxLevel` AS 'maxLevel', ";
	$troop_sql .= "(SELECT ww1.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww1 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww1.`ww_name` AND ";
	$troop_sql .= "     ww1.`ww_level` = 1) AS '1', ";
	$troop_sql .= "(SELECT ww2.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww2 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww2.`ww_name` AND ";
	$troop_sql .= "     ww2.`ww_level` = 2) AS '2', ";
	$troop_sql .= "(SELECT ww3.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww3 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww3.`ww_name` AND ";
	$troop_sql .= "     ww3.`ww_level` = 3) AS '3', ";
	$troop_sql .= "(SELECT ww4.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww4 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww4.`ww_name` AND ";
	$troop_sql .= "     ww4.`ww_level` = 4) AS '4', ";
	$troop_sql .= "(SELECT ww5.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww5 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww5.`ww_name` AND ";
	$troop_sql .= "     ww5.`ww_level` = 5) AS '5', ";
	$troop_sql .= "(SELECT ww6.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww6 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww6.`ww_name` AND ";
	$troop_sql .= "     ww6.`ww_level` = 6) AS '6', ";
	$troop_sql .= "(SELECT ww7.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww7 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww7.`ww_name` AND ";
	$troop_sql .= "     ww7.`ww_level` = 7) AS '7', ";
	$troop_sql .= "(SELECT ww8.`ww_weight` ";
	$troop_sql .= "   FROM `API_WarWeight` AS ww8 ";
	$troop_sql .= "   WHERE amt.`troops_name` LIKE ww8.`ww_name` AND ";
	$troop_sql .= "     ww8.`ww_level` = 8) AS '8' ";
	$troop_sql .= "FROM `API_Mem_Troops` AS amt ";
	$troop_sql .= "ORDER BY `troops_name` ASC;";
	$troop_result = $conn->query($troop_sql);
	
	$i = 0;
	while($row = $troop_result->fetch_assoc()) {
		$troops_array[$i]['name'] = $row['name'];
		$troops_array[$i]['maxLevel'] = $row['maxLevel'];
		$troops_array[$i]['1'] = $row['1'];
		$troops_array[$i]['2'] = $row['2'];
		$troops_array[$i]['3'] = $row['3'];
		$troops_array[$i]['4'] = $row['4'];
		$troops_array[$i]['5'] = $row['5'];
		$troops_array[$i]['6'] = $row['6'];
		$troops_array[$i]['7'] = $row['7'];
		$troops_array[$i]['8'] = $row['8'];
		$i++;
	}

	$hero_sql = "SELECT amh.`hero_name` AS 'name', amh.`hero_maxLevel` AS 'maxLevel', ";
	$hero_sql .= "(SELECT ww1.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww1 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww1.`ww_name` AND ";
	$hero_sql .= "    ww1.`ww_level` = 1) AS '1', ";
	$hero_sql .= "(SELECT ww2.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww2 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww2.`ww_name` AND ";
	$hero_sql .= "    ww2.`ww_level` = 2) AS '2', ";
	$hero_sql .= "(SELECT ww3.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww3 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww3.`ww_name` AND ";
	$hero_sql .= "    ww3.`ww_level` = 3) AS '3', ";
	$hero_sql .= "(SELECT ww4.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww4 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww4.`ww_name` AND ";
	$hero_sql .= "    ww4.`ww_level` = 4) AS '4', ";
	$hero_sql .= "(SELECT ww5.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww5 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww5.`ww_name` AND ";
	$hero_sql .= "    ww5.`ww_level` = 5) AS '5', ";
	$hero_sql .= "(SELECT ww6.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww6 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww6.`ww_name` AND ";
	$hero_sql .= "    ww6.`ww_level` = 6) AS '6', ";
	$hero_sql .= "(SELECT ww7.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww7 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww7.`ww_name` AND ";
	$hero_sql .= "    ww7.`ww_level` = 7) AS '7', ";
	$hero_sql .= "(SELECT ww8.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww8 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww8.`ww_name` AND ";
	$hero_sql .= "    ww8.`ww_level` = 8) AS '8', ";
	$hero_sql .= "(SELECT ww9.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww9 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww9.`ww_name` AND ";
	$hero_sql .= "    ww9.`ww_level` = 9) AS '9', ";
	$hero_sql .= "(SELECT ww10.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww10 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww10.`ww_name` AND ";
	$hero_sql .= "    ww10.`ww_level` = 10) AS '10', ";
	$hero_sql .= "(SELECT ww11.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww11 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww11.`ww_name` AND ";
	$hero_sql .= "    ww11.`ww_level` = 11) AS '11', ";
	$hero_sql .= "(SELECT ww12.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww12 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww12.`ww_name` AND ";
	$hero_sql .= "    ww12.`ww_level` = 12) AS '12', ";
	$hero_sql .= "(SELECT ww13.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww13 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww13.`ww_name` AND ";
	$hero_sql .= "    ww13.`ww_level` = 13) AS '13', ";
	$hero_sql .= "(SELECT ww14.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww14 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww14.`ww_name` AND ";
	$hero_sql .= "    ww14.`ww_level` = 14) AS '14', ";
	$hero_sql .= "(SELECT ww15.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww15 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww15.`ww_name` AND ";
	$hero_sql .= "    ww15.`ww_level` = 15) AS '15', ";
	$hero_sql .= "(SELECT ww16.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww16 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww16.`ww_name` AND ";
	$hero_sql .= "    ww16.`ww_level` = 16) AS '16', ";
	$hero_sql .= "(SELECT ww17.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww17 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww17.`ww_name` AND ";
	$hero_sql .= "    ww17.`ww_level` = 17) AS '17', ";
	$hero_sql .= "(SELECT ww18.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww18 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww18.`ww_name` AND ";
	$hero_sql .= "    ww18.`ww_level` = 18) AS '18', ";
	$hero_sql .= "(SELECT ww19.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww19 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww19.`ww_name` AND ";
	$hero_sql .= "    ww19.`ww_level` = 19) AS '19', ";
	$hero_sql .= "(SELECT ww20.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww20 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww20.`ww_name` AND ";
	$hero_sql .= "    ww20.`ww_level` = 20) AS '20', ";
	$hero_sql .= "(SELECT ww21.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww21 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww21.`ww_name` AND ";
	$hero_sql .= "    ww21.`ww_level` = 21) AS '21', ";
	$hero_sql .= "(SELECT ww22.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww22 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww22.`ww_name` AND ";
	$hero_sql .= "    ww22.`ww_level` = 22) AS '22', ";
	$hero_sql .= "(SELECT ww23.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww23 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww23.`ww_name` AND ";
	$hero_sql .= "    ww23.`ww_level` = 23) AS '23', ";
	$hero_sql .= "(SELECT ww24.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww24 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww24.`ww_name` AND ";
	$hero_sql .= "    ww24.`ww_level` = 24) AS '24', ";
	$hero_sql .= "(SELECT ww25.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww25 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww25.`ww_name` AND ";
	$hero_sql .= "    ww25.`ww_level` = 25) AS '25', ";
	$hero_sql .= "(SELECT ww26.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww26 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww26.`ww_name` AND ";
	$hero_sql .= "    ww26.`ww_level` = 26) AS '26', ";
	$hero_sql .= "(SELECT ww27.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww27 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww27.`ww_name` AND ";
	$hero_sql .= "    ww27.`ww_level` = 27) AS '27', ";
	$hero_sql .= "(SELECT ww28.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww28 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww28.`ww_name` AND ";
	$hero_sql .= "    ww28.`ww_level` = 28) AS '28', ";
	$hero_sql .= "(SELECT ww29.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww29 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww29.`ww_name` AND ";
	$hero_sql .= "    ww29.`ww_level` = 29) AS '29', ";
	$hero_sql .= "(SELECT ww30.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww30 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww30.`ww_name` AND ";
	$hero_sql .= "    ww30.`ww_level` = 30) AS '30', ";
	$hero_sql .= "(SELECT ww31.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww31 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww31.`ww_name` AND ";
	$hero_sql .= "    ww31.`ww_level` = 31) AS '31', ";
	$hero_sql .= "(SELECT ww32.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww32 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww32.`ww_name` AND ";
	$hero_sql .= "    ww32.`ww_level` = 32) AS '32', ";
	$hero_sql .= "(SELECT ww33.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww33 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww33.`ww_name` AND ";
	$hero_sql .= "    ww33.`ww_level` = 33) AS '33', ";
	$hero_sql .= "(SELECT ww34.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww34 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww34.`ww_name` AND ";
	$hero_sql .= "    ww34.`ww_level` = 34) AS '34', ";
	$hero_sql .= "(SELECT ww35.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww35 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww35.`ww_name` AND ";
	$hero_sql .= "    ww35.`ww_level` = 35) AS '35', ";
	$hero_sql .= "(SELECT ww36.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww36 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww36.`ww_name` AND ";
	$hero_sql .= "    ww36.`ww_level` = 36) AS '36', ";
	$hero_sql .= "(SELECT ww37.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww37 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww37.`ww_name` AND ";
	$hero_sql .= "    ww37.`ww_level` = 37) AS '37', ";
	$hero_sql .= "(SELECT ww38.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww38 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww38.`ww_name` AND ";
	$hero_sql .= "    ww38.`ww_level` = 38) AS '38', ";
	$hero_sql .= "(SELECT ww39.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww39 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww39.`ww_name` AND ";
	$hero_sql .= "    ww39.`ww_level` = 39) AS '39', ";
	$hero_sql .= "(SELECT ww40.`ww_weight` ";
	$hero_sql .= "  FROM `API_WarWeight` AS ww40 ";
	$hero_sql .= "  WHERE amh.`hero_name` LIKE ww40.`ww_name` AND ";
	$hero_sql .= "    ww40.`ww_level` = 40) AS '40' ";
	$hero_sql .= "FROM `API_Mem_Heros` AS amh ";
	$hero_sql .= "ORDER BY `hero_name` ASC;";
	$hero_result = $conn->query($hero_sql);
	
	$i = 0;
	while($row = $hero_result->fetch_assoc()) {
		$heros_array[$i]['name'] = $row['name'];
		$heros_array[$i]['maxLevel'] = $row['maxLevel'];
		$heros_array[$i]['1'] = $row['1'];
		$heros_array[$i]['2'] = $row['2'];
		$heros_array[$i]['3'] = $row['3'];
		$heros_array[$i]['4'] = $row['4'];
		$heros_array[$i]['5'] = $row['5'];
		$heros_array[$i]['6'] = $row['6'];
		$heros_array[$i]['7'] = $row['7'];
		$heros_array[$i]['8'] = $row['8'];
		$heros_array[$i]['9'] = $row['9'];
		$heros_array[$i]['10'] = $row['10'];
		$heros_array[$i]['11'] = $row['11'];
		$heros_array[$i]['12'] = $row['12'];
		$heros_array[$i]['13'] = $row['13'];
		$heros_array[$i]['14'] = $row['14'];
		$heros_array[$i]['15'] = $row['15'];
		$heros_array[$i]['16'] = $row['16'];
		$heros_array[$i]['17'] = $row['17'];
		$heros_array[$i]['18'] = $row['18'];
		$heros_array[$i]['19'] = $row['19'];
		$heros_array[$i]['20'] = $row['20'];
		$heros_array[$i]['21'] = $row['21'];
		$heros_array[$i]['22'] = $row['22'];
		$heros_array[$i]['23'] = $row['23'];
		$heros_array[$i]['24'] = $row['24'];
		$heros_array[$i]['25'] = $row['25'];
		$heros_array[$i]['26'] = $row['26'];
		$heros_array[$i]['27'] = $row['27'];
		$heros_array[$i]['28'] = $row['28'];
		$heros_array[$i]['29'] = $row['29'];
		$heros_array[$i]['30'] = $row['30'];
		$heros_array[$i]['31'] = $row['31'];
		$heros_array[$i]['32'] = $row['32'];
		$heros_array[$i]['33'] = $row['33'];
		$heros_array[$i]['34'] = $row['34'];
		$heros_array[$i]['35'] = $row['35'];
		$heros_array[$i]['36'] = $row['36'];
		$heros_array[$i]['37'] = $row['37'];
		$heros_array[$i]['38'] = $row['38'];
		$heros_array[$i]['39'] = $row['39'];
		$heros_array[$i]['40'] = $row['40'];
		$i++;
	}

	$spell_sql = "SELECT amt.`spells_name` AS 'name', amt.`spells_maxLevel` AS 'maxLevel', ";
	$spell_sql .= "(SELECT ww1.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww1 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww1.`ww_name` AND ";
	$spell_sql .= "    ww1.`ww_level` = 1) AS '1', ";
	$spell_sql .= "(SELECT ww2.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww2 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww2.`ww_name` AND ";
	$spell_sql .= "     ww2.`ww_level` = 2) AS '2', ";
	$spell_sql .= "(SELECT ww3.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww3 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww3.`ww_name` AND ";
	$spell_sql .= "     ww3.`ww_level` = 3) AS '3', ";
	$spell_sql .= "(SELECT ww4.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww4 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww4.`ww_name` AND ";
	$spell_sql .= "     ww4.`ww_level` = 4) AS '4', ";
	$spell_sql .= "(SELECT ww5.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww5 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww5.`ww_name` AND ";
	$spell_sql .= "     ww5.`ww_level` = 5) AS '5', ";
	$spell_sql .= "(SELECT ww6.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww6 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww6.`ww_name` AND ";
	$spell_sql .= "   ww6.`ww_level` = 6) AS '6', ";
	$spell_sql .= "(SELECT ww7.`ww_weight` ";
	$spell_sql .= "   FROM `API_WarWeight` AS ww7 ";
	$spell_sql .= "   WHERE amt.`spells_name` LIKE ww7.`ww_name` AND ";
	$spell_sql .= "     ww7.`ww_level` = 7) AS '7' ";
	$spell_sql .= "FROM `API_Mem_Spells` AS amt ";
	$spell_sql .= "ORDER BY `spells_name` ASC;";
	$spell_result = $conn->query($spell_sql);
	
	$i = 0;
	while($row = $spell_result->fetch_assoc()) {
		$spells_array[$i]['name'] = $row['name'];
		$spells_array[$i]['maxLevel'] = $row['maxLevel'];
		$spells_array[$i]['1'] = $row['1'];
		$spells_array[$i]['2'] = $row['2'];
		$spells_array[$i]['3'] = $row['3'];
		$spells_array[$i]['4'] = $row['4'];
		$spells_array[$i]['5'] = $row['5'];
		$spells_array[$i]['6'] = $row['6'];
		$spells_array[$i]['7'] = $row['7'];
		$spells_array[$i]['8'] = $row['8'];
		$i++;
	}


?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title>War Weight</title>
</head>
<body>
	<h1 align="center">War Weight</h1>
	<div id="accordion">
		<h3 align="center">Troops</h3>
		<div id="Troops">
			<center>
				<table width="100%" class="sortable">
					<tr>
						<td align="center"><b>Name</b></td>
						<td align="center"><b>1</b></td>
						<td align="center"><b>2</b></td>
						<td align="center"><b>3</b></td>
						<td align="center"><b>4</b></td>
						<td align="center"><b>5</b></td>
						<td align="center"><b>6</b></td>
						<td align="center"><b>7</b></td>
						<td align="center"><b>8</b></td>
						<td align="center"><b>Max<br>Level</b></td>
					</tr>
					<?php foreach ( $troops_array as $key => $value ) { ?>
						<tr>
							<td><?php echo $troops_array[$key]['name'] ?></td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 1 ) {
										if ( $troops_array[$key]['1'] ) {
											echo $troops_array[$key]['1'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 2 ) {
										if ( $troops_array[$key]['2'] ) {
											echo $troops_array[$key]['2'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 3 ) {
										if ( $troops_array[$key]['3'] ) {
											echo $troops_array[$key]['3'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 4 ) {
										if ( $troops_array[$key]['4'] ) {
											echo $troops_array[$key]['4'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 5 ) {
										if ( $troops_array[$key]['5'] ) {
											echo $troops_array[$key]['5'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 6 ) {
										if ( $troops_array[$key]['6'] ) {
											echo $troops_array[$key]['6'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 7 ) {
										if ( $troops_array[$key]['7'] ) {
											echo $troops_array[$key]['7'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $troops_array[$key]['maxLevel'] >= 8 ) {
										if ( $troops_array[$key]['8'] ) {
											echo $troops_array[$key]['8'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td><?php echo $troops_array[$key]['maxLevel'] ?></td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Heros</h3>
		<div id="Heros">
			<center>
				<table width="100%" class="sortable">
					<tr>
						<td align="center"><b>Name</b></td>
						<td align="center"><b>1</b></td>
						<td align="center"><b>2</b></td>
						<td align="center"><b>3</b></td>
						<td align="center"><b>4</b></td>
						<td align="center"><b>5</b></td>
						<td align="center"><b>6</b></td>
						<td align="center"><b>7</b></td>
						<td align="center"><b>8</b></td>
						<td align="center"><b>9</b></td>
						<td align="center"><b>10</b></td>
						<td align="center"><b>Max<br>Level</b></td>
					</tr>
					<?php foreach ( $heros_array as $key => $value ) { ?>
						<tr>
							<td><?php echo $heros_array[$key]['name'] ?> 1-10</td>
							<?php for ( $i = 1; $i <= 10; $i++ ) { ?>
								<td><?php echo $heros_array[$key][$i] ?></td>							
							<?php } ?>
							<td><?php echo $heros_array[$key]['maxLevel'] ?></td>
						</tr>
						<tr>
							<td><?php echo $heros_array[$key]['name'] ?> 11-20</td>
							<?php for ( $i = 11; $i <= 20; $i++ ) { ?>
								<td><?php echo $heros_array[$key][$i] ?></td>							
							<?php } ?>
							<td></td>
						</tr>
						<tr>
							<td><?php echo $heros_array[$key]['name'] ?> 21-30</td>
							<?php for ( $i = 21; $i <= 30; $i++ ) { ?>
								<td><?php echo $heros_array[$key][$i] ?></td>							
							<?php } ?>
							<td></td>
						</tr>
						<tr>
							<td><?php echo $heros_array[$key]['name'] ?> 31-40</td>
							<?php for ( $i = 31; $i <= 40; $i++ ) { ?>
								<td><?php echo $heros_array[$key][$i] ?></td>							
							<?php } ?>
							<td></td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
		<h3 align="center">Spells</h3>
		<div id="Spells">
			<center>
				<table width="100%" class="sortable">
					<tr>
						<td align="center"><b>Name</b></td>
						<td align="center"><b>1</b></td>
						<td align="center"><b>2</b></td>
						<td align="center"><b>3</b></td>
						<td align="center"><b>4</b></td>
						<td align="center"><b>5</b></td>
						<td align="center"><b>6</b></td>
						<td align="center"><b>7</b></td>
						<td align="center"><b>8</b></td>
						<td align="center"><b>Max<br>Level</b></td>
					</tr>
					<?php foreach ( $spells_array as $key => $value ) { ?>
						<tr>
							<td><?php echo $spells_array[$key]['name'] ?></td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 1 ) {
										if ( $spells_array[$key]['1'] ) {
											echo $spells_array[$key]['1'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 2 ) {
										if ( $spells_array[$key]['2'] ) {
											echo $spells_array[$key]['2'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 3 ) {
										if ( $spells_array[$key]['3'] ) {
											echo $spells_array[$key]['3'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 4 ) {
										if ( $spells_array[$key]['4'] ) {
											echo $spells_array[$key]['4'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 5 ) {
										if ( $spells_array[$key]['5'] ) {
											echo $spells_array[$key]['5'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 6 ) {
										if ( $spells_array[$key]['6'] ) {
											echo $spells_array[$key]['6'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 7 ) {
										if ( $spells_array[$key]['7'] ) {
											echo $spells_array[$key]['7'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td>
								<?php
									if ( $spells_array[$key]['maxLevel'] >= 8 ) {
										if ( $spells_array[$key]['8'] ) {
											echo $spells_array[$key]['8'];
										} else {
											echo "<font color=\"red\">0</font>";
										}
									} else {
										echo "--";
									}
								?>
							</td>
							<td><?php echo $spells_array[$key]['maxLevel'] ?></td>
						</tr>
					<?php } ?>
				</table>
			</center>
		</div>
	</div>






<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
<!-- JQuery Script -->
<link rel="stylesheet" href="/images/jumi_code/jquery-ui.theme.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#accordion,#accordion_Home_Player" ).accordion({
			heightStyle: "content"
		});
	});
</script>

</body>
