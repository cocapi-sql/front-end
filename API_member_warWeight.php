<?php
	// SQL Variables
	$servername = "localhost";
	$username = "thebl962_jumi";
	$password = "=4%Q}PJTeV8y";
	$dbname = "thebl962_jumi";

	// Variables
	$member_selected = "#R280P220";
	$member_array = array();
	$defense_array = array();
	$heros_array = array();
	$troops_array = array();
	$spells_array = array();
	
	// setup and zero war weight
	$warWeight = array();
	$warWeight['defense'] = 0;
	$warWeight['bldg_defense'] = 0;
	$warWeight['wall_defense'] = 0;
	$warWeight['traps'] = 0;
	$warWeight['misc'] = 0;
	$warWeight['troops'] = 0;
	$warWeight['elixir_troops'] = 0;
	$warWeight['de_troops'] = 0;
	$warWeight['heros'] = 0;
	$warWeight['spells'] = 0;
	$warWeight['total'] = 0;
	$warWeight['unadjusted'] = 0;
	$warWeight['adjusted'] = 0;
	$warWeight['penalty'] = 0;
	$warWeight['flag'] = 1;
	
	// Check and create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	// Get member or use default set to BlackList
	if ( isset ( $_GET['member'] ) ) {
		$member_selected = '#' . $_GET['member'];
	}
			
	$member_sql = "SELECT * ";
	$member_sql .= " FROM `API_Member` AS am ";
	$member_sql .= " INNER JOIN `API_League` AS al ";
	$member_sql .= "  ON am.`member_league_id`=al.`league_id` ";
	$member_sql .= " INNER JOIN `API_Clan` AS ac ";
	$member_sql .= "  ON am.`member_clan_tag`=ac.`clan_tag` ";
	$member_sql .= " WHERE `member_tag` LIKE '" . $member_selected . "' ";
	$member_sql .= " ORDER BY `member_timestamp` DESC ";
	$member_sql .= " LIMIT 1;";
	$member_result = $conn->query($member_sql);
	
	while($member_row = $member_result->fetch_assoc()) {
		$member_array["member_tag"] = $member_row["member_tag"];
		$member_array["clan_tag"] = $member_row["clan_tag"];
		$member_array["league_id"] = $member_row["league_id"];
		$member_array["member_name"] = $member_row["member_name"];
		$member_array["member_timestamp"] = $member_row["member_timestamp"];
		$member_array["member_clanRank"] = $member_row["member_clanRank"];
		$member_array["member_previousClanRank"] = $member_row["member_previousClanRank"];
		$member_array["member_expLevel"] = $member_row["member_expLevel"];
		$member_array["member_role"] = $member_row["member_role"];
		$member_array["member_townHallLevel"] = $member_row["member_townHallLevel"];
		$member_array["member_trophies"] = $member_row["member_trophies"];
		$member_array["member_bestTrophies"] = $member_row["member_bestTrophies"];
		$member_array["member_warStars"] = $member_row["member_warStars"];
		$member_array["member_attackWins"] = $member_row["member_attackWins"];
		$member_array["member_defenseWins"] = $member_row["member_defenseWins"];
		$member_array["member_donations"] = $member_row["member_donations"];
		$member_array["member_total_donations"] = $member_row["member_total_donations"];
		$member_array["member_donationsReceived"] = $member_row["member_donationsReceived"];
		$member_array["member_total_donationsReceived"] = $member_row["member_total_donationsReceived"];
		$member_array["clan_name"] = $member_row["clan_name"];
		$member_array["clan_description"] = $member_row["clan_description"];
		$member_array["clan_badgeUrls_small"] = $member_row["clan_badgeUrls_small"];
		$member_array["clan_badgeUrls_medium"] = $member_row["clan_badgeUrls_medium"];
		$member_array["clan_badgeUrls_large"] = $member_row["clan_badgeUrls_large"];
		$member_array["league_name"] = $member_row["league_name"];
		$member_array["league_iconUrls_tiny"] = $member_row["league_iconUrls_tiny"];
		$member_array["league_iconUrls_small"] = $member_row["league_iconUrls_small"];
		$member_array["league_iconUrls_medium"] = $member_row["league_iconUrls_medium"];
		$member_array["get_member_id"] = str_replace ( '#', '', $member_row["member_tag"] );
	}	

	$member_defense_sql = "SELECT defToMem_defense_name, MAX(defToMem_timestamp) AS defToMem_timestamp, ";
	$member_defense_sql .= "   MAX(defToMem_pos1) AS defToMem_pos1, MAX(defToMem_pos2) AS defToMem_pos2, ";
	$member_defense_sql .= "   MAX(defToMem_pos3) AS defToMem_pos3, MAX(defToMem_pos4) AS defToMem_pos4, ";
	$member_defense_sql .= "   MAX(defToMem_pos5) AS defToMem_pos5, MAX(defToMem_pos6) AS defToMem_pos6, ";
	$member_defense_sql .= "   MAX(defToMem_pos7) AS defToMem_pos7, MAX(defToMem_pos8) AS defToMem_pos8, ";
	$member_defense_sql .= "   MAX(defToMem_pos9) AS defToMem_pos9, MAX(defToMem_pos10) AS defToMem_pos10, ";
	$member_defense_sql .= "   MAX(defToMem_pos11) AS defToMem_pos11, MAX(defToMem_pos12) AS defToMem_pos12, ";
	$member_defense_sql .= "   def_cnt_th" . $member_array["member_townHallLevel"] . ", def_maxLevel, ww_order ";
	$member_defense_sql .= " FROM `API_Mem_DefToMem` ";
	$member_defense_sql .= " INNER JOIN `API_Mem_Defense` ";
	$member_defense_sql .= "   ON `defToMem_defense_name` LIKE `def_name` ";
	$member_defense_sql .= " INNER JOIN `API_WarWeight` ";
	$member_defense_sql .= "   ON `defToMem_defense_name` LIKE `ww_name` ";
	$member_defense_sql .= " WHERE  `defToMem_member_tag` LIKE  '" . $member_selected . "' ";
	$member_defense_sql .= " GROUP BY `defToMem_defense_name` ";
	$member_defense_sql .= " ORDER BY `defToMem_timestamp` ASC;";
	$member_defense_result = $conn->query($member_defense_sql);

	while($member_defense_row = $member_defense_result->fetch_assoc()) {
		$temp_order= $member_defense_row["ww_order"];
		$temp_thCnt = $member_defense_row["def_cnt_th" . $member_array["member_townHallLevel"]];
		
		$defense_array[$temp_order]["name"] = $member_defense_row["defToMem_defense_name"];
		$defense_array[$temp_order]["timestamp"] = $member_defense_row["defToMem_timestamp"];
		$defense_array[$temp_order]["maxLevel"] = $member_defense_row["def_maxLevel"];
		$defense_array[$temp_order]["thCnt"] = $temp_thCnt;
		
		if ( $defense_array[$temp_order]["timestamp"] > $defense_array[0]["timestamp"] ) {
			$defense_array[0]["timestamp"] = $defense_array[$temp_order]["timestamp"];
		}
		
		$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
		$member_warWeight_sql .= " FROM `API_WarWeight` ";
		$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $defense_array[$temp_order]["name"] . "' ";
		$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
		$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
		$member_warWeight_result = $conn->query($member_warWeight_sql);
		
		$temp_ww = array();
		$i = 1;
		while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
			$temp_ww[$i] = $member_warWeight_row["ww_weight"];
			$defense_array[$temp_order]["maxTHLevel"] = $member_warWeight_row["ww_level"];
			$i++;
		}
		
		// if walls
		if ( $temp_order == 20 ) {
			for ( $i = 1; $i <= 12; $i++ ) {
				 $temp_cnt = $member_defense_row["defToMem_pos" . $i];
				 $defense_array[$temp_order]["cnt"][$i] = $temp_cnt;
				 $defense_array[$temp_order]["warWeight"] = $defense_array[$temp_order]["warWeight"] + ( $temp_ww[$i] * $temp_cnt );
			}
			$warWeight['wall_defense'] = $warWeight['wall_defense'] + $defense_array[$temp_order]["warWeight"];
		} else {
			for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
				 $temp_level = $member_defense_row["defToMem_pos" . $i];
				 $defense_array[$temp_order]["level"][$i] = $temp_level;
				 $defense_array[$temp_order]["warWeight"] = $defense_array[$temp_order]["warWeight"] + $temp_ww[$temp_level];
			}
			$warWeight['bldg_defense'] = $warWeight['bldg_defense'] + $defense_array[$temp_order]["warWeight"];
		}
		$warWeight['defense'] = $warWeight['defense'] + $defense_array[$temp_order]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $defense_array[$temp_order]["warWeight"];		
	}
	
	if ( empty ( $defense_array[1]["name"] )) {
		$warWeight['flag'] = 0;
	} 

	$member_traps_sql = "SELECT trapToMem_trap_name, MAX(trapToMem_timestamp) AS trapToMem_timestamp, ";
	$member_traps_sql .= "   MAX(trapToMem_pos1) AS trapToMem_pos1, MAX(trapToMem_pos2) AS trapToMem_pos2, ";
	$member_traps_sql .= "   MAX(trapToMem_pos3) AS trapToMem_pos3, MAX(trapToMem_pos4) AS trapToMem_pos4, ";
	$member_traps_sql .= "   MAX(trapToMem_pos5) AS trapToMem_pos5, MAX(trapToMem_pos6) AS trapToMem_pos6, ";
	$member_traps_sql .= "   trap_cnt_th" . $member_array["member_townHallLevel"] . ", trap_maxLevel, ww_order ";
	$member_traps_sql .= " FROM `API_Mem_TrapToMem` ";
	$member_traps_sql .= " INNER JOIN `API_Mem_Traps` ";
	$member_traps_sql .= "   ON `trapToMem_trap_name` LIKE `trap_name` ";
	$member_traps_sql .= " INNER JOIN `API_WarWeight` ";
	$member_traps_sql .= "   ON `trapToMem_trap_name` LIKE `ww_name` ";
	$member_traps_sql .= " WHERE  `trapToMem_member_tag` LIKE  '" . $member_selected . "' ";
	$member_traps_sql .= " GROUP BY `trapToMem_trap_name` ";
	$member_traps_sql .= " ORDER BY `ww_order` ASC;";
	$member_traps_result = $conn->query($member_traps_sql);

	while($member_traps_row = $member_traps_result->fetch_assoc()) {
		$temp_order= $member_traps_row["ww_order"];
		$temp_thCnt = $member_traps_row["trap_cnt_th" . $member_array["member_townHallLevel"]];
		
		$traps_array[$temp_order]["name"] = $member_traps_row["trapToMem_trap_name"];
		$traps_array[$temp_order]["timestamp"] = $member_traps_row["trapToMem_timestamp"];
		$traps_array[$temp_order]["maxLevel"] = $member_traps_row["trap_maxLevel"];
		$traps_array[$temp_order]["thCnt"] = $temp_thCnt;
		
		if ( $traps_array[$temp_order]["timestamp"] > $traps_array[0]["timestamp"] ) {
			$traps_array[0]["timestamp"] = $traps_array[$temp_order]["timestamp"];
		}
	
		$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
		$member_warWeight_sql .= " FROM `API_WarWeight` ";
		$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $traps_array[$temp_order]["name"] . "' ";
		$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
		$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
		$member_warWeight_result = $conn->query($member_warWeight_sql);
		
		$temp_ww = array();
		$i = 1;
		while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
			$temp_ww[$i] = $member_warWeight_row["ww_weight"];
			$traps_array[$temp_order]["maxTHLevel"] = $member_warWeight_row["ww_level"];
			$i++;
		}
		
		for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
			 $temp_level = $member_traps_row["trapToMem_pos" . $i];
			 $traps_array[$temp_order]["level"][$i] = $temp_level;
			 $traps_array[$temp_order]["warWeight"] = $traps_array[$temp_order]["warWeight"] + $temp_ww[$temp_level];
		}

		$warWeight['traps'] = $warWeight['traps'] + $traps_array[$temp_order]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $traps_array[$temp_order]["warWeight"];		
	}
	
	$member_misc_sql = "SELECT miscToMem_misc_name, MAX(miscToMem_timestamp) AS miscToMem_timestamp, ";
	$member_misc_sql .= "   MAX(miscToMem_pos1) AS miscToMem_pos1, MAX(miscToMem_pos2) AS miscToMem_pos2, ";
	$member_misc_sql .= "   MAX(miscToMem_pos3) AS miscToMem_pos3, MAX(miscToMem_pos4) AS miscToMem_pos4, ";
	$member_misc_sql .= "   MAX(miscToMem_pos5) AS miscToMem_pos5, MAX(miscToMem_pos6) AS miscToMem_pos6, ";
	$member_misc_sql .= "   MAX(miscToMem_pos7) AS miscToMem_pos7, ";
	$member_misc_sql .= "   misc_cnt_th" . $member_array["member_townHallLevel"] . ", misc_maxLevel, ww_order ";
	$member_misc_sql .= " FROM `API_Mem_MiscToMem` ";
	$member_misc_sql .= " INNER JOIN `API_Mem_Misc` ";
	$member_misc_sql .= "   ON `miscToMem_misc_name` LIKE `misc_name` ";
	$member_misc_sql .= " INNER JOIN `API_WarWeight` ";
	$member_misc_sql .= "   ON `miscToMem_misc_name` LIKE `ww_name` ";
	$member_misc_sql .= " WHERE  `miscToMem_member_tag` LIKE  '" . $member_selected . "' ";
	$member_misc_sql .= " GROUP BY `miscToMem_misc_name` ";
	$member_misc_sql .= " ORDER BY `ww_order` ASC;";
	$member_misc_result = $conn->query($member_misc_sql);

	while($member_misc_row = $member_misc_result->fetch_assoc()) {
		$temp_order= $member_misc_row["ww_order"];
		$temp_thCnt = $member_misc_row["misc_cnt_th" . $member_array["member_townHallLevel"]];
		
		$misc_array[$temp_order]["name"] = $member_misc_row["miscToMem_misc_name"];
		$misc_array[$temp_order]["timestamp"] = $member_misc_row["miscToMem_timestamp"];
		$misc_array[$temp_order]["maxLevel"] = $member_misc_row["misc_maxLevel"];
		$misc_array[$temp_order]["thCnt"] = $temp_thCnt;
		
		if ( $misc_array[$temp_order]["timestamp"] > $misc_array[0]["timestamp"] ) {
			$misc_array[0]["timestamp"] = $misc_array[$temp_order]["timestamp"];
		}
	
		$member_warWeight_sql = "SELECT `ww_level`, `ww_weight` ";
		$member_warWeight_sql .= " FROM `API_WarWeight` ";
		$member_warWeight_sql .= " WHERE `ww_name` LIKE '" . $misc_array[$temp_order]["name"] . "' ";
		$member_warWeight_sql .= "  AND `ww_thLevel` <= " . $member_array["member_townHallLevel"] . " ";
		$member_warWeight_sql .= " ORDER BY `ww_level` ASC;";
		$member_warWeight_result = $conn->query($member_warWeight_sql);
		
		$temp_ww = array();
		$i = 1;
		while($member_warWeight_row = $member_warWeight_result->fetch_assoc()) {
			$temp_ww[$i] = $member_warWeight_row["ww_weight"];
			$misc_array[$temp_order]["maxTHLevel"] = $member_warWeight_row["ww_level"];
			$i++;
		}
		
		for ( $i = 1; $i <= $temp_thCnt; $i++ ) {
			 $temp_level = $member_misc_row["miscToMem_pos" . $i];
			 $misc_array[$temp_order]["level"][$i] = $temp_level;
			 $misc_array[$temp_order]["warWeight"] = $misc_array[$temp_order]["warWeight"] + $temp_ww[$temp_level];
		}

		$warWeight['misc'] = $warWeight['misc'] + $misc_array[$temp_order]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $misc_array[$temp_order]["warWeight"];		
	}
	
	$member_heros_sql = "SELECT `heroToMem_heros_name`, `heroToMem_timestamp`, ";
	$member_heros_sql .= "  MAX(`heroToMem_level`) AS heroToMem_level, ";
	$member_heros_sql .= "  MAX(wwhe.`ww_weight`) AS ww_weight, wwhe.`ww_order`, ";
	$member_heros_sql .= "  (SELECT wwthhigh.`ww_level` ";
	$member_heros_sql .= "     FROM API_WarWeight AS wwthhigh ";
	$member_heros_sql .= "     WHERE `heroToMem_heros_name` LIKE wwthhigh.`ww_name` ";
	$member_heros_sql .= "     AND  `ww_thLevel` = " . $member_array["member_townHallLevel"] . " ";
	$member_heros_sql .= "     Order BY wwthhigh.`ww_level` DESC ";
	$member_heros_sql .= "     LIMIT 1) AS ww_maxTHLevel, ";
	$member_heros_sql .= "  (SELECT wwhigh.`ww_level` ";
	$member_heros_sql .= "     FROM API_WarWeight AS wwhigh ";
	$member_heros_sql .= "     WHERE `heroToMem_heros_name` LIKE wwhigh.`ww_name` ";
	$member_heros_sql .= "     Order BY wwhigh.`ww_level` DESC ";
	$member_heros_sql .= "     LIMIT 1) AS ww_maxLevel ";
	$member_heros_sql .= " FROM  `API_Mem_HerosToMem` ";
	$member_heros_sql .= " INNER JOIN `API_WarWeight` AS wwhe ";
	$member_heros_sql .= "   ON  `heroToMem_heros_name` LIKE wwhe.`ww_name` ";
	$member_heros_sql .= "  AND `heroToMem_level` LIKE wwhe.`ww_level` ";
	$member_heros_sql .= " WHERE  `heroToMem_member_tag` LIKE  '" . $member_selected . "' ";
	$member_heros_sql .= " GROUP BY `heroToMem_heros_name` ";
	$member_heros_sql .= " ORDER BY wwhe.`ww_order` ASC;";
	$member_heros_result = $conn->query($member_heros_sql);
	
	while($member_heros_row = $member_heros_result->fetch_assoc()) {
		$i = $member_heros_row["ww_order"];
		$heros_array[$i]["name"] = $member_heros_row["heroToMem_heros_name"];
		$heros_array[$i]["timestamp"] = $member_heros_row["heroToMem_timestamp"];
		$heros_array[$i]["level"] = $member_heros_row["heroToMem_level"];
		$heros_array[$i]["maxTHLevel"] = $member_heros_row["ww_maxTHLevel"];
		$heros_array[$i]["maxLevel"] = $member_heros_row["ww_maxLevel"];
		$heros_array[$i]["warWeight"] = $member_heros_row["ww_weight"];
		$warWeight['heros'] = $warWeight['heros'] + $heros_array[$i]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $heros_array[$i]["warWeight"];
	}
	
	$member_troops_sql = "SELECT `troopsToMem_troops_name`, `troopsToMem_timestamp`, ";
	$member_troops_sql .= "   MAX(`troopsToMem_level`) AS troopsToMem_level, ";
	$member_troops_sql .= "   MAX(wwtr.`ww_weight`) AS ww_weight, wwtr.`ww_order`, ";
	$member_troops_sql .= "   (SELECT wwhigh.`ww_level`  ";
	$member_troops_sql .= "     FROM API_WarWeight AS wwhigh ";
	$member_troops_sql .= "     WHERE `troopsToMem_troops_name` LIKE wwhigh.`ww_name` ";
	$member_troops_sql .= "     Order BY wwhigh.`ww_level` DESC ";
	$member_troops_sql .= "     LIMIT 1) AS troops_maxLevel ";
	$member_troops_sql .= " FROM  `API_Mem_TroopsToMem` ";
	$member_troops_sql .= " INNER JOIN `API_WarWeight` AS wwtr ";
	$member_troops_sql .= "   ON  `troopsToMem_troops_name` LIKE wwtr.`ww_name` ";
	$member_troops_sql .= "   AND `troopsToMem_level` LIKE wwtr.`ww_level` ";
	$member_troops_sql .= " WHERE  `troopsToMem_member_tag` LIKE  '" . $member_selected . "'";
	$member_troops_sql .= " GROUP BY `troopsToMem_troops_name` ";
	$member_troops_sql .= " ORDER BY wwtr.`ww_order` ASC;";
	$member_troops_result = $conn->query($member_troops_sql);
	
	while($member_troops_row = $member_troops_result->fetch_assoc()) {
		$i = $member_troops_row["ww_order"];
		$troops_array[$i]["name"] = $member_troops_row["troopsToMem_troops_name"];
		$troops_array[$i]["timestamp"] = $member_troops_row["troopsToMem_timestamp"];
		$troops_array[$i]["level"] = $member_troops_row["troopsToMem_level"];
		$troops_array[$i]["maxLevel"] = $member_troops_row["troops_maxLevel"];
		$troops_array[$i]["warWeight"] = $member_troops_row["ww_weight"];
		$troops_array[$i]["maxTHLevel"] = maxTHLevel ( $troops_array[$i]["name"], $member_array["member_townHallLevel"], $conn );

		if ( $i < 30 ) {
			$warWeight['elixir_troops'] = $warWeight['elixir_troops'] + $troops_array[$i]["warWeight"];
		} else {
			$warWeight['de_troops'] = $warWeight['de_troops'] + $troops_array[$i]["warWeight"];
		}
		$warWeight['troops'] = $warWeight['troops'] + $troops_array[$i]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $troops_array[$i]["warWeight"];
	}
		
	$member_spells_sql = "SELECT REPLACE (`spellsToMem_spells_name`, ' Spell', '') AS spellsToMem_spells_name, ";
	$member_spells_sql .= "  `spellsToMem_timestamp`, MAX(`spellsToMem_level`) AS spellsToMem_level, ";
	$member_spells_sql .= "  MAX(ww.`ww_weight`) AS ww_weight, ww.`ww_order`, ";
	$member_spells_sql .= "  (SELECT wwhigh.`ww_level` ";
	$member_spells_sql .= "     FROM API_WarWeight AS wwhigh ";
	$member_spells_sql .= "     WHERE REPLACE (`spellsToMem_spells_name`, ' Spell', '') LIKE wwhigh.`ww_name` ";
	$member_spells_sql .= "     Order BY wwhigh.`ww_level` DESC ";
	$member_spells_sql .= "     LIMIT 1) AS ww_maxLevel ";
	$member_spells_sql .= " FROM  `API_Mem_SpellsToMem` ";
	$member_spells_sql .= " INNER JOIN `API_WarWeight` AS ww ";
	$member_spells_sql .= "   ON  REPLACE (`spellsToMem_spells_name`, ' Spell', '') LIKE ww.`ww_name` ";
	$member_spells_sql .= "   AND `spellsToMem_level` LIKE ww.`ww_level` ";
	$member_spells_sql .= " WHERE  `spellsToMem_member_tag` LIKE  '" . $member_selected . "' ";
	$member_spells_sql .= " GROUP BY `spellsToMem_spells_name` ";
	$member_spells_sql .= " ORDER BY ww.`ww_order` ASC;";
	$member_spells_result = $conn->query($member_spells_sql);
	
	while($member_spells_row = $member_spells_result->fetch_assoc()) {
		$i = $member_spells_row["ww_order"];
		$spells_array[$i]["name"] = $member_spells_row["spellsToMem_spells_name"];
		$spells_array[$i]["timestamp"] = $member_spells_row["spellsToMem_timestamp"];
		$spells_array[$i]["level"] = $member_spells_row["spellsToMem_level"];
		$spells_array[$i]["maxLevel"] = $member_spells_row["ww_maxLevel"];
		$spells_array[$i]["warWeight"] = $member_spells_row["ww_weight"];
		$spells_array[$i]["maxTHLevel"] = maxTHLevel ( $spells_array[$i]["name"], $member_array["member_townHallLevel"], $conn );

		$warWeight['spells'] = $warWeight['spells'] + $spells_array[$i]["warWeight"];
		$warWeight['total'] = $warWeight['total'] + $spells_array[$i]["warWeight"];
	}
	
	if ( $warWeight['flag'] == 1 ) {
		$warWeight['unadjusted'] = ( floor ( $warWeight['total'] / 2000 )) * 2000;
	
		if ( $member_array["member_townHallLevel"] == 1 ) {
			$warWeight['median'] = 0;
			$warWeight['priorTH'] = 0;
		} elseif ( $member_array["member_townHallLevel"] == 2 ) {
			$warWeight['median'] = 1000;
			$warWeight['priorTH'] = 0;
		} elseif ( $member_array["member_townHallLevel"] == 3 ) {
			$warWeight['median'] = 4000;
			$warWeight['priorTH'] = 2000;
		} elseif ( $member_array["member_townHallLevel"] == 4 ) {
			$warWeight['median'] = 8000;
			$warWeight['priorTH'] = 6000;
		} elseif ( $member_array["member_townHallLevel"] == 5 ) {
			$warWeight['median'] = 14000;
			$warWeight['priorTH'] = 10000;
		} elseif ( $member_array["member_townHallLevel"] == 6 ) {
			$warWeight['median'] = 22000;
			$warWeight['priorTH'] = 18000;
		} elseif ( $member_array["member_townHallLevel"] == 7 ) {
			$warWeight['median'] = 33000;
			$warWeight['priorTH'] = 26000;
		} elseif ( $member_array["member_townHallLevel"] == 8 ) {
			$warWeight['median'] = 51000;
			$warWeight['priorTH'] = 40000;
		} elseif ( $member_array["member_townHallLevel"] == 9 ) {
			$warWeight['median'] = 74000;
			$warWeight['priorTH'] = 62000;
		} elseif ( $member_array["member_townHallLevel"] == 10 ) {
			$warWeight['median'] = 103000;
			$warWeight['priorTH'] = 86000;
		} elseif ( $member_array["member_townHallLevel"] == 11 ) {
			$warWeight['median'] = 135000;
			$warWeight['priorTH'] = 120000;
		} else {
			echo "ERROR: Unknown Town Hall level: " . $member_array["member_townHallLevel"] . "<br>";
		}
	
		if (( $warWeight['unadjusted'] + 10000 < $warWeight['median'] )&&( $warWeight['total'] - 6 < $warWeight['priorTH'] ))  {
			$warWeight['adjusted'] = $warWeight['median'];
		} else {
			$warWeight['adjusted'] = $warWeight['unadjusted'];
		}
	
		$warWeight['penalty'] = $warWeight['adjusted'] - $warWeight['unadjusted'];
	
		$member_ww_sql = "SELECT  `memWW_unadjusted` ,  `memWW_adjusted` ,  `memWW_penalty` ";
		$member_ww_sql .= " FROM  `API_Mem_WarWeight` ";
		$member_ww_sql .= "WHERE  `memWW_member_tag` LIKE  '" . $member_selected . "' ";
		$member_ww_sql .= "ORDER BY  `memWW_timestamp` DESC ";
		$member_ww_sql .= "LIMIT 1;";
		$member_ww_result = $conn->query($member_ww_sql);

		while($member_ww_row = $member_ww_result->fetch_assoc()) {
			$warWeight['db_unadjusted'] = $member_ww_row['memWW_unadjusted'];
			$warWeight['db_adjusted'] = $member_ww_row['memWW_adjusted'];
			$warWeight['db_penalty'] = $member_ww_row['memWW_penalty'];
		}	
	
		if ( $warWeight['unadjusted'] != $warWeight['db_unadjusted'] || $warWeight['adjusted'] != $warWeight['db_adjusted'] || 
						$warWeight['penalty'] != $warWeight['db_penalty'] ) {
		
			$member_ww_insert_sql = "INSERT INTO `thebl962_jumi`.`API_Mem_WarWeight` ";
			$member_ww_insert_sql .= " (`memWW_member_tag`, `memWW_unadjusted`, `memWW_adjusted`, `memWW_penalty`) ";
			$member_ww_insert_sql .= " VALUES ('" . $member_selected . "', '" . $warWeight['unadjusted'] . "', ";
			$member_ww_insert_sql .= " '" . $warWeight['adjusted'] . "', '" . $warWeight['penalty'] . "');";
		
			if ($conn->query($member_ww_insert_sql) === TRUE) {
				echo "War Weight Updated";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
	}
?>
		
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
	<title><?php echo $member_array["member_name"]; ?></title>
</head>
<body>
	<h1 align="center"><?php echo $member_array["member_name"]; ?></h1>
	<center><img src="<?php echo $member_array["league_iconUrls_medium"]; ?>" /><br><c/enter>
	<div id="accordion">
		<h3 align="center">Defense Building War Weight</h3>
		<div id="Building War Weight">
			<center>
				<?php if ( $warWeight['flag'] == 0 ) { ?>
					<h2>Buildings have not been entered for this players yet.</h2>
					<p>If this is your base and you want this updated, please send the count and level of your base 
					to gary@the-blacklist.ca with your player name. You can easily pull this by going to base design
					and using the "Remove All" button.</p>
					<p>i.e. 2 cannnon level 10, 2 cannon level 11, etc...</p>
				<?php } else { ?>
					<h3>Buildings*</h3>
					<table width="100%" class="sortable">
						<thead>
							<tr>
								<td align="center">
									<b></b>
								</td>
								<?php for ( $i = 1; $i <= 8; $i++ ) { ?>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
								<?php } ?>
								<td align="center">
									<b>Max Level<br>For TH</b>
								</td>
								<td align="center">
									<b>Max<br>Level</b>
								</td>
								<td align="center">
									<b>War<br>Weight</b>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;
							while ( $i < 20 ) { 
								if ( empty ( $defense_array[$i]["name"] )) {
									$i++;
									continue;
								}?>
								<tr>
									<td align="right">
										<?php echo $defense_array[$i]["name"]; ?>
									</td>
									<?php for ( $j = 1; $j <= 8; $j++ ) { ?>
										<td align="center">
											<?php 
												if ( empty ( $defense_array[$i]["level"][$j] ) && $j <= $defense_array[$i]["thCnt"] ) {
													echo "0";
												} elseif ( empty ( $defense_array[$i]["level"][$j] )) {
													echo "--";
												} elseif ( $defense_array[$i]["level"][$j] == $defense_array[$i]["maxTHLevel"] ) {
													echo "<font color=\"green\">" . $defense_array[$i]["level"][$j] . "</font>";
												} else {
													echo $defense_array[$i]["level"][$j]; 
												}
											?>
										</td>
									<?php } ?>
									<td align="center">
										<?php echo $defense_array[$i]["maxTHLevel"]; ?>
									</td>
									<td align="center">
										<?php echo $defense_array[$i]["maxLevel"]; ?>
									</td>
									<td align="right">
										<?php echo number_format ( $defense_array[$i]["warWeight"], 1, ".", "," ); ?>
									</td>
								</tr>
							<?php $i++;
							} ?>
						</tbody>
						<tfoot>
							<tr>
								<td align="right" colspan="11">
									<b>Sub Total</b>
								</td>
								<td align="right">
									<b><?php echo number_format ( $warWeight['bldg_defense'], 1, ".", "," );?></b>
								</td>
							</tr>
						</tfoot>
					</table>
					<p>*  Shows the building level for each building.</p><br>
				
					<h3>Walls*</h3>
					<table width="100%" class="sortable">
						<thead>
							<tr>
								<td align="center">
									<b></b>
								</td>
								<?php for ( $i = 1; $i <= 12; $i++ ) { ?>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
								<?php } ?>
								<td align="center">
									<b>War<br>Weight</b>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td align="right">
									Walls Count
								</td>
								<?php for ( $i = 1; $i <= 12; $i++ ) { ?>
									<td align="center">
										<?php 
											if ( empty ( $defense_array[20]["cnt"][$i] ) && $i <= $defense_array[20]["maxTHLevel"] ) {
												echo "0";
											} elseif ( empty ( $defense_array[20]["cnt"][$i] )) {
												echo "--";
											} elseif ( $defense_array[20]["cnt"][$i] == $defense_array[20]["maxTHLevel"] ) {
												echo "<font color=\"green\">" . $defense_array[20]["cnt"][$i] . "</font>";
											} else {
												echo $defense_array[20]["cnt"][$i]; 
											}
										?>
									</td>
								<?php } ?>
								<td align="right">
									<?php echo number_format ( $defense_array[20]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						</tbody>
					</table>
					<p>*  Shows the number of wall units for each wall level.</p><br>

					<h3>Totals</h3>
					<table width="50%" class="sortable">
						<thead>
							<tr>
								<td align="center">
									<b></b>
								</td>
								<td align="center">
									<b>War<br>Weight</b>
								</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td align="right">
									Buildings Sub Total
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['bldg_defense'], 1, ".", "," ); ?>
								</td>
							</tr>
							<tr>
								<td align="right">
									Walls
								</td>
								<td align="right">
									<?php echo number_format ( $defense_array[20]['warWeight'], 1, ".", "," ); ?>
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td align="right">
									Total
								</td>
								<td align="right">
									<?php echo number_format ( $warWeight['defense'], 1, ".", "," ); ?>
								</td>
							</tr>
						</tfoot>
					</table>
					
				<?php } 
					echo "<p>Stats above were updated on: " . $defense_array[0]["timestamp"] . "</p>"; ?>
			</center>
		</div>
		<h3 align="center">Trap War Weight</h3>
		<div id="Trap War Weight">
			<center>
				<?php if ( $warWeight['flag'] == 0 ) { ?>
					<h2>Traps have not been entered for this players yet.</h2>
					<p>If this is your base and you want this updated, please send the count and level of your base 
					to gary@the-blacklist.ca with your player name. You can easily pull this by going to base design
					and using the "Remove All" button.</p>
					<p>i.e. 2 cannnon level 10, 2 cannon level 11, etc...</p>
				<?php } else { ?>
					<table width="100%" class="sortable">
						<thead>
							<tr>
								<td align="center">
									<b></b>
								</td>
								<?php for ( $i = 1; $i <= 6; $i++ ) { ?>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
								<?php } ?>
								<td align="center">
									<b>Max Level<br>For TH</b>
								</td>
								<td align="center">
									<b>Max<br>Level</b>
								</td>
								<td align="center">
									<b>War<br>Weight</b>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;
							while ( $i < 20 ) { 
								if ( empty ( $traps_array[$i]["name"] )) {
									$i++;
									continue;
								}?>
								<tr>
									<td align="right">
										<?php echo $traps_array[$i]["name"]; ?>
									</td>
									<?php for ( $j = 1; $j <= 6; $j++ ) { ?>
										<td align="center">
											<?php 
												if ( empty ( $traps_array[$i]["level"][$j] ) && $j <= $traps_array[$i]["thCnt"] ) {
													echo "0";
												} elseif ( empty ( $traps_array[$i]["level"][$j] )) {
													echo "--";
												} elseif ( $traps_array[$i]["level"][$j] == $traps_array[$i]["maxTHLevel"] ) {
													echo "<font color=\"green\">" . $traps_array[$i]["level"][$j] . "</font>";
												} else {
													echo $traps_array[$i]["level"][$j]; 
												}
											?>
										</td>
									<?php } ?>
									<td align="center">
										<?php echo $traps_array[$i]["maxTHLevel"]; ?>
									</td>
									<td align="center">
										<?php echo $traps_array[$i]["maxLevel"]; ?>
									</td>
									<td align="right">
										<?php echo number_format ( $traps_array[$i]["warWeight"], 1, ".", "," ); ?>
									</td>
								</tr>
							<?php $i++;
							} ?>
						</tbody>
						<tfoot>
							<tr>
								<td align="right" colspan="9">
									<b>Sub Total</b>
								</td>
								<td align="right">
									<b><?php echo number_format ( $warWeight['traps'], 1, ".", "," );?></b>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php } 
					echo "<p>Stats above were updated on: " . $traps_array[0]["timestamp"] . "</p>"; ?>
			</center>
		</div>
		<h3 align="center">Misc. Building War Weight</h3>
		<div id="Misc Bldg War Weight">
			<center>
				<?php if ( $warWeight['flag'] == 0 ) { ?>
					<h2>Misc. Bldg. have not been entered for this players yet.</h2>
					<p>If this is your base and you want this updated, please send the count and level of your base 
					to gary@the-blacklist.ca with your player name. You can easily pull this by going to base design
					and using the "Remove All" button.</p>
					<p>i.e. 2 cannnon level 10, 2 cannon level 11, etc...</p>
				<?php } else { ?>
					<table width="100%" class="sortable">
						<thead>
							<tr>
								<td align="center">
									<b></b>
								</td>
								<?php for ( $i = 1; $i <= 7; $i++ ) { ?>
									<td align="center">
										<b><?php echo $i ?></b>
									</td>
								<?php } ?>
								<td align="center">
									<b>Max Level<br>For TH</b>
								</td>
								<td align="center">
									<b>Max<br>Level</b>
								</td>
								<td align="center">
									<b>War<br>Weight</b>
								</td>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;
							while ( $i < 20 ) { 
								if ( empty ( $misc_array[$i]["name"] )) {
									$i++;
									continue;
								}?>
								<tr>
									<td align="right">
										<?php echo $misc_array[$i]["name"]; ?>
									</td>
									<?php for ( $j = 1; $j <= 7; $j++ ) { ?>
										<td align="center">
											<?php 
												if ( empty ( $misc_array[$i]["level"][$j] ) && $j <= $misc_array[$i]["thCnt"] ) {
													echo "0";
												} elseif ( empty ( $misc_array[$i]["level"][$j] )) {
													echo "--";
												} elseif ( $misc_array[$i]["level"][$j] == $misc_array[$i]["maxTHLevel"] ) {
													echo "<font color=\"green\">" . $misc_array[$i]["level"][$j] . "</font>";
												} else {
													echo $misc_array[$i]["level"][$j]; 
												}
											?>
										</td>
									<?php } ?>
									<td align="center">
										<?php echo $misc_array[$i]["maxTHLevel"]; ?>
									</td>
									<td align="center">
										<?php echo $misc_array[$i]["maxLevel"]; ?>
									</td>
									<td align="right">
										<?php echo number_format ( $misc_array[$i]["warWeight"], 1, ".", "," ); ?>
									</td>
								</tr>
							<?php $i++;
							} ?>
						</tbody>
						<tfoot>
							<tr>
								<td align="right" colspan="10">
									<b>Sub Total</b>
								</td>
								<td align="right">
									<b><?php echo number_format ( $warWeight['misc'], 1, ".", "," );?></b>
								</td>
							</tr>
						</tfoot>
					</table>
				<?php } 
					echo "<p>Stats above were updated on: " . $misc_array[0]["timestamp"] . "</p>"; ?>
			</center>
		</div>
		<h3 align="center">Hero War Weight</h3>
		<div id="Hero War Weight">
			<center>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b>Hero</b>
							</td>
							<td align="center">
								<b>Level</b>
							</td>
							<td align="center">
								<b>Max Level<br>For TH</b>
							</td>
							<td align="center">
								<b>Max<br>Level</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 30 ) { 
							if ( empty ( $heros_array[$i]["name"] )) {
								$i = 30;
								break;
							}?>
							<tr>
								<td align="right">
									<?php echo $heros_array[$i]["name"]; ?>
								</td>
								<td align="center">
									<?php if ( $heros_array[$i]["level"] == $heros_array[$i]["maxTHLevel"] ) {
											   echo "<font color=\"green\">" . $heros_array[$i]["level"] . "</font>";
											 } else {
											   echo $heros_array[$i]["level"]; 
											 }
									?>
								</td>
								<td align="center">
									<?php echo $heros_array[$i]["maxTHLevel"]; ?>
								</td>
								<td align="center">
									<?php echo $heros_array[$i]["maxLevel"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $heros_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="4">
								<b>Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['heros'], 1, ".", "," );?></b>
							</td>
						</tr>
					</tfoot>
				</table>
			</center>
		</div>
		<h3 align="center">Troop War Weight</h3>
		<div id="Troop War Weight">
			<center>
				<h3>Elixir Troops</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b>Troop</b>
							</td>
							<td align="center">
								<b>Level</b>
							</td>
							<td align="center">
								<b>Max Level<br>For TH</b>
							</td>
							<td align="center">
								<b>Max<br>Level</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 30 ) { 
							if ( empty ( $troops_array[$i]["name"] )) {
								$i = 30;
								break;
							}?>
							<tr>
								<td align="right">
									<?php echo $troops_array[$i]["name"]; ?>
								</td>
								<td align="center">
									<?php if ( $troops_array[$i]["level"] == $troops_array[$i]["maxTHLevel"] ) {
											   echo "<font color=\"green\">" . $troops_array[$i]["level"] . "</font>";
											 } else {
											   echo $troops_array[$i]["level"]; 
											 }
									?>
								</td>
								<td align="center">
									<?php echo $troops_array[$i]["maxTHLevel"]; ?>
								</td>
								<td align="center">
									<?php echo $troops_array[$i]["maxLevel"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $troops_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="4">
								<b>Sub Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['elixir_troops'], 1, ".", "," );?></b>
							</td>
						</tr>
					</tfoot>
				</table>


				<h3>Dark Elixir Troops</h3>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b>Troop</b>
							</td>
							<td align="center">
								<b>Level</b>
							</td>
							<td align="center">
								<b>Max Level<br>For TH</b>
							</td>
							<td align="center">
								<b>Max<br>Level</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php while ( $i < 60 ) { 
							if ( empty ( $troops_array[$i]["name"] )) {
								break;
							}?>
							<tr>
								<td align="right">
									<?php echo $troops_array[$i]["name"]; ?>
								</td>
								<td align="center">
									<?php if ( $troops_array[$i]["level"] == $troops_array[$i]["maxTHLevel"] ) {
											   echo "<font color=\"green\">" . $troops_array[$i]["level"] . "</font>";
											 } else {
											   echo $troops_array[$i]["level"]; 
											 }
									?>
								</td>
								<td align="center">
									<?php echo $troops_array[$i]["maxTHLevel"]; ?>
								</td>
								<td align="center">
									<?php echo $troops_array[$i]["maxLevel"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $troops_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot
						<tr>
							<td align="right" colspan="4">
								<b>Sub Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['de_troops'], 1, ".", "," ); ?></b>
							</td>
						</tr>
					</tfoot>
				</table>


				<h3>Totals</h3>
				<table width="50%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b>Troop Type</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="right">
								Elixir Troops
							</td>
							<td align="right">
								<?php echo number_format ( $warWeight['elixir_troops'], 1, ".", "," ); ?>
							</td>
						</tr>
						<tr>
							<td align="right">
								Dark Elixir Troops
							</td>
							<td align="right">
								<?php echo number_format ( $warWeight['de_troops'], 1, ".", "," ); ?>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td align="right">
								Total
							</td>
							<td align="right">
								<?php echo number_format ( $warWeight['troops'], 1, ".", "," ); ?>
							</td>
						</tr>
					</tfoot>
				</table>
			</center>
		</div>
		<h3 align="center">Spell War Weight</h3>
		<div id="Spell War Weight">
			<center>
				<table width="100%" class="sortable">
					<thead>
						<tr>
							<td align="center">
								<b>Spell</b>
							</td>
							<td align="center">
								<b>Level</b>
							</td>
							<td align="center">
								<b>Max Level<br>For TH</b>
							</td>
							<td align="center">
								<b>Max<br>Level</b>
							</td>
							<td align="center">
								<b>War<br>Weight</b>
							</td>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						while ( $i < 30 ) { 
							if ( empty ( $spells_array[$i]["name"] )) {
								$i++;
								continue;
							}?>
							<tr>
								<td align="right">
									<?php echo $spells_array[$i]["name"]; ?>
								</td>
								<td align="center">
									<?php if ( $spells_array[$i]["level"] == $spells_array[$i]["maxTHLevel"] ) {
											   echo "<font color=\"green\">" . $spells_array[$i]["level"] . "</font>";
											 } else {
											   echo $spells_array[$i]["level"]; 
											 }
									?>
								</td>
								<td align="center">
									<?php echo $spells_array[$i]["maxTHLevel"]; ?>
								</td>
								<td align="center">
									<?php echo $spells_array[$i]["maxLevel"]; ?>
								</td>
								<td align="right">
									<?php echo number_format ( $spells_array[$i]["warWeight"], 1, ".", "," ); ?>
								</td>
							</tr>
						<?php $i++;
						} ?>
					</tbody>
					<tfoot>
						<tr>
							<td align="right" colspan="4">
								<b>Total</b>
							</td>
							<td align="right">
								<b><?php echo number_format ( $warWeight['spells'], 1, ".", "," );?></b>
							</td>
						</tr>
					<tfoot>
				</table>
			</center>
		</div>
		<h3 align="center">Total War Weight</h3>
		<div id="Total War Weight">
			<center>
				<table width="50%">
					<tr>
						<td align="right" width="60%">
							Def. Bldg. War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['defense'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Trap War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['traps'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Misc. Bldg. War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['misc'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Hero War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['heros'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Troop War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['troops'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Spell War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['spells'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Total War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['total'], 1, ".", "," ); ?>
						</td>
					</tr>
				</table>
				<br>
				<table width="50%">
					<tr>
						<td align="right" width="60%">
							Unadjusted War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['unadjusted'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							Adjusted War Weight:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['adjusted'], 1, ".", "," ); ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							War Weight Penalty:
						</td>
						<td align="right">
							<?php echo number_format ( $warWeight['penalty'], 1, ".", "," ); ?>
						</td>
					</tr>
				</table>
				<br>
				<b>-- Information Only --</b>
				<table width="50%" class="sortable">
					<tr>
						<td align="center">
							<b>Town Hall</b>
						</td>
						<td align="center">
							<b>Median<br>War Weight</b>
						</td>
						<td align="center">
							<b>Max<br>War Weight</b>
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>1</b>
						</td>
						<td align="right">
							0
						</td>
						<td align="right">
							0
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>2</b>
						</td>
						<td align="right">
							1,000
						</td>
						<td align="right">
							2,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>3</b>
						</td>
						<td align="right">
							4,000
						</td>
						<td align="right">
							6,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>4</b>
						</td>
						<td align="right">
							8,000
						</td>
						<td align="right">
							10,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>5</b>
						</td>
						<td align="right">
							14,000
						</td>
						<td align="right">
							18,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>6</b>
						</td>
						<td align="right">
							22,000
						</td>
						<td align="right">
							26,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>7</b>
						</td>
						<td align="right">
							33,000
						</td>
						<td align="right">
							40,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>8</b>
						</td>
						<td align="right">
							51,000
						</td>
						<td align="right">
							62,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>9</b>
						</td>
						<td align="right">
							74,000
						</td>
						<td align="right">
							86,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>10</b>
						</td>
						<td align="right">
							103,000
						</td>
						<td align="right">
							120,000
						</td>
					</tr>
					<tr>
						<td align="center">
							<b>11</b>
						</td>
						<td align="right">
							135,000
						</td>
						<td align="right">
							150,000
						</td>
					</tr>
				</table>
			</center>
		</div>

	</div>	
	<center>
		<img src="<?php echo $member_array["clan_badgeUrls_medium"]; ?>" /><br>
	</center>
		
<center>
	<b>This data was updated at <?php echo $member_array["member_timestamp"]; ?> GMT.</b><br>
</center>
</br>  

<script type="text/javascript" src="/images/jumi_code/sorttable.js"></script>
<!-- JQuery Script -->
<link rel="stylesheet" href="/images/jumi_code/jquery-ui.theme.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#accordion" ).accordion({
			heightStyle: "content", active: 6
		});
	});
</script>

</body>

<?php
function maxTHLevel ( $inputName, $inputTH, $conn ) {
	$maxTHLevel = 0;
	
	// escape single quotes
	$inputName = addslashes($inputName);	
	
	// Freeze spell is special
	if ( $inputName == 'Freeze' && $inputTH = 9 ) {
		return 1;
	}
	
	$maxTHLevel_sql = "SELECT tww.`ww_level`, lww.`ww_thLevel`  ";
	$maxTHLevel_sql .= " FROM `API_WarWeight` AS tww ";
	$maxTHLevel_sql .= " INNER JOIN `API_WarWeight` AS lww ";
	$maxTHLevel_sql .= "   ON tww.`ww_labLevel` = lww.`ww_level` ";
	$maxTHLevel_sql .= " WHERE tww.`ww_name` LIKE '" . $inputName . "' ";
	$maxTHLevel_sql .= "   AND lww.`ww_name` LIKE 'Laboratory' ";
	$maxTHLevel_sql .= " ORDER BY tww.`ww_level` ASC;";
	$maxTHLevel_result = $conn->query($maxTHLevel_sql);
	
	while($maxTHLevel_row = $maxTHLevel_result->fetch_assoc()) {
		$temp_level = $maxTHLevel_row["ww_level"];
		$temp_thLevel = $maxTHLevel_row["ww_thLevel"];
		
		if ( $temp_thLevel <= $inputTH ) {
			$maxTHLevel = $temp_level;
		}
	}
	
	return $maxTHLevel;
}
