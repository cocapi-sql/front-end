<?php
	$clan_array = array();

	$clan_sql = "SELECT * ";
	$clan_sql .= "FROM `API_Clan` AS ac ";
	$clan_sql .= "INNER JOIN `API_Clan_Info` AS aci ";
	$clan_sql .= "  ON ac.clan_tag=aci.clan_info_tag ";
	$clan_sql .= "WHERE clan_name LIKE '" . $clan_selected . "' ";
	$clan_sql .= "ORDER BY clan_info_time DESC ";
	$clan_sql .= "LIMIT 1;";
	$clan_result = $conn->query($clan_sql);
	

	while($clan_row = $clan_result->fetch_assoc()) {
		$clan_array["clan_tag"] = $clan_row["clan_tag"];
		$clan_array["clan_name"] = $clan_row["clan_name"];
		$clan_array["clan_description"] = str_replace( '\\', '', $clan_row["clan_description"] );
		$clan_array["clan_badgeUrls_small"] = $clan_row["clan_badgeUrls_small"];
		$clan_array["clan_badgeUrls_medium"] = $clan_row["clan_badgeUrls_medium"];
		$clan_array["clan_badgeUrls_large"] = $clan_row["clan_badgeUrls_large"];
		$clan_array["clan_location_id"] = $clan_row["clan_location_id"];
		$clan_array["clan_location_name"] = $clan_row["clan_location_name"];
		$clan_array["clan_location_countryCode"] = $clan_row["clan_location_countryCode"];
		$clan_array["clan_warFrequency"] = $clan_row["clan_warFrequency"];
		$clan_array["clan_info_time"] = $clan_row["clan_info_time"];
		$clan_array["clan_info_clanLevel"] = $clan_row["clan_info_clanLevel"];
		$clan_array["clan_info_clanPoints"] = $clan_row["clan_info_clanPoints"];
		$clan_array["clan_info_warWins"] = $clan_row["clan_info_warWins"];
		$clan_array["clan_info_warWinStreak"] = $clan_row["clan_info_warWinStreak"];
		$clan_array["clan_info_warTies"] = $clan_row["clan_info_warTies"];
		$clan_array["clan_info_warLosses"] = $clan_row["clan_info_warLosses"];
		$clan_array["clan_info_members"] = $clan_row["clan_info_members"];
		$clan_array["clan_info_type"] = $clan_row["clan_info_type"];
		$clan_array["clan_info_requiredTrophies"] = $clan_row["clan_info_requiredTrophies"];
		$clan_array["clan_info_isWarLogPublic"] = $clan_row["clan_info_isWarLogPublic"];
	}
	
	if ( $clan_array["clan_info_warLosses"] == 0 ) {
		
		$clan_sql = "SELECT clan_info_warTies, clan_info_warLosses ";
		$clan_sql .= "FROM `API_Clan_Info` ";
		$clan_sql .= "WHERE clan_info_tag LIKE '" . $clan_array["clan_tag"] . "' ";
		$clan_sql .= " AND clan_info_warLosses > 0 ";
		$clan_sql .= "ORDER BY clan_info_time ";
		$clan_sql .= "DESC LIMIT 1;";
		$clan_result = $conn->query($clan_sql);
	

		while($clan_row = $clan_result->fetch_assoc()) {
			$clan_array["clan_info_warTies"] = $clan_row["clan_info_warTies"];
			$clan_array["clan_info_warLosses"] = $clan_row["clan_info_warLosses"];
		}
	}
	if ( $clan_array["clan_tag"] == "#P9GYVGCP" ) {			// BlackList
		$clan_array["clan_article_id"] = 59;
	} elseif ( $clan_array["clan_tag"] == "#LQVUPRR0" ) {	// WhiteList
		$clan_array["clan_article_id"] = 60;
	} elseif ( $clan_array["clan_tag"] == "#LQVUPRR0" ) {	// GoldList
		$clan_array["clan_article_id"] = 61;
	} else {	// Unknown
		$clan_array["clan_article_id"] = 62;				// Unknown
	}
?>


	<title><?php echo $clan_array["clan_name"]; ?></title>
</head>
<body>

	<h1 align="center"><?php echo $clan_array["clan_name"]; ?></h1>
	<center>
		<img src="<?php echo $clan_array["clan_badgeUrls_medium"]; ?>" /><br>
		<?php echo $clan_array["clan_description"]; ?><br>
	</center>
	
	<table width="100%">
		<tr>
			<td width="50%">
				<center>
					General Information
				</center>
				<table width="100%">
					<tr>
						<td align="right" width="40%">
							<b>Tag:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_tag"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Level:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_clanLevel"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Points:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_clanPoints"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Members:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_members"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Type:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_type"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Trophies:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_requiredTrophies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Frequency:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_warFrequency"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>Location:</b>
						</td>
						<td>
							<?php echo $clan_array["clan_location_name"]; ?>
						</td>
					</tr>
				</table>
			</td>
			<td>
				<center>
					<a href="http://www.the-blacklist.ca/index.php?option=com_content&view=article&id=<?php echo $clan_array["clan_article_id"] ?>"><b>War Information</b></a>
				</center>
				<table width="100%">
					<tr>
						<td align="right" width="40%">
							<b>Wars Won :</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_warWins"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>War Win Streak :</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_warWinStreak"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>War Tied :</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_warTies"]; ?>
						</td>
					</tr>
					<tr>
						<td align="right">
							<b>War Lost :</b>
						</td>
						<td>
							<?php echo $clan_array["clan_info_warLosses"]; ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>  
  
	<center>
		<b>This data was updated at <?php echo $clan_array["clan_info_time"]; ?> GMT.</b>
	</center>
  
  
  
  
  